@extends('layout.app')

@section('content')
    <div class="card text-left">
        <div class="card-header">Введите email, чтобы сгенерировать токен для доступа к API</div>
        <div class="card-body">
            <form action="{{ route('token.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <button type="submit" class="btn btn-primary">Подтвердить</button>
            </form>
        </div>
    </div>
@endsection
