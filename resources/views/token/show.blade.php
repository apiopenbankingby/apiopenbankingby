@extends('layout.app')

@section('content')
    <div class="card text-left">
        <div class="card-body" style="max-width: 500px; font-size: 15px">
            <div class="container">
                <div class="row">
                    <p>
                        Это единственный раз, когда вы видите сгенерированный токен. Запишите его и используйте для доступа к API.
                    </p>
                    <p id="token" class="text-break" style="font-size: 12px;">{{ $token }}</p>
                </div>
                <div class="row mt-1">
                    <button type="submit" class="btn btn-primary"  onclick="copyDivToClipboard()">Скопировать</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        const copyDivToClipboard = () => {
            const range = document.createRange();
            range.selectNode(document.getElementById("token"));
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
            document.execCommand("copy");
            window.getSelection().removeAllRanges();
        }
    </script>
@endsection
