<?php

namespace Tests\Feature;

use App\Factory\ATMFactory;
use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use Tests\TestCase;
use Tests\Traits\JsonSchema;

class SchemaJsonValidationTest extends TestCase
{
    use JsonSchema;

    public function setUp(): void
    {
        parent::setUp();
        $this->delete(['branches', 'atms', 'banks']);
    }

    /**
     * Bank - json schema
     */
    public function testBankJsonSchema()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);
        $bank->atm()->attach([$atm->id]);

        $bankResponse = $this->json('GET', '/banks')->content();
        $jsonSchema = $this->generateJsonSchema('/banks', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }

    /**
     * Bank one - json schema
     */
    public function testBankOneJsonSchema()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);
        $bank->atm()->attach([$atm->id]);

        $bankResponse = $this->json('GET', "/banks/{$bank->id}")->content();
        $jsonSchema = $this->generateJsonSchema('/banks/{id}', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }

    /**
     * ATM - json schema
     */
    public function testATMJsonSchema()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $bankResponse = $this->json('GET', "/banks/{$bank->id}/atms")->content();
        $jsonSchema = $this->generateJsonSchema('/banks/{id}/atms', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }

    /**
     * ATM one - json schema
     */
    public function testATMOneJsonSchema()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $bankResponse = $this->json('GET', "/banks/{$bank->id}/atms/{$atm->id}")->content();
        $jsonSchema = $this->generateJsonSchema('/banks/{id}/atms/{atmId}', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }

    /**
     * Branch - json schema
     */
    public function testBranchJsonSchema()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $bankResponse = $this->json('GET', "/banks/{$bank->id}/branches")->content();
        $jsonSchema = $this->generateJsonSchema('/banks/{id}/branches', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }

    /**
     * Branch one - json schema
     */
    public function testBranchOneJsonSchema()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $bankResponse = $this->json('GET', "/banks/{$bank->id}/branches/{$branch->id}")->content();
        $jsonSchema = $this->generateJsonSchema('/banks/{id}/branches/{branchId}', 'get', 200);
        $response = json_decode($bankResponse);

        $jsonValidator = $this->jsonValidator($response, $jsonSchema);

        $this->assertEmpty($jsonValidator->getErrors());
        $this->assertIsBool($jsonValidator->isValid());
        $this->assertTrue($jsonValidator->isValid());
    }
}
