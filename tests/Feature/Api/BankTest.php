<?php

namespace Tests\Feature\Api;

use App\Factory\ATMFactory;
use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Models\Bank;
use Tests\TestCase;

class BankTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->delete(['branches', 'atms', 'banks']);
    }

    /**
     * Index bank
     */
    public function testBankIndex()
    {
        $bank = BankFactory::create();
        $this->factoryBank($bank);

        $this->json('GET', '/banks')
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'BICFI',
                    'ClearingSystemIdentification',
                    'ClearingSystemMemberIdentification',
                    'Code',
                    'Proprietary',
                    'MemberIdentification',
                    'Name',
                    'LegalEntityIdentifier',
                    'PostalAddress' => [
                        'StreetName',
                        'BuildingNumber',
                        'Department',
                        'PostCode',
                        'TownName',
                        'CountrySubDivision',
                        'Country',
                        'AddressLine',
                        'Description'
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Show bank
     */
    public function testShow()
    {
        $bank = BankFactory::create();
        $this->factoryBank($bank);

        $this->json('GET', "/banks/{$bank->id}")
            ->assertOk()
            ->assertJsonStructure([
                'Id',
                'BICFI',
                'ClearingSystemIdentification',
                'ClearingSystemMemberIdentification',
                'Code',
                'Proprietary',
                'MemberIdentification',
                'Name',
                'LegalEntityIdentifier',
                'PostalAddress' => [
                    'StreetName',
                    'BuildingNumber',
                    'Department',
                    'PostCode',
                    'TownName',
                    'CountrySubDivision',
                    'Country',
                    'AddressLine',
                    'Description'
                ]
            ]);
    }

    /**
     * Show fail bank
     */
    public function testShowFail()
    {
        $this->json('GET', "/banks/123456")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store bank
     */
    public function testStore()
    {
        $params = $this->params();

        $this->json('POST', '/banks/', $params)
            ->assertOk();
    }

    /**
     * Store fail bank
     */
    public function testStoreFail()
    {
        $params = $this->paramsFail();

        $this->json('POST', '/banks/', $params)
            ->assertStatus(422);
    }

    /**
     * Update Bank
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}", $params)
            ->assertOk();

        $this->assertDatabaseHas('banks', [
            'bicfi' => $params['BICFI'],
            'clearing_system_member_identification' => $params['ClearingSystemMemberIdentification'],
            'proprietary' => $params['Proprietary'],
            'member_identification' => $params['MemberIdentification'],
            'name' => $params['Name'],
        ]);
    }

    /**
     * Update fail bank
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete bank
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $this->factoryBank($bank);

        $this->json('DELETE', "/banks/{$bank->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail bank
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Factory - bank
     *
     * @param $bank
     */
    private function factoryBank(Bank $bank): void
    {
        $atm = ATMFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);
        $bank->atm()->attach([$atm->id]);
    }

    /**
     * Param bank
     *
     * @return array
     */
    private function params(): array
    {
        return [
            'Id' => 'Test-params-bank-id',
            'BICFI' => 'BPSBBY2X',
            'ClearingSystemMemberIdentification' => 1234567,
            'Code' => null,
            'Proprietary' => 'BYNBB',
            'MemberIdentification' => 'Банк ONE',
            'Name' => 'БПС-Сбербанк - ONE',
            'LegalEntityIdentifier' => null,
            'PostalAddress' => [
                'StreetName' => 'бульвар имени Мулявина',
                'BuildingNumber' => '10',
                'Department' => null,
                'PostCode' => '220005',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '5',
                'Country' => 'BB',
            ]
        ];
    }

    /**
     * Param fail bank
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Test-params-bank-id',
            'PostalAddress' => [
                'Country' => 'BB',
            ]
        ];
    }
}
