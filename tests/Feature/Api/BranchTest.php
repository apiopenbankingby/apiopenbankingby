<?php

namespace Tests\Feature\Api;

use App\Factory\ATMFactory;
use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Models\Bank;
use Tests\TestCase;

class BranchTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->delete(['branches', 'atms', 'banks']);
    }

    /**
     * Index branch
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $this->json('GET', "/banks/{$bank->id}/branches")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Name',
                    'WiFi',
                    'EQueue',
                    'Accessibilities' => [
                        'Accessibility' => [[
                            'Type',
                            'Description'
                        ]]
                    ],
                    'PostalAddress' => [
                        'StreetName',
                        'BuildingNumber',
                        'Department',
                        'PostCode',
                        'TownName',
                        'CountrySubDivision',
                        'Country',
                        'AddressLine',
                        'Description',
                        'GeographicCoordinates' => [
                            'Geolocation' => [
                                'Latitude',
                                'Longitude'
                            ]
                        ]
                    ],
                    'Information' => [[
                        'Segment',
                        'Availability' => [[
                            'Access24Hours',
                            'IsRestricted',
                            'SameAsOrganization',
                            'Description',
                            'StandardAvailability' => [
                                'Day' => [[
                                    'DayCode',
                                    'OpeningTime',
                                    'ClosingTime',
                                    'Break' => [[
                                        'BreakFromTime',
                                        'BreakToTime'
                                    ]]
                                ]]
                            ],
                            'NonStandardAvailability' => [[
                                'Name',
                                'FromDate',
                                'ToDate',
                                'Description',
                                'Day' => [[
                                    'DayCode',
                                    'OpeningTime',
                                    'ClosingTime',
                                    'Break' => [[
                                        'BreakFromTime',
                                        'BreakToTime'
                                    ]]
                                ]]
                            ]]
                        ]],
                        'ContactDetails' => [[
                            'Name',
                            'PhoneNumber',
                            'MobileNumber',
                            'FaxNumber',
                            'EmailAddress',
                            'Other',
                            'SocialNetworks' => [[
                                'Network',
                                'URL',
                                'Description'
                            ]]
                        ]]
                    ]]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Show branch
     */
    public function testShow()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $this->json('GET', "/banks/{$bank->id}/branches/{$branch->id}")
            ->assertOk()
            ->assertJsonStructure([
                'Id',
                'Name',
                'WiFi',
                'EQueue',
                'Accessibilities' => [
                    'Accessibility' => [[
                        'Type',
                        'Description'
                    ]]
                ],
                'PostalAddress' => [
                    'StreetName',
                    'BuildingNumber',
                    'Department',
                    'PostCode',
                    'TownName',
                    'CountrySubDivision',
                    'Country',
                    'AddressLine',
                    'Description',
                    'GeographicCoordinates' => [
                        'Geolocation' => [
                            'Latitude',
                            'Longitude'
                        ]
                    ]
                ],
                'Information' => [[
                    'Segment',
                    'Availability' => [[
                        'Access24Hours',
                        'IsRestricted',
                        'SameAsOrganization',
                        'Description',
                        'StandardAvailability' => [
                            'Day' => [[
                                'DayCode',
                                'OpeningTime',
                                'ClosingTime',
                                'Break' => [[
                                    'BreakFromTime',
                                    'BreakToTime'
                                ]]
                            ]]
                        ],
                        'NonStandardAvailability' => [[
                            'Name',
                            'FromDate',
                            'ToDate',
                            'Description',
                            'Day' => [[
                                'DayCode',
                                'OpeningTime',
                                'ClosingTime',
                                'Break' => [[
                                    'BreakFromTime',
                                    'BreakToTime'
                                ]]
                            ]]
                        ]]
                    ]],
                    'ContactDetails' => [[
                        'Name',
                        'PhoneNumber',
                        'MobileNumber',
                        'FaxNumber',
                        'EmailAddress',
                        'Other',
                        'SocialNetworks' => [[
                            'Network',
                            'URL',
                            'Description'
                        ]]
                    ]]
                ]],
                'Services'
            ]);
    }

    /**
     * Show fail branch
     */
    public function testShowFail()
    {
        $this->json('GET', "/banks/123456/branches/123456")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store branch
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $params = $this->params();

        $this->json('POST', "/banks/{$bank->id}/branches", $params)
            ->assertOk();
    }

    /**
     * Store fail branch
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/branches", $params)
            ->assertStatus(422);
    }

    /**
     * Update branch
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/branches/{$branch->id}", $params)
            ->assertOk();

        $this->assertDatabaseHas('branches', [
            'name' => $params['Name']
        ]);
    }

    /**
     * Update fail branch
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/branches/{$branch->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete branch
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $this->json('DELETE', "/banks/{$bank->id}/branches/{$branch->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail branch
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/branches/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param branch
     *
     * @return array
     */
    private function params(): array
    {
        return [
            'Id' => 'Test-params-branch-id',
            'Name' => 'Дополнительный офис №751',
            'WiFi' => true,
            'EQueue' => false,
            'Accessibilities' => [
                [
                    'Type' => 'Braille',
                    'Description' => null
                ]
            ],
            'PostalAddress' => [
                'StreetName' => 'ул. Маяковского',
                'BuildingNumber' => '5',
                'Department' => null,
                'PostCode' => '220028',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '10',
                'Country' => 'AF',
                'Geolocation' => [
                    'Latitude' => 54.86857,
                    'Longitude' => 28.569547
                ]
            ],
            'Information' => [
                [
                    'Segment' => 'Business',
                    'Availability' => [
                        [
                            'Access24Hours' => false,
                            'IsRestricted' => false,
                            'SameAsOrganization' => false,
                            'Description' => 'sample availability desc',
                            'StandardAvailability' => [
                                'Day' => [
                                    [
                                        'DayCode' => '01',
                                        'OpeningTime' => '08:00:00',
                                        'ClosingTime' => '17:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '13:30:00',
                                                'BreakToTime' => '14:30:00'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'NonStandardAvailability' => [
                                [
                                    'Name' => 'ExtraDayOff',
                                    'FromDate' => '2019-05-09 00:00:00',
                                    'ToDate' => '2019-05-09 00:00:00',
                                    'Description' => 'sample extra day off description',
                                    'Day' => [
                                        [
                                            'DayCode' => '03',
                                            'OpeningTime' => '09:00:00',
                                            'ClosingTime' => '18:00:00',
                                            'Break' => [
                                                [
                                                    'BreakFromTime' => '10:30:00',
                                                    'BreakToTime' => '13:30:00'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'ContactDetails' => [
                        [
                            'Name' => null,
                            'PhoneNumber' => '5-148-148',
                            'MobileNumber' => '5-148-148',
                            'FaxNumber' => null,
                            'EmailAddress' => 'inbox@bps-sberbank.by',
                            'Other' => null,
                            'SocialNetworks' => [
                                [
                                    'Network' => 'VK',
                                    'URL' => 'URL'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Param fail branch
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Test-params-branch-id-fail',
            'Accessibilities' => [
                [
                    'Description' => 'Test'
                ]
            ],
            'PostalAddress' => [
                'StreetName' => 'ул. Маяковского',
                'BuildingNumber' => '5',
            ],
            'Information' => [
                [
                    'Segment' => 'Businessese - fail',
                ]
            ]
        ];
    }
}
