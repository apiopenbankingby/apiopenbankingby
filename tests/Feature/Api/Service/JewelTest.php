<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\JewelFactory;
use Tests\TestCase;

class JewelTest extends TestCase
{
    /**
     * Index service jewel
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $jewel = JewelFactory::create();
        $bank->services->service()->attach([$jewel->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=jewel")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Branches',
                    'Type',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'AdditionalData',
                    'Jewel' => [
                        'Coins' => [
                            'Coin' => [[
                                'Name',
                                'Value',
                                'Quality',
                                'SellingPrice',
                                'Currency',
                                'DateTime',
                                'Descriptions',
                                'CoinMaterial'
                            ]]
                        ],
                        'Stones' => [
                            'Stone' => [
                                [
                                    'Material',
                                    'Weight',
                                    'SellingPrice',
                                    'Certificate',
                                    'Form',
                                    'Color',
                                    'Clarity',
                                    'DateTime',
                                    'Currency'
                                ]
                            ]
                        ],
                        'PreciousMetals' => [
                            'PreciousMetal' => [[
                                'Material',
                                'Weight',
                                'Sample',
                                'SellingPrice',
                                'Currency',
                                'PurchasePrice',
                                'DateTime'
                            ]]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Store service jewel
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service jewel
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service jewel
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $jewel = JewelFactory::create();
        $bank->services->service()->attach([$jewel->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$jewel->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service jewel
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $jewel = JewelFactory::create();
        $bank->services->service()->attach([$jewel->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$jewel->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete service jewel
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $jewel = JewelFactory::create();
        $bank->services->service()->attach([$jewel->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$jewel->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service jewel
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param service jewel
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'Test-id-service-jewel',
            'Type' => 'Jewel',
            'Name' => 'Jewel name',
            'Segment' => 'Business',
            'DateTime' => '2018-10-01 19:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description Jewel',
            'URL' => 'URL:Jewel',
            'Jewel' => [
                'Coin' => [
                    [
                        'Name' => '75 лет освобождения Беларуси от немецко-фашистских захватчиков',
                        'Value' => 21,
                        'Quality' => 'пруф',
                        'SellingPrice' => 115,
                        'Currency' => 'BYN',
                        'DateTime' => '2018-10-01 19:00:00',
                        'CoinMaterial' => [
                            [
                                'Material' => 'Platinum',
                                'Sample' => 10
                            ]
                        ]
                    ],
                    [
                        'Name' => 'освобождения Беларуси от немецко-фашистских захватчиков',
                        'Value' => 20,
                        'Quality' => 'пруф',
                        'SellingPrice' => 115,
                        'Currency' => 'BYN',
                        'DateTime' => '2018-10-01 19:00:00'
                    ]
                ],
                'Stone' => [
                    [
                        'Material' => 'Platinum',
                        'Weight' => 100,
                        'SellingPrice' => 80,
                        'Currency' => 'BYN'
                    ]
                ],
                'PreciousMetal' => [
                    [
                        'Material' => 'Platinum',
                        'Weight' => 100,
                        'Sample' => 90,
                        'SellingPrice' => 80,
                        'Currency' => 'BYN',
                        'PurchasePrice' => 9
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service jewel
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Test-id-service-jewel',
            'Type' => 'Card',
            'Name' => 'Jewel name',
            'Segment' => 'Business',
            'DateTime' => '2018-10-01 19:00:00',
            'CurrentStatus' => 'Active on',
            'Jewel' => [
                'Coin' => [
                    [
                        'Name' => '75 лет освобождения Беларуси от немецко-фашистских захватчиков',
                        'Value' => 21,
                        'Quality' => 'пруф',
                        'SellingPrice' => 115
                    ]
                ]
            ]
        ];
    }
}
