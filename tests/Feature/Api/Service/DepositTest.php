<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\DepositFactory;
use Tests\TestCase;

class DepositTest extends TestCase
{
    /**
     * Index service deposit
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $deposit = DepositFactory::create();
        $bank->services->service()->attach([$deposit->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=deposit")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Type',
                    'Branches',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'Deposit' => [
                        'DepositType',
                        'IsEarlyWithdrawalInterest',
                        'Currency' => [
                            [
                                'Currency',
                                'Terms' => [
                                    'Term' => [[
                                        'AmountMin',
                                        'AmountMax',
                                        'TaxIncluded',
                                        'AdditionalData',
                                        'Description',
                                        'Period' => [
                                            [
                                                'PeriodMin',
                                                'PeriodMax',
                                                'PeriodType',
                                                'Interest' => [
                                                    [
                                                        'InterestRate',
                                                        'Rate',
                                                        'Description'
                                                    ]
                                                ],
                                                'Replenishment' => [
                                                    'Replenishment',
                                                    'AmountMin',
                                                    'AmountMax',
                                                    'Description'
                                                ]
                                            ]
                                        ]
                                    ]]
                                ]
                            ]
                        ],
                        'EarlyWithdrawal' => [
                            'WithdrawalType',
                            'IsPartitionalWithdrawal',
                            'EarlyWithdrawalFee',
                            'Description'
                        ],
                        'Capitalization' => [
                            'CapitalizationType',
                            'Description'
                        ],
                        'ContactDetails' => [
                            'Name',
                            'PhoneNumber',
                            'MobileNumber',
                            'FaxNumber',
                            'EmailAddress',
                            'Other',
                            'SocialNetworks' => [
                                [
                                    'Network',
                                    'URL'
                                ],
                            ]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Delete service deposit
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $deposit = DepositFactory::create();
        $bank->services->service()->attach([$deposit->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$deposit->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service deposit
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store service deposit
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service deposit
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service deposit
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $deposit = DepositFactory::create();
        $bank->services->service()->attach([$deposit->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$deposit->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service deposit
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $deposit = DepositFactory::create();
        $bank->services->service()->attach([$deposit->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$deposit->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Param service deposit
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'Test-id-service-deposit',
            'Type' => 'Deposit',
            'Name' => 'Deposit name_test',
            'Segment' => 'Business',
            'DateTime' => '2019-04-11 14:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description Deposit',
            'URL' => 'URL:Deposit',
            'Deposit' => [
                'DepositType' => 'UrgentRevocable',
                'IsEarlyWithdrawalInterest' => false,
                'Currency' => [
                    [
                        'Currency' => 'BYN',
                        'Terms' => [
                            [
                                'AmountMin' => 150,
                                'AmountMax' => 20000,
                                'TaxIncluded' => false,
                                'AdditionalData' => false,
                                'Description' => null,
                                'Period' => [
                                    [
                                        'PeriodMin' => 6,
                                        'PeriodMax' => 6,
                                        'PeriodType' => 'Month',
                                        'Interest' => [
                                            [
                                                'InterestRate' => 'Refinance',
                                                'Rate' => -4.2,
                                                'Description' => 'Переменная годовая процентная ставка изменяется в случае изменения базового показателя со следующего дня после его изменения'
                                            ]
                                        ],
                                        'Replenishment' => [
                                            'Replenishment' => true,
                                            'AmountMin' => 0,
                                            'AmountMax' => 1000,
                                            'Description' => 'До дня наступления последнего месяца хранения'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'AmountMin' => 150,
                                'AmountMax' => 20000,
                                'TaxIncluded' => false,
                                'AdditionalData' => false,
                                'Description' => null,
                                'Period' => [
                                    [
                                        'PeriodMin' => 3,
                                        'PeriodMax' => 3,
                                        'PeriodType' => 'Month',
                                        'Interest' => [
                                            [
                                                'InterestRate' => 'Overnight',
                                                'Rate' => -4.2,
                                                'Description' => 'Переменная годовая процентная ставка изменяется в случае изменения базового показателя со следующего дня после его изменения'
                                            ]
                                        ],
                                        'Replenishment' => [
                                            'Replenishment' => true,
                                            'AmountMin' => 0,
                                            'AmountMax' => 1000,
                                            'Description' => 'До дня наступления последнего месяца хранения'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'EarlyWithdrawal' => [
                    'WithdrawalType' => 'WithoutLoses',
                    'IsPartitionalWithdrawal' => false,
                    'EarlyWithdrawalFee' => '0',
                    'Description' => null
                ],
                'Capitalization' => [
                    'CapitalizationType' => 'NoCapitalization',
                    'Description' => null
                ],
                'ContactDetails' => [
                    'Name' => 'test',
                    'PhoneNumber' => '+375172899090',
                    'MobileNumber' => null,
                    'FaxNumber' => null,
                    'EmailAddress' => null,
                    'Other' => null,
                    'SocialNetworks' => [
                        [
                            'Network' => 'Facebook',
                            'URL' => '@priorbankby'
                        ],
                        [
                            'Network' => 'Viber',
                            'URL' => '375172899090'
                        ]
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service deposit
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Test-id-service-deposit',
            'Type' => 'Loan',
            'Name' => 'Deposit name_test',
            'Segment' => 'Business',
            'Deposit' => [
                'DepositType' => 'UrgentRevocable',
                'IsEarlyWithdrawalInterest' => false
            ]
        ];
    }
}
