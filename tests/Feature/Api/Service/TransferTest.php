<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\TransferFactory;
use Tests\TestCase;

class TransferTest extends TestCase
{
    /**
     * Index transfer
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $transfer = TransferFactory::create();
        $bank->services->service()->attach([$transfer->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=transfer")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Type',
                    'Branches',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'AdditionalData',
                    'Transfer' => [
                        'TransferSystem',
                        'AdditionalServices',
                        'CanReceive',
                        'Currency' => [
                            [
                                'Currency',
                                'Terms' => [
                                    "Term" => [[
                                        'CountryDestination',
                                        'AmountMin',
                                        'TransferPeriod',
                                        'FreeRate',
                                        'FreeAmount',
                                        'AdditionalData',
                                        'AmountMax'
                                    ]]
                                ]
                            ]
                        ],
                        'ContactDetails' => [
                            'Name',
                            'MobileNumber',
                            'FaxNumber',
                            'EmailAddress',
                            'Other'
                        ],
                        'Availability' => [
                            'Access24Hours',
                            'IsRestricted',
                            'SameAsOrganization',
                            'Description',
                            'StandardAvailability' => [
                                'Day' => [
                                    [
                                        'DayCode',
                                        'OpeningTime',
                                        'ClosingTime',
                                        'Break'
                                    ]
                                ]
                            ],
                            'NonStandardAvailability' => [
                                [
                                    'Name',
                                    'FromDate',
                                    'ToDate',
                                    'Description',
                                    'Day' => [
                                        [
                                            'DayCode',
                                            'OpeningTime',
                                            'ClosingTime',
                                            'Break'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Delete service transfer
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $transfer = TransferFactory::create();
        $bank->services->service()->attach([$transfer->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$transfer->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service transfer
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store service transfer
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service transfer
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service transfer
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $transfer = TransferFactory::create();
        $bank->services->service()->attach([$transfer->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$transfer->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service transfer
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $transfer = TransferFactory::create();
        $bank->services->service()->attach([$transfer->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$transfer->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Param service transfer
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'test-deposit-id-service',
            'Type' => 'Transfer',
            'Name' => 'Transfer name',
            'Segment' => 'Business',
            'DateTime' => '2019-01-01 20:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description Transfer',
            'URL' => 'URL=>Transfer',
            'Transfer' => [
                'TransferSystem' => 'Hutki',
                'AdditionalServices' => [
                    'Доставка чека домой - 3 руб'
                ],
                'CanReceive' => true,
                'Currency' => [
                    [
                        'Currency' => 'BYN',
                        'Terms' => [
                            [
                                'CountryDestination' => 'AZ',
                                'AmountMin' => 10,
                                'AmountMax' => 100
                            ]
                        ]
                    ]
                ],
                'ContactDetails' => [
                    'Name' => 'Name - test Transfer',
                    'PhoneNumber' => '1234567',
                    'SocialNetworks' => [
                        [
                            'Network' => 'Twitter',
                            'URL' => 'twitter.com'
                        ]
                    ]
                ],
                'Availability' => [
                    'Access24Hours' => true,
                    'IsRestricted' => true,
                    'SameAsOrganization' => false,
                    'StandardAvailability' => [
                        'Day' => [
                            [
                                'DayCode' => '01',
                                'OpeningTime' => '20:00:00',
                                'ClosingTime' => '20:00:00',
                                'Break' => [
                                    [
                                        'BreakFromTime' => '20:00:00',
                                        'BreakToTime' => '20:00:00'
                                    ]
                                ]
                            ],
                            [
                                'DayCode' => '04',
                                'OpeningTime' => '20:00:00',
                                'ClosingTime' => '20:00:00'
                            ]
                        ]
                    ],
                    'NonStandardAvailability' => [
                        [
                            'Name' => 'Test - test no standard',
                            'FromDate' => '2019-01-01 20:00:00',
                            'ToDate' => '2019-01-01 20:00:00',
                            'Day' => [
                                [
                                    'DayCode' => '02',
                                    'OpeningTime' => '20:00:00',
                                    'ClosingTime' => '20:00:00',
                                    'Break' => [
                                        [
                                            'BreakFromTime' => '20:00:00',
                                            'BreakToTime' => '20:00:00'
                                        ]
                                    ]
                                ],
                                [
                                    'DayCode' => '03',
                                    'OpeningTime' => '20:00:00',
                                    'ClosingTime' => '20:00:00'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service transfer
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'test-deposit-id-service',
            'Type' => 'Loan',
            'Name' => 'Transfer name',
            'Segment' => 'Business',
            'DateTime' => '2019-01-01 20:00:00',
            'CurrentStatus' => 'Active',
            'Transfer' => [
                'TransferSystem' => 'Hutki',
                'AdditionalServices' => [
                    'Доставка чека домой - 3 руб'
                ],
                'CanReceive' => true
            ]
        ];
    }
}
