<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\CurrencyExchangeFactory;
use Tests\TestCase;

class CurrencyExchangeTest extends TestCase
{
    /**
     * Index service currencyExchange
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $currencyExchange = CurrencyExchangeFactory::create();
        $bank->services->service()->attach([$currencyExchange->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=currencyExchange")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Branches',
                    'Type',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'AdditionalData',
                    'CurrencyExchange' => [
                        'Terms' => [
                            'Term' => [[
                                'ExchangeType',
                                'SourceCurrency',
                                'TargetCurrency',
                                'Scale',
                                'ExchangeRate',
                                'AmountMin',
                                'AmountMax',
                                'Direction',
                                'DateTime',
                                'UpdatedDateTime',
                                'AdditionalData'
                            ]]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Store service currencyExchange
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service currencyExchange
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service currencyExchange
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $currencyExchange = CurrencyExchangeFactory::create();
        $bank->services->service()->attach([$currencyExchange->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$currencyExchange->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service currencyExchange
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $currencyExchange = CurrencyExchangeFactory::create();
        $bank->services->service()->attach([$currencyExchange->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$currencyExchange->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete service currencyExchange
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $currencyExchange = CurrencyExchangeFactory::create();
        $bank->services->service()->attach([$currencyExchange->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$currencyExchange->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service currencyExchange
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param service currencyExchange
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => "Test-id-service-currency",
            'Type' => 'CurrencyExchange',
            'Name' => 'CurrencyExchange name',
            'Segment' => 'Individual',
            'DateTime' => '2019-08-15 12:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description CurrencyExchange',
            'URL' => 'URL=>CurrencyExchange',
            'CurrencyExchange' => [
                'Terms' => [
                    [
                        'ExchangeType' => 'Online',
                        'SourceCurrency' => 'USD',
                        'TargetCurrency' => 'BYN',
                        'ScaleCurrency' => 10,
                        'Scale' => 1,
                        'ExchangeRate' => 2.26,
                        'AmountMin' => 5.5,
                        'AmountMax' => 500,
                        'Direction' => 'buy',
                        'DateTime' => '2019-08-15 12:00:00',
                        'UpdatedDateTime' => '2019-08-15 12:00:00',
                        'AdditionalData' => null
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service currencyExchange
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => "Test-id-service-currency",
            'Type' => 'Card',
            'Name' => 'CurrencyExchange name',
            'Segment' => 'Individual',
            'URL' => 'URL=>CurrencyExchange',
            'CurrencyExchange' => [
                'Terms' => [
                    [
                        'ExchangeType' => 'Online',
                        'SourceCurrency' => 'USD'
                    ]
                ]
            ]
        ];
    }
}
