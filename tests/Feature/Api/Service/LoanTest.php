<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\LoanFactory;
use Tests\TestCase;

class LoanTest extends TestCase
{
    /**
     * Index service loan
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $loan = LoanFactory::create();
        $bank->services->service()->attach([$loan->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=loan")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Branches',
                    'Type',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'AdditionalData',
                    'Loan' => [
                        'LoanCategory',
                        'LoanType',
                        'LoanForm',
                        'ApplicationType',
                        'AdditionalServices',
                        'IsRenewable',
                        'Currency' => [
                            [
                                'Currency',
                                'Terms' => [
                                    'Term' => [[
                                        'AmountMin',
                                        'AmountMax',
                                        'Period',
                                        'OwnFunds',
                                        'PeriodType',
                                        'AdditionalData',
                                        'Description',
                                        'Interest' => [
                                            'InterestRate',
                                            'Rate',
                                            'EarlyRepaymentInterest',
                                            'GracePeriodInterest',
                                            'Description'
                                        ],
                                        'Guarantee'
                                    ]]
                                ]
                            ]
                        ],
                        'ContactDetails' => [
                            'Name',
                            'PhoneNumber',
                            'MobileNumber',
                            'FaxNumber',
                            'EmailAddress',
                            'Other'
                        ],
                        'Eligibility' => [
                            [
                                'AgeMin',
                                'AgeMax',
                                'IsSalaryProject',
                                'IncomeMin',
                                'WorkPeriod',
                                'IsRelativeIncome',
                                'IsResident',
                                'NeedIncomeCertificate',
                                'IsRetire',
                                'IsEntrepreneur',
                                'AdditionalData',
                                'Description'
                            ]
                        ],
                        'Repayment' => [
                            [
                                'RepaymentType',
                                'GracePeriod',
                                'AdditionalData',
                                'Description',
                                'IsEarlyRepayment'
                            ]
                        ],
                        'Partners' => [
                            'PartnerCategory' => [[
                                'Category',
                                'Partner' => [
                                    [
                                        'PartnerUIN'
                                    ]
                                ]
                            ]]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Delete service loan
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $loan = LoanFactory::create();
        $bank->services->service()->attach([$loan->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$loan->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service loan
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "api/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store service loan
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service loan
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service loan
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $loan = LoanFactory::create();
        $bank->services->service()->attach([$loan->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$loan->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service loan
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $loan = LoanFactory::create();
        $bank->services->service()->attach([$loan->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$loan->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Param service loan
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'test-id-service-loan',
            'Type' => 'Loan',
            'Name' => 'Loan name',
            'Segment' => 'Business',
            'DateTime' => '2019-09-09 10:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description Loan',
            'URL' => 'URL:Loan',
            'Loan' => [
                'LoanCategory' => 'Consumer',
                'LoanType' => [
                    'Building'
                ],
                'LoanForm' => [
                    'Cashless'
                ],
                'ApplicationType' => [
                    'Bank'
                ],
                'AdditionalServices' => [
                    'InsuranceProperty'
                ],
                'IsRenewable' => false,
                'Currency' => [
                    [
                        'Currency' => 'BYN',
                        'Terms' => [
                            [
                                'AmountMin' => 10,
                                'AmountMax' => 20,
                                'Period' => 20,
                                'PeriodType' => 'Day',
                                'Interest' => [
                                    'InterestRate' => 'FixRate',
                                    'Rate' => 10,
                                    'EarlyRepaymentInterest' => 11
                                ],
                                'Guarantee' => [
                                    [
                                        'GuaranteeType' => 'Penalty'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'ContactDetails' => [
                    'Name' => 'Name - test Transfer',
                    'PhoneNumber' => '1234567',
                    'MobileNumber' => null,
                    'FaxNumber' => null,
                    'EmailAddress' => null,
                    'Other' => null,
                    'SocialNetworks' => [
                        [
                            'Network' => 'Twitter',
                            'URL' => 'twitter.com'
                        ]
                    ]
                ],
                'Eligibility' => [
                    [
                        'AgeMin' => 10,
                        'AgeMax' => 20,
                        'IsSalaryProject' => true,
                        'IncomeMin' => 10,
                        'WorkPeriod' => 5,
                        'NeedIncomeCertificate' => true
                    ]
                ],
                'Repayment' => [
                    [
                        'RepaymentType' => 'EqualPayments',
                        'IsEarlyRepayment' => true
                    ]
                ],
                'Partners' => [
                    [
                        'Category' => 'Минск',
                        'Partner' => [
                            [
                                'PartnerUIN' => 12345
                            ]
                        ]
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service loan
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'test-id-service-loan',
            'Type' => 'Jewel',
            'Name' => 'Loan name',
            'Segment' => 'Business',
            'DateTime' => '2019-09-09 10:00:00',
            'CurrentStatus' => 'Active',
            'Loan' => [
                'LoanCategory' => 'Consumer',
                'LoanType' => [
                    'Building'
                ],
                'LoanForm' => [
                    'Cashless'
                ]
            ]
        ];
    }
}
