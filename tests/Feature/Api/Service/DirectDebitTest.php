<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\DirectDebitFactory;
use Tests\TestCase;

class DirectDebitTest extends TestCase
{
    /**
     * Index service directDebit
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $directDebit = DirectDebitFactory::create();
        $bank->services->service()->attach([$directDebit->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=directDebit")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Type',
                    'Branches',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'DirectDebit' => [
                        'Terms' => [
                            'Term' => [[
                                'FeeType',
                                'FeeRate',
                                'FeeAmount',
                                'FeeAmountMin',
                                'FeeAmountMax',
                                'Description'
                            ]]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Store service directDebit
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service directDebit
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service directDebit
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $directDebit = DirectDebitFactory::create();
        $bank->services->service()->attach([$directDebit->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$directDebit->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service directDebit
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $directDebit = DirectDebitFactory::create();
        $bank->services->service()->attach([$directDebit->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$directDebit->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete service directDebit
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $directDebit = DirectDebitFactory::create();
        $bank->services->service()->attach([$directDebit->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$directDebit->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service directDebit
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param service directDebit
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'test-id-service-direct-debit',
            'Type' => 'DirectDebit',
            'Name' => 'DirectDebit name',
            'Segment' => 'Business',
            'DateTime' => '2019-10-05 13:10:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description DirectDebit',
            'URL' => 'URL:DirectDebit',
            'DirectDebit' => [
                'Terms' => [
                    [
                        'FeeType' => 'TransactionFee',
                        'FeeRate' => 2,
                        'FeeAmount' => 10,
                        'FeeAmountMin' => 1,
                        'FeeAmountMax' => 5.99,
                        'Description' => 'Описание прямого дебетования'
                    ],
                    [
                        'FeeType' => 'TransactionFee',
                        'Frequency' => 'OneTime'
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service directDebit
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'test-id-service-direct-debit',
            'Type' => 'Deposit',
            'Name' => 'DirectDebit name',
            'Segment' => 'Business - 1',
            'DirectDebit' => [
                'Terms' => [
                    [
                        'FeeType' => 'TransactionFee',
                        'FeeRate' => 2,
                        'FeeAmount' => 10,
                        'FeeAmountMin' => 1,
                        'FeeAmountMax' => 5.99,
                        'Description' => 'Описание прямого дебетования'
                    ]
                ]
            ]
        ];
    }
}
