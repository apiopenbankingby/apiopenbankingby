<?php

namespace Tests\Feature\Api\Service;

use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\CardFactory;
use Tests\TestCase;

class CardTest extends TestCase
{
    /**
     * Index service card
     */
    public function testIndex()
    {
        $card = CardFactory::create();
        $bank = BankFactory::create();
        $bank->services->service()->attach([$card->id]);

        $this->json('GET', "/banks/{$bank->id}/services?type=card")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Type',
                    'Branches',
                    'Name',
                    'Segment',
                    'DateTime',
                    'CurrentStatus',
                    'Description',
                    'URL',
                    'AdditionalData',
                    'Card' => [
                        'PaymentCardSystem',
                        'CardCategory',
                        'CardType',
                        'ConsumerCard',
                        'CardDataEntryMode',
                        'ForeignCountryPayments',
                        'CustomDesign',
                        'InternetPayments',
                        'InstantIssue',
                        'UrgentIssue',
                        'PermanentBalance',
                        'IssueAdditionalCard',
                        'Notifications',
                        'InterestRateOnBalance',
                        'BankPartners',
                        'MobilePayments',
                        'FeeCharges' => [
                            [
                                'IssueFee',
                                'InstantIssueFee',
                                'UrgentIssueFee',
                                'AdditionalCardIssueFee',
                                'NotificationFee',
                                'ReIssueFee' => [
                                    'ChangeDataFee',
                                    'VOIDFee'
                                ],
                                'ReplenishmentFee' => [
                                    [
                                        'FeeAmountMin',
                                        'FeeAmountMax',
                                        'FeeRate',
                                        'Description'
                                    ]
                                ],
                                'ServiceFee' => [
                                    'FeeAnnual',
                                    'FeeMonthly',
                                    'FeeOneTime',
                                    'FeeEIssue'
                                ],
                                'BalanceCheckFee' => [
                                    'IssuerBank' => [
                                        'FeeAmount'
                                    ],
                                    'BankPartners' => [
                                        'FeeAmount'
                                    ],
                                    'OtherBank' => [
                                        'FeeAmount'
                                    ],
                                    'NonResidentBank' => [
                                        'FeeAmount'
                                    ]
                                ],
                                'BankWithdrawalFee' => [
                                    'IssuerBank' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'BankPartners' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'OtherBank' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'NonResidentBank' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ]
                                ],
                                'ATMWithdrawalFee' => [
                                    'IssuerATM' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'BankPartnersATM' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'OtherATM' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ],
                                    'NonResidentATM' => [
                                        'FeeRate',
                                        'FeeAmountMin',
                                        'FeeAmountMax'
                                    ]
                                ],
                                'CustomFee'
                            ]
                        ],
                        'CardExpirationDate' => [
                            'Months'
                        ],
                        'Currency',
                        'CashBack' => [
                            'AmountMin',
                            'AmountMax',
                            'Frequency',
                            'Partners' => [
                                [
                                    'PartnerCategory' => [[
                                        'Category',
                                        'Partner' => [['PartnerName']]
                                    ]]
                                ]
                            ]
                        ],
                        'LoyaltyProgram' => [
                            'LoyaltyServices',
                            'Partners' => [
                                [
                                    'PartnerCategory' => [
                                        [
                                            'Category',
                                            'Partner' => [
                                                [
                                                    'PartnerName'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'PaymentSystemLoyaltyURL'
                        ],
                        'Instalment' => [
                            'Partners' => [
                                [
                                    'PartnerCategory' => [
                                        [
                                            'Category',
                                            'Partner' => [
                                                [
                                                    'PartnerName'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'Terms' => [
                                'Term' => [[
                                    'AmountMin',
                                    'AmountMax',
                                    'PeriodMin',
                                    'PeriodMax',
                                    'Description',
                                    'Interest' => [
                                        'Rate'
                                    ]
                                ]]
                            ]
                        ],
                        'CreditProduct' => [
                            'Terms' => [
                                'Term' => [[
                                    'AmountMin',
                                    'AmountMax',
                                    'Period',
                                    'Description',
                                    'Interest' => [
                                        'InterestRate',
                                        'Rate',
                                        'EarlyRepaymentInterest',
                                        'GracePeriodInterest',
                                        'Description'
                                    ],
                                    'Guarantee' => [
                                        [
                                            'GuaranteeType',
                                            'Description'
                                        ]
                                    ]
                                ]]
                            ]
                        ]
                    ]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Store service card
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $branch = BranchFactory::create();
        $bank->branch()->attach([$branch->id]);

        $params = $this->params(['Branches' => [$branch->id]]);

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertOk();
    }

    /**
     * Store fail service card
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/services", $params)
            ->assertStatus(422);
    }

    /**
     * Update service card
     */
    public function testUpdate()
    {
        $card = CardFactory::create();
        $bank = BankFactory::create();
        $bank->services->service()->attach([$card->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/services/{$card->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail service card
     */
    public function testUpdateFail()
    {
        $card = CardFactory::create();
        $bank = BankFactory::create();
        $bank->services->service()->attach([$card->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/services/{$card->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete service card
     */
    public function testDelete()
    {
        $card = CardFactory::create();
        $bank = BankFactory::create();
        $bank->services->service()->attach([$card->id]);

        $this->json('DELETE', "/banks/{$bank->id}/services/{$card->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail service card
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/services/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param service card
     *
     * @param array $overrides new params
     * @return array
     */
    private function params($overrides = []): array
    {
        return array_merge([
            'Id' => 'Id-testCard_service',
            'Type' => 'Card',
            'Name' => 'Card name',
            'Segment' => 'Business', 'Individual',
            'DateTime' => '2019-05-12 12:00:00',
            'CurrentStatus' => 'Active',
            'Description' => 'Description Card',
            'URL' => 'URL:Card',
            'AdditionalData' => null,
            'Card' => [
                'PaymentCardSystem' => ['Visa'],
                'CardCategory' => 'ClassicCard',
                'CardType' => 'CreditCard',
                'ConsumerCard' => 'CorporateCard',
                'CardDataEntryMode' => 'MagneticStripeProximityEMV',
                'ForeignCountryPayments' => true,
                'CustomDesign' => false,
                'InternetPayments' => true,
                'InstantIssue' => false,
                'UrgentIssue' => false,
                'PermanentBalance' => false,
                'IssueAdditionalCard' => true,
                'Notifications' => true,
                'InterestRateOnBalance' => 0,
                'BankPartners' => ['Partners'],
                'FeeCharges' => [
                    [
                        'IssueFee' => 0,
                        'InstantIssueFee' => null,
                        'UrgentIssueFee' => null,
                        'AdditionalCardIssueFee' => null,
                        'NotificationFee' => 1.99,
                        'ReIssueFee' => [
                            'ChangeDataFee' => 2,
                            'VOIDFee' => 3
                        ],
                        'ReplenishmentFee' => [
                            [
                                'FeeAmountMin' => 100,
                                'FeeAmountMax' => 100,
                                'FeeRate' => 1.99,
                                'Description' => null
                            ]
                        ],
                        'ServiceFee' => [
                            'FeeAnnual' => 0,
                            'FeeMonthly' => 0,
                            'FeeOneTime' => 0,
                            'FeeEIssue' => 0
                        ],
                        'BalanceCheckFee' => [
                            'IssuerBank' => [
                                'FeeAmount' => 10
                            ],
                            'BankPartners' => [
                                'FeeAmount' => 10
                            ],
                            'OtherBank' => [
                                'FeeAmount' => 5
                            ],
                            'NonResidentBank' => [
                                'FeeAmount' => 10
                            ]
                        ],
                        'BankWithdrawalFee' => [
                            'IssuerBank' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 100
                            ],
                            'BankPartners' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ],
                            'OtherBank' => [
                                'FeeRate' => 5,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 20
                            ],
                            'NonResidentBank' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ]
                        ],
                        'ATMWithdrawalFee' => [
                            'IssuerATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 100
                            ],
                            'BankPartnersATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ],
                            'OtherATM' => [
                                'FeeRate' => 5,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 20
                            ],
                            'NonResidentATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ]
                        ],
                        'CustomFee' => []
                    ]
                ],
                'CardExpirationDate' => [
                    'Months' => 36
                ],
                'Currency' => 'BYN',
                'CashBack' => [
                    'AmountMin' => 1,
                    'AmountMax' => 10,
                    'Frequency' => 'Quarterly',
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'LoyaltyProgram' => [
                    'LoyaltyServices' => [
                        'LoyaltyServices'
                    ],
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'PaymentSystemLoyaltyURL' => null
                ],
                'Instalment' => [
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'Terms' => [
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 1000,
                            'PeriodMin' => 2,
                            'PeriodMax' => 12,
                            'Description' => 'В магазинах-партнерах',
                            'Interest' => [
                                'Rate' => 1.0e-6
                            ]
                        ],
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 1000,
                            'PeriodMin' => 2,
                            'PeriodMax' => 2,
                            'Description' => 'Во всех магазинах',
                            'Interest' => [
                                'Rate' => 1.0e-6
                            ]
                        ]
                    ]
                ],
                'CreditProduct' => [
                    'Terms' => [
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 999.99,
                            'Period' => 12,
                            'Description' => 'В магазинах-партнерах',
                            'Interest' => [
                                'InterestRate' => 'Refinance',
                                'Rate' => 1.0e-6,
                                'EarlyRepaymentInterest' => null,
                                'GracePeriodInterest' => null,
                                'Description' => null
                            ],
                            'Guarantee' => [
                                [
                                    'GuaranteeType' => [
                                        'NoGuarantee'
                                    ],
                                    'Description' => null
                                ]
                            ]
                        ],
                        [
                            'AmountMin' => 10,
                            'AmountMax' => 99,
                            'Period' => 2,
                            'Description' => 'Во всех магаззинах',
                            'Interest' => [
                                'InterestRate' => 'Refinance',
                                'Rate' => 1.0e-6,
                                'EarlyRepaymentInterest' => null,
                                'GracePeriodInterest' => null,
                                'Description' => null
                            ],
                            'Guarantee' => [
                                [
                                    'GuaranteeType' => [
                                        'NoGuarantee'
                                    ],
                                    'Description' => null
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ], $overrides);
    }

    /**
     * Param fail service card
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Id-testCard_service',
            'Type' => 'Test',
            'Name' => 'Card name',
            'Segment' => 'Business', 'Individual',
            'DateTime' => '2019-05-12 12:00:00',
            'CurrentStatus' => 'Active',
            'Card' => [
                'PaymentCardSystem' => 'Visa'
            ]
        ];
    }
}
