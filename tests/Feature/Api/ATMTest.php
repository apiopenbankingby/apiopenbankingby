<?php

namespace Tests\Feature\Api;

use App\Factory\ATMFactory;
use App\Factory\BankFactory;
use Tests\TestCase;

class ATMTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->delete(['branches', 'atms', 'banks']);
    }

    /**
     * Index ATM
     */
    public function testIndex()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $this->json('GET', "/banks/{$bank->id}/atms")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [[
                    'Id',
                    'Type',
                    'BaseCurrency',
                    'Currency',
                    'Cards',
                    'CurrentStatus',
                    'Description',
                    'AdditionalData',
                    'Location' => [
                        'StreetName',
                        'BuildingNumber',
                        'Department',
                        'PostCode',
                        'TownName',
                        'CountrySubDivision',
                        'Country',
                        'AddressLine',
                        'Description',
                        'GeographicCoordinates' => [
                            'Geolocation' => [
                                'Latitude',
                                'Longitude'
                            ]
                        ]
                    ],
                    'ServicesATM' => [
                        'ServiceATM' => [[
                            'ServiceType',
                            'Description',
                            'AdditionalData'
                        ]],
                    ],
                    'Availability' => [
                        'Access24Hours',
                        'IsRestricted',
                        'SameAsOrganization',
                        'Description',
                        'StandardAvailability' => [
                            'Day' => [[
                                'DayCode',
                                'OpeningTime',
                                'ClosingTime',
                                'Break' => [[
                                    'BreakFromTime',
                                    'BreakToTime'
                                ]]
                            ]]
                        ],
                        'NonStandardAvailability' => [[
                            'Name',
                            'FromDate',
                            'ToDate',
                            'Description',
                            'Day' => [[
                                'DayCode',
                                'OpeningTime',
                                'ClosingTime',
                                'Break' => [[
                                    'BreakFromTime',
                                    'BreakToTime'
                                ]]
                            ]]
                        ]]
                    ],
                    'ContactDetails' => [
                        'PhoneNumber',
                        'Other'
                    ],
                    'Accessibilities' => [
                        'Accessibility' => [[
                            'Type',
                            'Description'
                        ]],
                    ],
                    'CurrencyExchange' => [[
                        'ExchangeType',
                        'SourceCurrency',
                        'TargetCurrency',
                        'UnitCurrency',
                        'ExchangeRate',
                        'AmountMin',
                        'AmountMax',
                        'Direction',
                        'DateTime'
                    ]]
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }

    /**
     * Show ATM
     */
    public function testShow()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $this->json('GET', "/banks/{$bank->id}/atms/{$atm->id}")
            ->assertOk()
            ->assertJsonStructure([
                'Id',
                'Type',
                'BaseCurrency',
                'Currency',
                'Cards',
                'CurrentStatus',
                'Description',
                'AdditionalData',
                'Location' => [
                    'StreetName',
                    'BuildingNumber',
                    'Department',
                    'PostCode',
                    'TownName',
                    'CountrySubDivision',
                    'Country',
                    'AddressLine',
                    'Description',
                    'GeographicCoordinates' => [
                        'Geolocation' => [
                            'Latitude',
                            'Longitude'
                        ]
                    ]
                ],
                'ServicesATM' => [
                    'ServiceATM' => [[
                        'ServiceType',
                        'Description',
                        'AdditionalData'
                    ]],
                ],
                'Availability' => [
                    'Access24Hours',
                    'IsRestricted',
                    'SameAsOrganization',
                    'Description',
                    'StandardAvailability' => [
                        'Day' => [[
                            'DayCode',
                            'OpeningTime',
                            'ClosingTime',
                            'Break' => [[
                                'BreakFromTime',
                                'BreakToTime'
                            ]]
                        ]]
                    ],
                    'NonStandardAvailability' => [[
                        'Name',
                        'FromDate',
                        'ToDate',
                        'Description',
                        'Day' => [[
                            'DayCode',
                            'OpeningTime',
                            'ClosingTime',
                            'Break' => [[
                                'BreakFromTime',
                                'BreakToTime'
                            ]]
                        ]]
                    ]]
                ],
                'ContactDetails' => [
                    'PhoneNumber',
                    'Other'
                ],
                'Accessibilities' => [
                    'Accessibility' => [[
                        'Type',
                        'Description'
                    ]],
                ],
                'CurrencyExchange' => [[
                    'ExchangeType',
                    'SourceCurrency',
                    'TargetCurrency',
                    'UnitCurrency',
                    'ExchangeRate',
                    'AmountMin',
                    'AmountMax',
                    'Direction',
                    'DateTime'
                ]]
            ]);
    }

    /**
     * Show fail ATM
     */
    public function testShowFail()
    {
        $this->json('GET', "/banks/123456/atms/123456")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Store ATM
     */
    public function testStore()
    {
        $bank = BankFactory::create();
        $params = $this->params();

        $this->json('POST', "/banks/{$bank->id}/atms", $params)
            ->assertOk();
    }

    /**
     * Store fail ATM
     */
    public function testStoreFail()
    {
        $bank = BankFactory::create();
        $params = $this->paramsFail();

        $this->json('POST', "/banks/{$bank->id}/atms", $params)
            ->assertStatus(422);
    }

    /**
     * Update ATM
     */
    public function testUpdate()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $params = $this->params();

        $this->json('PUT', "/banks/{$bank->id}/atms/{$atm->id}", $params)
            ->assertOk();
    }

    /**
     * Update fail ATM
     */
    public function testUpdateFail()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $params = $this->paramsFail();

        $this->json('PUT', "/banks/{$bank->id}/atms/{$atm->id}", $params)
            ->assertStatus(422);
    }

    /**
     * Delete ATM
     */
    public function testDelete()
    {
        $bank = BankFactory::create();
        $atm = ATMFactory::create();
        $bank->atm()->attach([$atm->id]);

        $this->json('DELETE', "/banks/{$bank->id}/atms/{$atm->id}")
            ->assertStatus(204);
    }

    /**
     * Delete fail ATM
     */
    public function testDeleteFail()
    {
        $this->json('DELETE', "/banks/1234567/atms/1234567")
            ->assertNotFound()
            ->assertJson([
                'message' => 'Not Found',
                'status' => 404
            ]);
    }

    /**
     * Param ATM
     *
     * @return array
     */
    private function params(): array
    {
        return [
            'Id' => 'Test-params-atm-id',
            'Type' => 'ATM',
            'BaseCurrency' => 'USD',
            'Currency' => ['BYN'],
            'Cards' => ['MasterCard'],
            'CurrentStatus' => 'On',
            'Location' => [
                'StreetName' => 'бульвар имени Банкомата',
                'BuildingNumber' => '13',
                'Department' => null,
                'PostCode' => '220145',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '5',
                'Country' => 'AF',
                'Geolocation' => [
                    'Latitude' => 54.86857,
                    'Longitude' => 28.569547
                ]
            ],
            'ServicesATM' => [
                [
                    'ServiceType' => 'CashWithdrawal',
                    'Description' => null,
                    'AdditionalData' => null
                ]
            ],
            'Availability' => [
                'Access24Hours' => false,
                'IsRestricted' => false,
                'SameAsOrganization' => false,
                'Description' => 'sample availability desc',
                'StandardAvailability' => [
                    'Day' => [
                        [
                            'DayCode' => '01',
                            'OpeningTime' => '08:00:00',
                            'ClosingTime' => '17:00:00',
                            'Break' => [
                                [
                                    'BreakFromTime' => '13:30:00',
                                    'BreakToTime' => '14:30:00'
                                ]
                            ]
                        ]
                    ]
                ],
                'NonStandardAvailability' => [
                    [
                        'Name' => 'ExtraDayOff',
                        'FromDate' => '2019-05-09 00:00:00',
                        'ToDate' => '2019-05-09 00:00:00',
                        'Description' => 'sample extra day off description',
                        'Day' => [
                            [
                                'DayCode' => '01',
                                'OpeningTime' => '08:00:00',
                                'ClosingTime' => '17:00:00',
                                'Break' => [
                                    [
                                        'BreakFromTime' => '13:30:00',
                                        'BreakToTime' => '14:30:00'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'ContactDetails' => [
                'PhoneNumber' => '+375232984532',
                'Other' => 'sample other'
            ],
            'Accessibilities' => [
                [
                    'Type' => 'Braille',
                    'Description' => null
                ]
            ],
            'CurrencyExchange' => [
                [
                    'ExchangeType' => 'Online',
                    'SourceCurrency' => 'BYN',
                    'TargetCurrency' => 'USD',
                    'UnitCurrency' => 2000,
                    'ExchangeRate' => 2.26,
                    'AmountMin' => 5.5,
                    'AmountMax' => 500,
                    'Direction' => 'buy',
                    'DateTime' => '2019-05-12 15:20:36',
                    'UpdatedDateTime' => '2019-05-12 15:20:36'
                ]
            ]
        ];
    }

    /**
     * Param fail ATM
     *
     * @return array
     */
    private function paramsFail(): array
    {
        return [
            'Id' => 'Test-params-atm-id-fail',
            'Type' => 'ATM',
            'BaseCurrency' => 'USD',
            'Currency' => ['BYN'],
            'Cards' => ['belcart', 'MasterCard'],
            'CurrentStatus' => 'On',
            'Location' => [
                'StreetName' => 'бульвар имени Банкомата',
                'BuildingNumber' => '13',
                'Department' => null,
                'PostCode' => '220145',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '5',
                'Country' => 'AF',
                'Geolocation' => [
                    'Latitude' => 54.86857,
                    'Longitude' => 28.569547
                ]
            ],
            'ServicesATM' => [
                [
                    'ServiceType' => 'CashWithdrawal',
                    'Description' => null,
                    'AdditionalData' => null
                ]
            ],
            'Availability' => [
                'Access24Hours' => false,
                'IsRestricted' => false,
                'SameAsOrganization' => false,
                'Description' => 'sample availability desc',
                'StandardAvailability' => [
                    'Day' => [
                        [
                            'DayCode' => '01',
                            'OpeningTime' => '08:00:00',
                            'ClosingTime' => '17:00:00',
                            'Break' => [
                                [
                                    'BreakFromTime' => '13:30:00',
                                    'BreakToTime' => '14:30:00'
                                ]
                            ]
                        ]
                    ]
                ],
                'NonStandardAvailability' => [
                    [
                        'Name' => 'ExtraDayOff',
                        'FromDate' => '2019-05-09 00:00:00',
                        'ToDate' => '2019-05-09 00:00:00',
                        'Description' => 'sample extra day off description',
                        'Day' => [
                            [
                                'DayCode' => '01',
                                'OpeningTime' => '08:00:00',
                                'ClosingTime' => '17:00:00',
                                'Break' => [
                                    [
                                        'BreakFromTime' => '13:30:00',
                                        'BreakToTime' => '14:30:00'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'ContactDetails' => [
                'PhoneNumber' => '+375232984532',
                'Other' => 'sample other'
            ],
            'Accessibilities' => [
                [
                    'Type' => 'Braille',
                    'Description' => null
                ]
            ],
            'CurrencyExchange' => [
                [
                    'ExchangeType' => 'Online',
                    'SourceCurrency' => 'BYN',
                    'TargetCurrency' => 'USD',
                    'UnitCurrency' => 2000,
                    'ExchangeRate' => 2.26,
                    'AmountMin' => 5.5,
                    'AmountMax' => 500,
                    'Direction' => 'buy',
                    'DateTime' => '2019-05-12 15:20:36',
                    'UpdatedDateTime' => '2019-05-12 15:20:36'
                ]
            ]
        ];
    }
}
