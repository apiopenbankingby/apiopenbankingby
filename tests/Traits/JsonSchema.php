<?php


namespace Tests\Traits;


use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use JsonSchema\Validator;

trait JsonSchema
{
    private $apiDocs = '/api-docs/api-docs.json';

    /**
     * Generate json schema
     *
     * @param string $url
     * @param string $method
     * @param int $responseCode
     * @param string $elementType
     * @return array
     */
    public function generateJsonSchema(string $url, string $method, int $responseCode, string $elementType = 'object'): array
    {

        $SWAGGER_PATH = storage_path() . $this->apiDocs;
        $json = json_decode(file_get_contents($SWAGGER_PATH), true);

        $properties = $json['paths'][$url][$method]['responses'][$responseCode]['content']['application/json']['schema'];
        $components = $json['components'];

        return [
            'type' => $elementType,
            'properties' => isset($properties['properties']) ? $properties['properties'] : $properties,
            'components' => $components,
        ];
    }

    /**
     * Json validator
     *
     * @param $response
     * @param array $jsonSchema
     * @return Validator
     */
    public function jsonValidator($response, array $jsonSchema): Validator
    {
        $schemaStorage = new SchemaStorage();
        $schemaStorage->addSchema('file://mySchema', $jsonSchema);

        $validator = new Validator(new Factory($schemaStorage));
        $validator->validate($response, $jsonSchema);

        return $validator;
    }
}
