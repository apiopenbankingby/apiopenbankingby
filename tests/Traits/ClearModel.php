<?php


namespace Tests\Traits;


use Illuminate\Support\Facades\DB;

trait ClearModel
{
    /**
     * Clear table
     *
     * @param array $models
     */
    public function delete(array $models): void
    {
        foreach ($models as $model) {
            DB::table($model)->delete();
        }
    }
}
