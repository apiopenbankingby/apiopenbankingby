<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

- [Migrations](https://laravel.com/docs/6.0/migrations)
- [Providers](https://laravel.com/docs/6.0/providers)
- [Seeding & Factories](https://laravel.com/docs/6.0/seeding)
- [Testing](https://laravel.com/docs/6.0/testing)

Beside Laravel, this project uses other tools like:

- [darkaonline/l5-swagger](https://github.com/DarkaOnLine/L5-Swagger)
- [tucker-eric/eloquentfilter](https://github.com/Tucker-Eric/EloquentFilter)
- Many more to discover.

## Installation

Development environment requirements :
- [Docker](https://www.docker.com) >= 17.06 CE
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Deployer](https://deployer.org)

Setting up your development environment on your local machine :
```bash
$ git clone https://dmitryneverovski@bitbucket.org/teamontid/api-server.git
$ cd api-server
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ cp .env.example .env
$ docker-compose up -d
$ composer install
$ php artisan key:generate

$ php artisan serve
```

```bash
$ php artisan migrate
-OR-
$ php artisan migrate:fresh --seed
```

1. Now you can access the application via [http://127.0.0.1:8000](http://127.0.0.1:8000).
2. Documentation [http://127.0.0.1:8000/api/docs](http://127.0.0.1:8000/api/docs).

### Deploy on server
Requires globally installed php deployer. There is only one installation option.
Ansible prepared .env config for current .env.testing during the first deployment.
Use only project user: openbanking
```bash
cd ~/openapi/init
#for first run git pull, or cd ~/openapi/current
deployer deploy
```

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
