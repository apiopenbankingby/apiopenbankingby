<?php

namespace App\Models;

use App\Models\Directory\CapitalizationType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Capitalization
 *
 * @package App\Models
 */
class Capitalization extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['capitalizationType'];

    /**
     * Capitalization type
     *
     * @return HasOne
     */
    public function capitalizationType(): HasOne
    {
        return $this->hasOne(CapitalizationType::class, 'id', 'type_id');
    }
}
