<?php

namespace App\Models;

use App\Models\Directory\AccessibilityType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Accessibility
 *
 * @package App\Models
 */
class Accessibility extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accessibility';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'description'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['accessibilityType'];

    /**
     * Type
     *
     * @return HasOne
     */
    public function accessibilityType(): HasOne
    {
        return $this->hasOne(AccessibilityType::class, 'id', 'type');
    }
}
