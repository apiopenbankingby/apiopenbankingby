<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ContactDetails
 *
 * @package App\Models
 */
class ContactDetails extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone_number',
        'mobile_number',
        'fax_number',
        'email_address',
        'other'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['socialNetwork'];

    /**
     * Social network
     *
     * @return BelongsToMany
     */
    public function socialNetwork(): belongsToMany
    {
        return $this->belongsToMany(SocialNetwork::class, 'contact_detail_social', 'c_id', 's_id');
    }
}
