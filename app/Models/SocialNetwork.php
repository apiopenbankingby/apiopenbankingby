<?php

namespace App\Models;

use App\Models\Directory\Network;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class SocialNetwork
 *
 * @package App\Models
 */
class SocialNetwork extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'network',
        'url',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['description' => 'array'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['social'];

    /**
     * Social
     *
     * @return HasOne
     */
    public function social(): HasOne
    {
        return $this->hasOne(Network::class, 'id', 'network');
    }
}
