<?php

namespace App\Models\Service;

use App\Filtres\ServiceFilter;
use App\Models\Card\Card;
use App\Models\CurrencyExchange\CurrencyExchange;
use App\Models\Deposit\Deposit;
use App\Models\DirectDebit\DirectDebit;
use App\Models\Directory\Segment;
use App\Models\BaseModel;
use App\Models\Directory\ServiceStatus;
use App\Models\Directory\ServiceType;
use App\Models\Jewel\Jewel;
use App\Models\Loan\Loan;
use App\Models\Transfer\Transfer;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

/**
 * Class ServicesService
 *
 * @property mixed id
 * @package App\Models\Service
 * @method static servicesId($serviceId)
 * @method static findOrFail($id)
 */
class ServicesService extends BaseModel
{
    use Filterable;

    /**
     * Model filter to default
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(ServiceFilter::class);
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'branches',
        'type_id',
        'name',
        'segment_id',
        'date_time',
        'status_id',
        'description',
        'url',
        'additional_data',
        'deposit_id',
        'loan_id',
        'card_id',
        'jewel_id',
        'transfer_id',
        'currency_exchange_id',
        'direct_debit_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'branches' => 'array',
        'additional_data' => 'array',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_time'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'type',
        'segment',
        'currentStatus',
        'deposit',
        'loan',
        'card',
        'jewel',
        'transfer',
        'currencyExchange',
        'directDebit'
    ];

    /**
     * Scope a query only services id
     * @param Builder $query
     * @param $serviceId
     * @return Builder
     */
    public function scopeServicesId(Builder $query, $serviceId): Builder
    {
        return $query->whereHas('services', function (Builder $query) use ($serviceId) {
            $query->where('s_id', $serviceId);
        });
    }

    /**
     * services
     *
     * @return BelongsToMany
     */
    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class, 'services_service', 'ss_id', 's_id')->without('service');
    }

    /**
     * Type
     *
     * @return HasOne
     */
    public function type(): HasOne
    {
        return $this->hasOne(ServiceType::class, 'id', 'type_id');
    }

    /**
     * Segment
     *
     * @return HasOne
     */
    public function segment(): HasOne
    {
        return $this->hasOne(Segment::class, 'id', 'segment_id');
    }

    /**
     * Current status
     *
     * @return HasOne
     */
    public function currentStatus(): HasOne
    {
        return $this->hasOne(ServiceStatus::class, 'id', 'status_id');
    }

    /**
     * Current status
     *
     * @return HasOne
     */
    public function deposit(): HasOne
    {
        return $this->hasOne(Deposit::class, 'id', 'deposit_id');
    }

    /**
     * Current status
     *
     * @return HasOne
     */
    public function loan(): HasOne
    {
        return $this->hasOne(Loan::class, 'id', 'loan_id');
    }

    /**
     * Current status
     *
     * @return HasOne
     */
    public function card(): HasOne
    {
        return $this->hasOne(Card::class, 'id', 'card_id');
    }

    /**
     * Jewel
     *
     * @return HasOne
     */
    public function jewel(): HasOne
    {
        return $this->hasOne(Jewel::class, 'id', 'jewel_id');
    }

    /**
     * Transfer
     *
     * @return HasOne
     */
    public function transfer(): HasOne
    {
        return $this->hasOne(Transfer::class, 'id', 'transfer_id');
    }

    /**
     * Currency Exchange
     *
     * @return HasOne
     */
    public function currencyExchange(): HasOne
    {
        return $this->hasOne(CurrencyExchange::class, 'id', 'currency_exchange_id');
    }

    /**
     * DirectDebit
     *
     * @return HasOne
     */
    public function directDebit(): HasOne
    {
        return $this->hasOne(DirectDebit::class, 'id', 'direct_debit_id');
    }
}
