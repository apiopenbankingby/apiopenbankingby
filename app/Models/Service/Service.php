<?php

namespace App\Models\Service;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Service
 *
 * @package App\Models\Service
 */
class Service extends BaseModel
{
    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['service'];

    /**
     * Service
     *
     * @return BelongsToMany
     */
    public function service(): BelongsToMany
    {
        return $this->belongsToMany(ServicesService::class, 'services_service', 's_id', 'ss_id');
    }
}
