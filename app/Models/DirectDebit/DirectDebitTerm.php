<?php

namespace App\Models\DirectDebit;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class DirectDebitTerm
 *
 * @package App\Models\DirectDebit
 */
class DirectDebitTerm extends BaseModel
{
    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Term
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(DirectDebitTermsTerm::class, 'direct_debit_terms_term', 'd_id', 'term_id');
    }
}
