<?php

namespace App\Models\DirectDebit;

use App\Models\BaseModel;
use App\Models\Directory\FeeType;
use App\Models\Directory\Frequency;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DirectDebitTermsTerm
 *
 * @package App\Models\DirectDebit
 */
class DirectDebitTermsTerm extends BaseModel
{
    protected $casts = [
        'fee_rate' => 'float',
        'fee_amount' => 'float',
        'fee_amount_min' => 'float',
        'fee_amount_max' => 'float',
        'additional_data' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fee_type_id',
        'fee_rate',
        'fee_amount',
        'fee_amount_min',
        'fee_amount_max',
        'frequency_id',
        'description',
        'additional_data',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['feeType', 'frequency'];

    /**
     * Fee type
     *
     * @return hasOne
     */
    public function feeType(): HasOne
    {
        return $this->hasOne(FeeType::class, 'id', 'fee_type_id');
    }

    /**
     * Frequency
     *
     * @return hasOne
     */
    public function frequency(): HasOne
    {
        return $this->hasOne(Frequency::class, 'id', 'frequency_id');
    }
}
