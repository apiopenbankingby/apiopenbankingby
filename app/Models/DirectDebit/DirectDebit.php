<?php

namespace App\Models\DirectDebit;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DirectDebit
 *
 * @package App\Models\DirectDebit
 */
class DirectDebit extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['terms_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['terms'];

    /**
     * Terms
     *
     * @return HasOne
     */
    public function terms(): HasOne
    {
        return $this->hasOne(DirectDebitTerm::class, 'id', 'terms_id');
    }
}
