<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class GeographicCoordinates
 *
 * @package App\Models
 */
class GeographicCoordinates extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['geolocation_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['geolocation'];

    /**
     * Geographic coordinates
     *
     * @return HasOne
     */
    public function geolocation(): HasOne
    {
        return $this->hasOne(Geolocation::class, 'id', 'geolocation_id');
    }
}
