<?php

namespace App\Models\Branch;

use App\Models\Availability;
use App\Models\BaseModel;
use App\Models\ContactDetails;
use App\Models\Directory\Segment;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class BranchInformation
 *
 * @package App\Models\Branch
 */
class BranchInformation extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_informations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['segment_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['segment', 'availability', 'contactDetails'];

    /**
     * @return HasOne
     */
    public function segment(): HasOne
    {
        return $this->hasOne(Segment::class, 'id', 'segment_id');
    }

    /**
     * Availability
     *
     * @return BelongsToMany
     */
    public function availability(): BelongsToMany
    {
        return $this->belongsToMany(Availability::class, 'branch_information_availability', 'b_id', 'a_id');
    }

    /**
     * Contact details
     *
     * @return BelongsToMany
     */
    public function contactDetails(): BelongsToMany
    {
        return $this->belongsToMany(ContactDetails::class, 'branch_information_contact', 'b_id', 'c_id');
    }
}
