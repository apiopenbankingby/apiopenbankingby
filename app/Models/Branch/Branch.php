<?php

namespace App\Models\Branch;

use App\Models\Accessibilities;
use App\Models\Location;
use App\Models\BaseModel;
use App\Models\Service\Service;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Branch
 *
 * @property mixed id
 * @package App\Models\Branch
 * @method static findOrFail($id)
 */
class Branch extends BaseModel
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'location_id',
        'id',
        'accessibility_id',
        'service_id',
        'wifi',
        'equeue'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'wifi' => 'boolean',
        'equeue' => 'boolean'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['services', 'location', 'accessibilities', 'information'];

    /**
     * Services
     *
     * @return HasOne
     */
    public function services(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    /**
     * Location
     *
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

    /**
     * Accessibility
     *
     * @return HasOne
     */
    public function accessibilities(): HasOne
    {
        return $this->hasOne(Accessibilities::class, 'id', 'accessibility_id');
    }

    /**
     * information
     *
     * @return belongsToMany
     */
    public function information(): BelongsToMany
    {
        return $this->belongsToMany(BranchInformation::class, 'branch_information', 'b_id', 'i_id');
    }
}
