<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Availability
 *
 * @package App\Models
 */
class Availability extends BaseModel
{
    protected $table = 'availabilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_restricted',
        'access_24_hours',
        'same_as_organization',
        'description',
        'standard_availability_id'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'standardAvailability',
        'nonStandardAvailability'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'access_24_hours' => 'boolean',
        'is_restricted' => 'boolean',
        'same_as_organization' => 'boolean',
    ];

    /**
     * Standard availability
     *
     * @return HasOne
     */
    public function standardAvailability(): HasOne
    {
        return $this->hasOne(StandardAvailability::class, 'id', 'standard_availability_id');
    }

    /**
     * non standard Availability
     *
     * @return BelongsToMany
     */
    public function nonStandardAvailability(): BelongsToMany
    {
        return $this->belongsToMany(NonStandardAvailability::class, 'availability_non_standard_availability', 's_id', 'n_id');
    }
}
