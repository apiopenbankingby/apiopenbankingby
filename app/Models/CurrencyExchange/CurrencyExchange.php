<?php

namespace App\Models\CurrencyExchange;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CurrencyExchange
 *
 * @package App\Models\CurrencyExchange
 */
class CurrencyExchange extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services_currency_exchanges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['curr_ech_terms_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['terms'];

    /**
     * Services Currency Exchange terms
     *
     * @return HasOne
     */
    public function terms(): HasOne
    {
        return $this->hasOne(CurrencyExchangeTerms::class, 'id', 'curr_ech_terms_id');
    }
}
