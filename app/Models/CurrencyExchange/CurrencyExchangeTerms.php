<?php

namespace App\Models\CurrencyExchange;

use App\Models\CurrencyExchange as Currency;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CurrencyExchangeTerms extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currency_exchange_terms';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Terms
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(Currency::class, 'currency_exchange_terms_term', 'terms_id', 'term_id');
    }
}
