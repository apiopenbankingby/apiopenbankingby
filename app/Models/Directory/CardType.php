<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CardType
 * Card types
 *
 * @package App\Models\Directory
 */
class CardType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
