<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class PartnerCategoryType
 *
 * @package App
 */
class PartnerCategoryType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
