<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class Network
 * Social networks
 *
 * @package App
 */
class Network extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
