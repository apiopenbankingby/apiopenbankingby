<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class LoanType
 *
 * @package App\Models\Directory
 */
class LoanType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
