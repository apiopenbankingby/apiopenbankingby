<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class AdditionalService
 *
 * @package App\Models\Directory
 */
class AdditionalService extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
