<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CardCategory
 * Card Categories
 *
 * @package App\Models\Directory
 */
class CardCategory extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
