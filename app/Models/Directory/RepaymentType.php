<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class RepaymentType
 *
 * @package App
 */
class RepaymentType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
