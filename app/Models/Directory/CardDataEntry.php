<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CardDataEntry
 * Card Features
 *
 * @package App\Models\Directory
 */
class CardDataEntry extends BaseModel
{
    protected $table = 'card_data_entries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
