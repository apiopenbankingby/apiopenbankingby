<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class DepositType
 *
 * @package App\Models\Directory
 */
class DepositType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
