<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class ApplicationType
 *
 * @package App\Models\Directory
 */
class ApplicationType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
