<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class Frequency
 * Periodicity
 *
 * @package App\Models\Directory
 */
class Frequency extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
