<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class RateType
 *
 * @package App\Models\Directory
 */
class RateType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
