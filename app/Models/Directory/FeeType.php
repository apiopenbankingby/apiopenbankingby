<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class FeeType
 *
 * @package App\Models\Directory
 */
class FeeType extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
