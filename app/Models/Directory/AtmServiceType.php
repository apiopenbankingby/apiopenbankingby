<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class AtmServiceType
 *
 * @package App\Models\Directory
 */
class AtmServiceType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
