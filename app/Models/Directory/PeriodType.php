<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class PeriodType
 *
 * @package App\Models\Directory
 */
class PeriodType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
