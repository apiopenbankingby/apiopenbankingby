<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class ExchangeType
 * Currency Exchange Types
 *
 * @package App\Models\Directory
 */
class ExchangeType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
