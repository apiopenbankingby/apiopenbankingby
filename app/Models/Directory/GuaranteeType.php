<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class GuaranteeType
 *
 * @package App\Models\Directory
 */
class GuaranteeType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
