<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class WithdrawalType
 *
 * @package App\Models\Directory
 */
class WithdrawalType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
