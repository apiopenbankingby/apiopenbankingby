<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class PaymentSystem
 * Payment Card Systems
 *
 * @package App\Models\Directory
 */
class PaymentSystem extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
