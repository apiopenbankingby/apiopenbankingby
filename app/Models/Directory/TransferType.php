<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class TransferType
 *
 * @package App\Models\Directory
 */
class TransferType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
