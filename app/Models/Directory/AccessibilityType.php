<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class AccessibilityType
 * Accessibility for people with disabilities
 *
 * @package App\Models\Directory
 */
class AccessibilityType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
