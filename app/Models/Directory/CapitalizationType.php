<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CapitalizationType
 *
 * @package App\Models\Directory
 */
class CapitalizationType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
