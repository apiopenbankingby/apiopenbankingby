<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class JewelType
 *
 * @package App\Models\Directory
 */
class JewelType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
