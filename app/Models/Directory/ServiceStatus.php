<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class ServiceStatus
 * Statuses
 *
 * @package App\Models\Directory
 */
class ServiceStatus extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
