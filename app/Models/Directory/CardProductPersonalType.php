<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CardProductPersonalType
 * Types of Personal Cards
 *
 * @package App\Models\Directory
 */
class CardProductPersonalType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
