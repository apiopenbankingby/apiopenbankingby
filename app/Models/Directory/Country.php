<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class Country
 *
 * @package App\Models\Directory
 */
class Country extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['number'];
}
