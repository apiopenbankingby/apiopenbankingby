<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

class OtherDataType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
