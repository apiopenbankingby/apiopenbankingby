<?php

namespace App\Models\Directory;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceType
 *
 * @package App\Models\Directory
 */
class ServiceType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
