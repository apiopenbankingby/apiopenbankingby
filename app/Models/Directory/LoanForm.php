<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class LoanForm
 *
 * @package App\Models\Directory
 */
class LoanForm extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
