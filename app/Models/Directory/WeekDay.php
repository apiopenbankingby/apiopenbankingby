<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

class WeekDay extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'code'];
}
