<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class Segment
 *
 * @package App\Models\Directory
 */
class Segment extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
