<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CardProductType
 * Types of cards by customer categories
 *
 * @package App\Models\Directory
 */
class CardProductCustomerType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
