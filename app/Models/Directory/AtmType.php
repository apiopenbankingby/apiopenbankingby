<?php

namespace App\Models\Directory;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AtmType
 *
 * @package App\Models\Directory
 */
class AtmType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];
}
