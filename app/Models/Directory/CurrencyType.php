<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class CurrencyType
 *
 * @package App\Models\Directory
 */
class CurrencyType extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'number'];
}
