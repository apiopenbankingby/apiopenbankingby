<?php

namespace App\Models\Directory;

use App\Models\BaseModel;

/**
 * Class LoanCategory
 *
 * @package App\Models\Directory
 */
class LoanCategory extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code'];
}
