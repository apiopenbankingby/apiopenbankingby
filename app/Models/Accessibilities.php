<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Accessibilities
 *
 * @package App\Models
 */
class Accessibilities extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accessibilities';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['accessibility'];

    /**
     * Accessibility
     *
     * @return BelongsToMany
     */
    public function accessibility(): BelongsToMany
    {
        return $this->belongsToMany(Accessibility::class, 'accessibilities_accessibility', 'acesibts_id', 'acesibt_id');
    }
}
