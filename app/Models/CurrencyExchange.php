<?php

namespace App\Models;

use App\Models\Directory\CurrencyType;
use App\Models\Directory\ExchangeType;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class CurrencyExchange
 *
 * @package App\Models
 */
class CurrencyExchange extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'additional_data' => 'array',
        'exchange_rate' => 'float',
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exchange_type_id',
        'source_currency_id',
        'target_currency_id',
        'unit_currency',
        'scale_currency',
        'exchange_rate',
        'amount_min',
        'amount_max',
        'direction',
        'date_time',
        'updated_date_time',
        'additional_data',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_time', 'updated_date_time'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'exchangeType',
        'sourceCurrency',
        'targetCurrency'
    ];

    /**
     * ExchangeType
     *
     * @return HasOne
     */
    public function exchangeType(): HasOne
    {
        return $this->hasOne(ExchangeType::class, 'id', 'exchange_type_id');
    }

    /**
     * Currency
     *
     * @return HasOne
     */
    public function sourceCurrency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'source_currency_id');
    }

    /**
     * Currency
     *
     * @return HasOne
     */
    public function targetCurrency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'target_currency_id');
    }
}
