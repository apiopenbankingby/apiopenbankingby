<?php

namespace App\Models;

use App\Filters\BankFilter;
use App\Models\Atm\Atm;
use App\Models\Service\Service;
use App\Models\Branch\Branch;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Bank
 *
 * @property mixed services
 * @property mixed id
 * @package App\Models
 * @method static findOrFail($id)
 * @method static filter(array $input)
 */
class Bank extends BaseModel
{
    use Filterable;

    /**
     * Model filter to default
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(BankFilter::class);
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'bicfi',
        'clearing_system_identification',
        'clearing_system_member_identification',
        'code',
        'proprietary',
        'member_identification',
        'name',
        'legal_entity_identifier',
        'location_id',
        'service_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['location'];

    /**
     * Postal Address
     *
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

    /**
     * ATM
     *
     * @return BelongsToMany
     */
    public function atm(): BelongsToMany
    {
        return $this->belongsToMany(Atm::class, 'bank_atm', 'b_id', 'a_id');
    }

    /**
     * Branch
     *
     * @return BelongsToMany
     */
    public function branch(): BelongsToMany
    {
        return $this->belongsToMany(Branch::class, 'bank_branch', 'b_id', 'branch_id');
    }

    /**
     * Services
     *
     * @return HasOne
     */
    public function services(): HasOne
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
}
