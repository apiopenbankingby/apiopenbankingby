<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BaseModel
 *
 * @package App\Models
 */
Class BaseModel extends Model
{
    use SoftDeletes;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get DateTime
     *
     * @param $value
     * @return mixed
     */
    public function getDateTimeAttribute($value)
    {
        return $value;
    }

    /**
     * Get FromDate
     *
     * @param $value
     * @return mixed
     */
    public function getFromDateAttribute($value)
    {
        return $value;
    }

    /**
     * Get ToDate
     *
     * @param $value
     * @return mixed
     */
    public function getToDateAttribute($value)
    {
        return $value;
    }

    /**
     * Get UpdatedDateTime
     *
     * @param $value
     * @return mixed
     */
    public function getUpdatedDateTimeAttribute($value)
    {
        return $value;
    }
}
