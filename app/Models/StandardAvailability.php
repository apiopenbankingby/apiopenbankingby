<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class StandardAvailability
 *
 * @package App\Models
 */
class StandardAvailability extends BaseModel
{
    protected $table = 'standard_availabilities';

    /**
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'day'
    ];

    /**
     * Day
     *
     * @return BelongsToMany
     */
    public function day(): BelongsToMany
    {
        return $this->belongsToMany(Day::class, 'day_standard_availability', 'a_id', 'day_id');
    }
}
