<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;
use App\Models\Capitalization;
use App\Models\ContactDetails;
use App\Models\Currency;
use App\Models\Directory\DepositType;
use App\Models\EarlyWithdrawal;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

/**
 * Class Deposit
 *
 * @package App\Models\Deposit
 */
class Deposit extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'capitalization_id',
        'contact_id',
        'early_withdrawal_id',
        'is_early_withdrawal_interest',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'depositType',
        'currency',
        'capitalization',
        'earlyWithdrawal',
        'contactDetails'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['is_early_withdrawal_interest' => 'boolean'];

    /**
     * Deposit types
     *
     * @return HasOne
     */
    public function depositType(): HasOne
    {
        return $this->hasOne(DepositType::class, 'id', 'type_id');
    }

    /**
     * Currency
     *
     * @return BelongsToMany
     */
    public function currency(): belongsToMany
    {
        return $this->belongsToMany(Currency::class, 'deposit_currency', 'd_id', 'c_id')->with('depositTerm');
    }

    /**
     * Capitalization
     *
     * @return HasOne
     */
    public function capitalization(): HasOne
    {
        return $this->hasOne(Capitalization::class, 'id', 'capitalization_id');
    }

    /**
     * Early withdrawal
     *
     * @return HasOne
     */
    public function earlyWithdrawal(): HasOne
    {
        return $this->hasOne(EarlyWithdrawal::class, 'id', 'early_withdrawal_id');
    }

    /**
     * Contact details
     *
     * @return HasOne
     */
    public function contactDetails(): HasOne
    {
        return $this->hasOne(ContactDetails::class, 'id', 'contact_id');
    }
}
