<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;

class DepositReplenishment extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'replenishment',
        'amount_min',
        'amount_max',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'replenishment' => 'boolean',
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];
}
