<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class DepositTerm
 *
 * @package App\Models\Deposit
 */
class DepositTerm extends BaseModel
{
    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Term
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(DepositTermsTerm::class, 'deposit_terms_term', 'terms_id', 'term_id');
    }
}
