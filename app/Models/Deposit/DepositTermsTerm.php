<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class DepositTermsTerm
 *
 * @package App\Models\Deposit
 */
class DepositTermsTerm extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deposit_terms_terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_min',
        'amount_max',
        'tax_included',
        'additional_data',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'additional_data' => 'array',
        'tax_included' => 'boolean',
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['period'];

    /**
     * Period
     *
     * @return BelongsToMany
     */
    public function period(): BelongsToMany
    {
        return $this->belongsToMany(DepositPeriod::class, 'deposit_terms_term_period', 'term_id', 'period_id');
    }
}
