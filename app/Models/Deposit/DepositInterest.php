<?php

namespace App\Models\Deposit;

use App\Models\Directory\RateType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DepositInterest
 *
 * @package App\Models\Deposit
 */
class DepositInterest extends BaseModel
{
    protected $casts = [
        'rate' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate_id',
        'rate',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interestRate'];

    /**
     * Interest Rate
     *
     * @return HasOne
     */
    public function interestRate(): HasOne
    {
        return $this->hasOne(RateType::class, 'id', 'rate_id');
    }
}
