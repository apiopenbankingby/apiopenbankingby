<?php

namespace App\Models\Deposit;

use App\Models\Directory\PeriodType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class DepositPeriod
 *
 * @package App\Models\Deposit
 */
class DepositPeriod extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'period_min',
        'period_max',
        'type_id',
        'replenishment_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['periodType', 'replenishment', 'interest'];

    protected $casts = [
        'period_min' => 'integer',
        'period_max' => 'integer',
    ];

    /**
     * Period type
     *
     * @return HasOne
     */
    public function periodType(): HasOne
    {
        return $this->hasOne(PeriodType::class, 'id', 'type_id');
    }

    /**
     * Replenishment
     *
     * @return HasOne
     */
    public function replenishment(): HasOne
    {
        return $this->hasOne(DepositReplenishment::class, 'id', 'replenishment_id');
    }

    /**
     * Interest
     *
     * @return BelongsToMany
     */
    public function interest(): BelongsToMany
    {
        return $this->belongsToMany(DepositInterest::class, 'deposit_period_interest', 'period_id', 'interest_id');
    }
}
