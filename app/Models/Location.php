<?php

namespace App\Models;

use App\Models\Directory\Country;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Location
 * @package App\Models
 */
class Location extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_name',
        'building_number',
        'department',
        'post_code',
        'town_name',
        'country_sub_division',
        'country_id',
        'address_line',
        'description',
        'geographic_coordinates_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['geographicCoordinates', 'country'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'address_line' => 'array',
        'description' => 'array',
    ];

    /**
     * Geographic coordinates
     *
     * @return HasOne
     */
    public function geographicCoordinates(): HasOne
    {
        return $this->hasOne(GeographicCoordinates::class, 'id', 'geographic_coordinates_id');
    }

    /**
     * Country
     *
     * @return HasOne
     */
    public function country(): HasOne
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
}
