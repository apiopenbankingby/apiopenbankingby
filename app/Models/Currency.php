<?php

namespace App\Models;

use App\Models\Deposit\DepositTerm;
use App\Models\Directory\CurrencyType;
use App\Models\Loan\LoanTerm;
use App\Models\Transfer\TransferTerms;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Currency
 *
 * @package App\Models
 */
class Currency extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['curr_id', 'deposit_term_id', 'loan_term_id', 'transfer_terms_id'];

    /**
     * Currency
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'curr_id');
    }

    /**
     * Deposit term
     *
     * @return HasOne
     */
    public function depositTerm(): HasOne
    {
        return $this->hasOne(DepositTerm::class, 'id', 'deposit_term_id');
    }

    /**
     * Deposit term
     *
     * @return HasOne
     */
    public function loanTerm(): HasOne
    {
        return $this->hasOne(LoanTerm::class, 'id', 'loan_term_id');
    }

    /**
     * Deposit term
     *
     * @return HasOne
     */
    public function transferTerm(): HasOne
    {
        return $this->hasOne(TransferTerms::class, 'id', 'transfer_terms_id');
    }
}
