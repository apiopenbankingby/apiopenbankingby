<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Directory\WeekDay;

/**
 * Class Day
 *
 * @package App\Models
 */
class Day extends BaseModel
{
    /**
     *
     * @var array
     */
    protected $fillable = ['day_code_id', 'opening_time', 'closing_time'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'dayCode',
        'break'
    ];

    /**
     * DayCode
     *
     * @return HasOne
     */
    public function dayCode(): HasOne
    {
        return $this->hasOne(WeekDay::class, 'id', 'day_code_id');
    }

    /**
     * Break
     *
     * @return BelongsToMany
     */
    public function break(): BelongsToMany
    {
        return $this->belongsToMany(DBreak::class, 'break_day', 'day_id', 'break_id');
    }
}
