<?php

namespace App\Models\Jewel;

use App\Models\Directory\CurrencyType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


/**
 * Class Coin
 *
 * @package App\Models\Jewel
 */
class Coin extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'selling_price' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
        'quality',
        'selling_price',
        'curr_id',
        'date_time',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'currency',
        'coinMaterial',
    ];

    protected $dates = ['date_time'];

    /**
     * Currency
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'curr_id');
    }

    /**
     * CoinMaterial
     *
     * @return BelongsToMany
     */
    public function coinMaterial(): BelongsToMany
    {
        return $this->belongsToMany(CoinMaterial::class, 'coin_coin_material', 'coin_id', 'coin_material_id');
    }
}
