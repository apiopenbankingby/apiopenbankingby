<?php

namespace App\Models\Jewel;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Coins
 *
 * @package App\Models\Jewel
 */
class Coins extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coins';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['coin'];

    /**
     * Coin
     *
     * @return BelongsToMany
     */
    public function coin(): BelongsToMany
    {
        return $this->belongsToMany(Coin::class, 'coins_coin', 'coins_id', 'coin_id');
    }
}
