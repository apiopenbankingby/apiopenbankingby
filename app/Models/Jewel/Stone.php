<?php

namespace App\Models\Jewel;

use App\Models\Directory\CurrencyType;
use App\Models\Directory\JewelType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class Stone
 *
 * @package App\Models\Jewel
 */
class Stone extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'weight' => 'float',
        'selling_price' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stone';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'material_id',
        'weight',
        'certificate',
        'form',
        'color',
        'clarity',
        'selling_price',
        'curr_id',
        'date_time'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'currency',
        'material',
    ];

    protected $dates = ['date_time'];

    /**
     * Currency
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'curr_id');
    }

    /**
     * Material
     *
     * @return HasOne
     */
    public function material(): HasOne
    {
        return $this->hasOne(JewelType::class, 'id', 'material_id');
    }
}
