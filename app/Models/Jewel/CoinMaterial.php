<?php

namespace App\Models\Jewel;

use App\Models\Directory\JewelType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CoinMaterial
 *
 * @package App\Models\Jewel
 */
class CoinMaterial extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'sample' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coin_materials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'material_id',
        'sample',
        'custom_metal',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['jewelType'];

    /**
     * JewelType
     *
     * @return HasOne
     */
    public function jewelType(): HasOne
    {
        return $this->hasOne(JewelType::class, 'id', 'material_id');
    }
}
