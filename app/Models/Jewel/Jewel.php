<?php

namespace App\Models\Jewel;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Jewel
 *
 * @package App\Models\Jewel
 */
class Jewel extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'precious_metals_id',
        'coins_id',
        'stones_id'
    ];

    protected $with = ['preciousMetal', 'coins', 'stones'];

    /**
     * PreciousMetal
     *
     * @return HasOne
     */
    public function preciousMetal(): HasOne
    {
        return $this->hasOne(PreciousMetals::class, 'id', 'precious_metals_id');
    }

    /**
     * Coins
     *
     * @return HasOne
     */
    public function coins(): HasOne
    {
        return $this->hasOne(Coins::class, 'id', 'coins_id');
    }

    /**
     * Stones
     *
     * @return HasOne
     */
    public function stones(): HasOne
    {
        return $this->hasOne(Stones::class, 'id', 'stones_id');
    }
}
