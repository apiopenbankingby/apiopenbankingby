<?php

namespace App\Models\Jewel;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Stones
 *
 * @package App\Models\Jewel
 */
class Stones extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stones';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['stone'];

    /**
     * Stone
     *
     * @return BelongsToMany
     */
    public function stone(): BelongsToMany
    {
        return $this->belongsToMany(Stone::class, 'stones_stone', 'stones_id', 'stone_id');
    }
}
