<?php

namespace App\Models\Jewel;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class PreciousMetals
 *
 * @package App\Models\Jewel
 */
class PreciousMetals extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'precious_metals';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['preciousMetal'];

    /**
     * PreciousMetal
     *
     * @return BelongsToMany
     */
    public function preciousMetal(): BelongsToMany
    {
        return $this->belongsToMany(PreciousMetal::class, 'precious_metals_precious_metal', 'precious_metals_id', 'precious_metal_id');
    }
}
