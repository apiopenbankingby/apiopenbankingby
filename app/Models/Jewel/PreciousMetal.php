<?php

namespace App\Models\Jewel;

use App\Models\Directory\CurrencyType;
use App\Models\Directory\JewelType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * Class PreciousMetal
 *
 * @package App\Models\Jewels
 */
class PreciousMetal extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'weight' => 'float',
        'sample' => 'float',
        'selling_price' => 'float',
        'purchase_price' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'precious_metal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'material_id',
        'sample',
        'selling_price',
        'purchase_price',
        'curr_id',
        'weight',
        'date_time',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'currency',
        'material',
    ];

    protected $dates = ['date_time'];

    /**
     * Currency
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'curr_id');
    }

    /**
     * Material
     *
     * @return HasOne
     */
    public function material(): HasOne
    {
        return $this->hasOne(JewelType::class, 'id', 'material_id');
    }
}
