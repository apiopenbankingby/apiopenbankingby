<?php

namespace App\Models;

/**
 * Class DBreak
 *
 * @package App\Models
 */
class DBreak extends BaseModel
{
    protected $table = 'dbreaks';

    /**
     *
     * @var array
     */
    protected $guarded = ['id'];
}
