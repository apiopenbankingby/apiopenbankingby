<?php

namespace App\Models\Loan;

use App\Models\BaseModel;
use App\Models\ContactDetails;
use App\Models\Currency;
use App\Models\Directory\AdditionalService;
use App\Models\Directory\ApplicationType;
use App\Models\Directory\LoanCategory;
use App\Models\Directory\LoanForm;
use App\Models\Directory\LoanType;
use App\Models\Directory\ServiceStatus;
use App\Models\Partner;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Loan
 *
 * @package App\Models
 */
class Loan extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'is_renewable',
        'contact_id',
        'partner_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['is_renewable' => 'boolean'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'loanCategory',
        'loanType',
        'loanForm',
        'applicationType',
        'additionalServices',
        'contactDetails',
        'eligibility',
        'repayment',
        'currency',
        'partners',
    ];

    /**
     * Loan category
     *
     * @return hasOne
     */
    public function loanCategory(): HasOne
    {
        return $this->hasOne(LoanCategory::class, 'id', 'category_id');
    }

    /**
     * Currency
     *
     * @return BelongsToMany
     */
    public function currency(): belongsToMany
    {
        return $this->belongsToMany(Currency::class, 'loan_currency', 'l_id', 'c_id')->with('loanTerm');
    }

    /**
     * Loan type
     *
     * @return BelongsToMany
     */
    public function loanType(): BelongsToMany
    {
        return $this->belongsToMany(LoanType::class, 'loan_type', 'l_id', 't_id');
    }

    /**
     * Loan form
     *
     * @return BelongsToMany
     */
    public function loanForm(): BelongsToMany
    {
        return $this->belongsToMany(LoanForm::class, 'loan_form', 'l_id', 'f_id');
    }

    /**
     * Application type
     *
     * @return BelongsToMany
     */
    public function applicationType(): BelongsToMany
    {
        return $this->belongsToMany(ApplicationType::class, 'loan_application_type', 'l_id', 't_id');
    }

    /**
     * Additional services
     *
     * @return BelongsToMany
     */
    public function additionalServices(): BelongsToMany
    {
        return $this->belongsToMany(AdditionalService::class, 'loan_additional_service', 'l_id', 's_id');
    }

    /**
     * Contact details
     *
     * @return HasOne
     */
    public function contactDetails(): HasOne
    {
        return $this->hasOne(ContactDetails::class, 'id', 'contact_id');
    }

    /**
     * Eligibility
     *
     * @return BelongsToMany
     */
    public function eligibility(): BelongsToMany
    {
        return $this->belongsToMany(Eligibility::class, 'loan_eligibility', 'loan_id', 'eligibility_id');
    }

    /**
     * Repayment
     *
     * @return BelongsToMany
     */
    public function repayment(): BelongsToMany
    {
        return $this->belongsToMany(Repayment::class, 'loan_repayment', 'loan_id', 'repayment_id');
    }

    /**
     * Partners
     *
     * @return HasOne
     */
    public function partners(): HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'partner_id');
    }
}
