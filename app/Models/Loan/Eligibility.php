<?php

namespace App\Models\Loan;

use App\Models\BaseModel;

/**
 * Class Eligibility
 *
 * @package App\Models\Loan
 */
class Eligibility extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'age_min',
        'age_max',
        'is_salary_project',
        'income_min',
        'work_period',
        'is_relative_income',
        'is_resident',
        'need_income_certificate',
        'additional_data',
        'is_retired',
        'is_entrepreneur',
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_salary_project' => 'boolean',
        'is_resident' => 'boolean',
        'is_relative_income' => 'boolean',
        'is_retired' => 'boolean',
        'is_entrepreneur' => 'boolean',
        'need_income_certificate' => 'boolean',
        'age_min' => 'integer',
        'age_max' => 'integer',
        'income_min' => 'float',
        'work_period' => 'integer',
    ];
}
