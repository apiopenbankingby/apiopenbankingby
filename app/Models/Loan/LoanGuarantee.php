<?php

namespace App\Models\Loan;

use App\Models\BaseModel;
use App\Models\Directory\GuaranteeType;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class LoanGuarantee
 *
 * @package App\Models\Loan
 */
class LoanGuarantee extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['guarantee_id', 'description'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['guaranteeType'];

    /**
     * Guarantee type
     *
     * @return hasOne
     */
    public function guaranteeType(): HasOne
    {
        return $this->hasOne(GuaranteeType::class, 'id', 'guarantee_id');
    }
}
