<?php

namespace App\Models\Loan;

use App\Models\Directory\RepaymentType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Repayment
 * Loan repayment
 *
 * @package App\Models\Loan
 */
class Repayment extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'repayment_type_id',
        'grace_period',
        'is_early_repayment',
        'additional_data',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['repaymentType'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_early_repayment' => 'boolean',
        'grace_period' => 'integer',
    ];

    /**
     * RepaymentType
     *
     * @return HasOne
     */
    public function repaymentType(): HasOne
    {
        return $this->hasOne(RepaymentType::class, 'id', 'repayment_type_id');
    }
}
