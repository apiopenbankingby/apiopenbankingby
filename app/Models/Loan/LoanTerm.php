<?php

namespace App\Models\Loan;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class LoanTerm
 *
 * @package App\Models\Loan
 */
class LoanTerm extends BaseModel
{
    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Term
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(LoanTermsTerm::class, 'loan_terms_term', 'terms_id', 'term_id');
    }
}
