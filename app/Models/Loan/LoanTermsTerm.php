<?php

namespace App\Models\Loan;

use App\Models\BaseModel;
use App\Models\Directory\PeriodType;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class LoanTermsTerm
 *
 * @package App
 */
class LoanTermsTerm extends BaseModel
{
    protected $casts = [
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'loan_terms_terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_min',
        'amount_max',
        'period',
        'period_type_id',
        'interest_id',
        'additional_data',
        'own_funds',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interest', 'guarantee', 'periodType'];

    /**
     * Interest
     *
     * @return HasOne
     */
    public function interest(): HasOne
    {
        return $this->hasOne(LoanInterest::class, 'id', 'interest_id');
    }

    /**
     * Guarantee
     *
     * @return BelongsToMany
     */
    public function guarantee(): BelongsToMany
    {
        return $this->belongsToMany(LoanGuarantee::class, 'loan_terms_term_guarantee', 't_id', 'g_id');
    }

    /**
     * Period type
     *
     * @return HasOne
     */
    public function periodType(): HasOne
    {
        return $this->hasOne(PeriodType::class, 'id', 'period_type_id');
    }
}
