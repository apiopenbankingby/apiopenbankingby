<?php

namespace App\Models\Loan;

use App\Models\BaseModel;
use App\Models\Directory\RateType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class LoanInterest
 *
 * @package App\Models\Loan
 */
class LoanInterest extends BaseModel
{
    protected $casts = [
        'rate' => 'float',
        'early_repayment_interest' => 'float',
        'grace_period_interest' => 'integer'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate_id',
        'rate',
        'early_repayment_interest',
        'grace_period_interest',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interestRate'];

    /**
     * Interest Rate
     *
     * @return HasOne
     */
    public function interestRate(): HasOne
    {
        return $this->hasOne(RateType::class, 'id', 'rate_id');
    }
}
