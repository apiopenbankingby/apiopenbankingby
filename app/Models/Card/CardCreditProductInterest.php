<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use App\Models\Directory\RateType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardCreditProductInterest
 *
 * @package App\Models\Card
 */
class CardCreditProductInterest extends BaseModel
{
    protected $casts = [
        'rate' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_credit_product_interests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate_id',
        'rate',
        'grace_period_interest',
        'early_repayment_interest',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interestRate'];

    /**
     * Interest Rate
     *
     * @return HasOne
     */
    public function interestRate(): HasOne
    {
        return $this->hasOne(RateType::class, 'id', 'rate_id');
    }
}
