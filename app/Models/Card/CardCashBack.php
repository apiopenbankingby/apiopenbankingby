<?php

namespace App\Models\Card;

use App\Models\Directory\Frequency;
use App\Models\Partner;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardCashBack
 *
 * @package App\Models\Card
 */
class CardCashBack extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount_min' => 'integer',
        'amount_max' => 'integer',
        'transaction_min' => 'integer',
        'non_partners_rate' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_min',
        'amount_max',
        'transaction_min',
        'non_partners_rate',
        'frequency_id',
        'description',
        'additional_data',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['frequency', 'partners'];

    /**
     * Terms
     *
     * @return HasOne
     */
    public function frequency(): HasOne
    {
        return $this->hasOne(Frequency::class, 'id', 'frequency_id');
    }

    /**
     * Partners
     *
     * @return BelongsToMany
     */
    public function partners(): BelongsToMany
    {
        return $this->belongsToMany(Partner::class, 'card_cash_back_partner', 'c_id', 'p_id');
    }
}
