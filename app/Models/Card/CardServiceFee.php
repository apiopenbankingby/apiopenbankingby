<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardServiceFee
 *
 * @package App\Models\Card
 * @method static create(array $array)
 */
class CardServiceFee extends BaseModel
{
    protected $casts = [
        'fee_annual' => 'float',
        'fee_monthly' => 'float',
        'fee_one_time' => 'float',
        'fee_e_issue' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fee_annual',
        'fee_monthly',
        'fee_one_time',
        'fee_e_issue',
    ];
}
