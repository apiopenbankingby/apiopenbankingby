<?php

namespace App\Models\Card;

use App\Models\Directory\Frequency;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardCustomFee
 *
 * @package App\Models\Card
 */
class CardCustomFee extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'fee_rate',
        'fee_amount',
        'fee_amount_min',
        'fee_amount_max',
        'frequency_id',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['frequency'];

    /**
     * Frequency
     *
     * @return HasOne
     */
    public function frequency(): HasOne
    {
        return $this->hasOne(Frequency::class, 'id', 'frequency_id');
    }
}
