<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardATMWithdrawalFee
 *
 * @package App\Models\Card
 */
class CardATMWithdrawalFee extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_atm_withdrawal_fees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issuer_atm_id',
        'bank_partner_atm_id',
        'other_atm_id',
        'non_resident_atm',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['issuerATM', 'bankPartnersATM', 'otherATM', 'nonResidentATM'];

    /**
     * Issuer ATM
     *
     * @return HasOne
     */
    public function issuerATM(): HasOne
    {
        return $this->hasOne(CardIssuerBank::class, 'id', 'issuer_atm_id');
    }

    /**
     * bank partners ATM
     *
     * @return HasOne
     */
    public function bankPartnersATM(): HasOne
    {
        return $this->hasOne(CardBankPartner::class, 'id', 'bank_partner_atm_id');
    }

    /**
     * Other ATM
     *
     * @return HasOne
     */
    public function otherATM(): HasOne
    {
        return $this->hasOne(CardOtherBank::class, 'id', 'other_atm_id');
    }

    /**
     * Non resident ATM
     *
     * @return HasOne
     */
    public function nonResidentATM(): HasOne
    {
        return $this->hasOne(CardNonResidentBank::class, 'id', 'non_resident_atm');
    }
}
