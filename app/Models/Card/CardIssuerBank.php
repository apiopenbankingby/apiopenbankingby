<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardIssuerBank
 *
 * @package App\Models\Card
 */
class CardIssuerBank extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'fee_rate' => 'float',
        'fee_amount_min' => 'float',
        'fee_amount_max' => 'float',
        'fee_amount' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fee_rate',
        'fee_amount_min',
        'fee_amount_max',
        'fee_amount',
    ];
}
