<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CardCreditProductTerms
 *
 * @package App\Models\Card
 */
class CardCreditProductTerms extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_credit_product_terms';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Term
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(CardCreditProductTermsTerm::class, 'card_credit_product_terms_term', 'ts_id', 't_id');
    }
}
