<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardReplenishmentFee
 *
 * @package App\Models\Card
 * @method static create(array $array)
 */
class CardReplenishmentFee extends BaseModel
{
    protected $casts = [
        'fee_amount_min' => 'float',
        'fee_amount_max' => 'float',
        'fee_rate' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fee_rate',
        'fee_amount_min',
        'fee_amount_max',
        'description',
    ];
}
