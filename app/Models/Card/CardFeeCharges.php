<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardFeeCharges
 *
 * @package App\Models\Card
 */
class CardFeeCharges extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'issue_fee' => 'float',
        'instant_issue_fee' => 'float',
        'urgent_issue_fee' => 'float',
        'additional_card_issue_fee' => 'float',
        'notification_fee' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_fee_charges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issue_fee',
        'instant_issue_fee',
        'urgent_issue_fee',
        'additional_card_issue_fee',
        'notification_fee',
        're_issue_fee_id',
        'service_fee_id',
        'balance_check_fee_id',
        'transfer_fee_id',
        'bank_withdrawal_fee_id',
        'atm_withdrawal_fee_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'reIssueFee',
        'replenishmentFee',
        'serviceFee',
        'balanceCheckFee',
        'cardTransferFee',
        'bankWithdrawalFee',
        'ATMWithdrawalFee',
        'customFee',
    ];

    /**
     * ReIssue fee
     *
     * @return HasOne
     */
    public function reIssueFee(): HasOne
    {
        return $this->hasOne(CardReIssueFee::class, 'id', 're_issue_fee_id');
    }

    /**
     * Replenishment fee
     *
     * @return BelongsToMany
     */
    public function replenishmentFee(): BelongsToMany
    {
        return $this->belongsToMany(CardReplenishmentFee::class, 'card_fee_charges_replenishment_fee', 'f_id', 'r_id');
    }

    /**
     * Service fee
     *
     * @return HasOne
     */
    public function serviceFee(): HasOne
    {
        return $this->hasOne(CardServiceFee::class, 'id', 'service_fee_id');
    }

    /**
     * Balance check fee
     *
     * @return HasOne
     */
    public function balanceCheckFee(): HasOne
    {
        return $this->hasOne(CardBalanceCheckFee::class, 'id', 'balance_check_fee_id');
    }

    /**
     * Card transfer fee
     *
     * @return HasOne
     */
    public function cardTransferFee(): HasOne
    {
        return $this->hasOne(CardTransferFee::class, 'id', 'transfer_fee_id');
    }

    /**
     * Bank withdrawal fee
     *
     * @return HasOne
     */
    public function bankWithdrawalFee(): HasOne
    {
        return $this->hasOne(CardBankWithdrawalFee::class, 'id', 'bank_withdrawal_fee_id');
    }

    /**
     * ATM withdrawal fee
     *
     * @return HasOne
     */
    public function ATMWithdrawalFee(): HasOne
    {
        return $this->hasOne(CardATMWithdrawalFee::class, 'id', 'atm_withdrawal_fee_id');
    }

    /**
     * Custom fee
     *
     * @return BelongsToMany
     */
    public function customFee(): BelongsToMany
    {
        return $this->belongsToMany(CardCustomFee::class, 'card_fee_charges_custom_fee', 'c_id', 'cust_id');
    }
}
