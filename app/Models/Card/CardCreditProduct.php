<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardCreditProduct
 *
 * @package App\Models\Card
 */
class CardCreditProduct extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_credit_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'credit_products_term_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['terms'];

    /**
     * Terms
     *
     * @return HasOne
     */
    public function terms(): HasOne
    {
        return $this->hasOne(CardCreditProductTerms::class, 'id', 'credit_products_term_id');
    }
}
