<?php

namespace App\Models\Card;

use App\Models\Directory\Frequency;
use App\Models\Partner;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardLoyaltyProgram
 *
 * @package App\Models\Card
 */
class CardLoyaltyProgram extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'loyalty_services',
        'payment_system_loyalty_url',
        'partner_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['partners'];

    protected $casts = [
        'loyalty_services' => 'array'
    ];

    /**
     * Partners
     *
     * @return BelongsToMany
     */
    public function partners(): BelongsToMany
    {
        return $this->belongsToMany(Partner::class, 'card_loyalty_program_partner', 'l_id', 'p_id');
    }
}
