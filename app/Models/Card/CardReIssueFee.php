<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardReIssueFee
 *
 * @package App\Models\Card
 */
class CardReIssueFee extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'change_data_fee',
        'VOID_fee',
    ];
}
