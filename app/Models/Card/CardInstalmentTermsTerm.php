<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardInstalmentTermsTerm
 *
 * @package App\Models\Card
 */
class CardInstalmentTermsTerm extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_instalment_terms_terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_min',
        'amount_max',
        'period_min',
        'period_max',
        'additional_data',
        'interest_id',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interest'];

    /**
     * Interest
     *
     * @return HasOne
     */
    public function interest(): HasOne
    {
        return $this->hasOne(CardInstalmentInterest::class, 'id', 'interest_id');
    }
}
