<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use App\Models\Directory\CardCategory;
use App\Models\Directory\CardDataEntry;
use App\Models\Directory\CardProductCustomerType;
use App\Models\Directory\CardProductPersonalType;
use App\Models\Directory\CardType;
use App\Models\Directory\CurrencyType;
use App\Models\Directory\PaymentSystem;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

/**
 * Class Card
 *
 * @package App\Models\Card
 */
class Card extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'card_category_id',
        'card_type_id',
        'consumer_card_id',
        'card_data_entry_mode_id',
        'instalment_id',
        'foreign_country_payments',
        'custom_design',
        'internet_payments',
        'instant_issue',
        'urgent_issue',
        'permanent_balance',
        'issue_additional_card',
        'notifications',
        'interest_rate_on_balance',
        'bank_partners',
        'mobile_payments',
        'loyalty_program_id',
        'cash_back_id',
        'currency_id',
        'card_expiration_date_id',
        'credit_product_id'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'paymentCardSystem',
        'cardCategory',
        'cardType',
        'consumerCard',
        'cardDataEntryMode',
        'creditProduct',
        'instalment',
        'loyaltyProgram',
        'cashBack',
        'cardExpirationDate',
        'currency',
        'feeCharges'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'foreign_country_payments' => 'boolean',
        'custom_design' => 'boolean',
        'internet_payments' => 'boolean',
        'instant_issue' => 'boolean',
        'urgent_issue' => 'boolean',
        'permanent_balance' => 'boolean',
        'issue_additional_card' => 'boolean',
        'notifications' => 'boolean',
        'interest_rate_on_balance' => 'float',
        'bank_partners' => 'array',
        'mobile_payments' => 'array',
    ];

    /**
     * Payment card system
     *
     * @return BelongsToMany
     */
    public function paymentCardSystem(): BelongsToMany
    {
        return $this->belongsToMany(PaymentSystem::class, 'cards_payment_systems', 'c_id', 'p_id');
    }

    /**
     * Card category
     *
     * @return HasOne
     */
    public function cardCategory(): HasOne
    {
        return $this->hasOne(CardCategory::class, 'id', 'card_category_id');
    }

    /**
     * Card type
     *
     * @return HasOne
     */
    public function cardType(): HasOne
    {
        return $this->hasOne(CardType::class, 'id', 'card_type_id');
    }

    /**
     * Consumer card
     *
     * @return HasOne
     */
    public function consumerCard(): HasOne
    {
        return $this->hasOne(CardProductPersonalType::class, 'id', 'consumer_card_id');
    }

    /**
     * Card data entry mode
     *
     * @return HasOne
     */
    public function cardDataEntryMode(): HasOne
    {
        return $this->hasOne(CardDataEntry::class, 'id', 'card_data_entry_mode_id');
    }

    /**
     * Current status
     *
     * @return HasOne
     */
    public function creditProduct(): HasOne
    {
        return $this->hasOne(CardCreditProduct::class, 'id', 'credit_product_id');
    }

    /**
     * Instalment
     *
     * @return HasOne
     */
    public function instalment(): HasOne
    {
        return $this->hasOne(CardInstalment::class, 'id', 'instalment_id');
    }

    /**
     * Loyalty program
     *
     * @return HasOne
     */
    public function loyaltyProgram(): HasOne
    {
        return $this->hasOne(CardLoyaltyProgram::class, 'id', 'loyalty_program_id');
    }

    /**
     * Cash back
     *
     * @return HasOne
     */
    public function cashBack(): HasOne
    {
        return $this->hasOne(CardCashBack::class, 'id', 'cash_back_id');
    }

    /**
     * Card expiration date
     *
     * @return HasOne
     */
    public function cardExpirationDate(): HasOne
    {
        return $this->hasOne(CardExpirationDate::class, 'id', 'card_expiration_date_id');
    }

    /**
     * Currency
     *
     * @return HasOne
     */
    public function currency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'currency_id');
    }

    /**
     * Fee charges
     *
     * @return BelongsToMany
     */
    public function feeCharges(): BelongsToMany
    {
        return $this->belongsToMany(CardFeeCharges::class, 'cards_fee_charges', 'c_id', 'f_id');
    }
}
