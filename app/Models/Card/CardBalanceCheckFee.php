<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardBalanceCheckFee
 *
 * @package App\Models\Card
 */
class CardBalanceCheckFee extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'issuer_bank_id',
        'bank_partner_id',
        'other_bank_id',
        'non_resident_bank',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['issuerBank', 'bankPartners', 'otherBank', 'nonResidentBank'];

    /**
     * Issuer bank
     *
     * @return HasOne
     */
    public function issuerBank(): HasOne
    {
        return $this->hasOne(CardIssuerBank::class, 'id', 'issuer_bank_id');
    }

    /**
     * bank partners
     *
     * @return HasOne
     */
    public function bankPartners(): HasOne
    {
        return $this->hasOne(CardBankPartner::class, 'id', 'bank_partner_id');
    }

    /**
     * Other bank
     *
     * @return HasOne
     */
    public function otherBank(): HasOne
    {
        return $this->hasOne(CardOtherBank::class, 'id', 'other_bank_id');
    }

    /**
     * Non resident bank
     *
     * @return HasOne
     */
    public function nonResidentBank(): HasOne
    {
        return $this->hasOne(CardNonResidentBank::class, 'id', 'non_resident_bank');
    }
}
