<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardCreditProductTermsTerm
 *
 * @package App\Models\Card
 */
class CardCreditProductTermsTerm extends BaseModel
{
    protected $casts = [
        'amount_min' => 'float',
        'amount_max' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_credit_product_terms_terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount_min',
        'amount_max',
        'period',
        'interest_id',
        'additional_data',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['interest', 'guarantee'];

    /**
     * Interest
     *
     * @return HasOne
     */
    public function interest(): HasOne
    {
        return $this->hasOne(CardCreditProductInterest::class, 'id', 'interest_id');
    }

    /**
     * Guarantee
     *
     * @return BelongsToMany
     */
    public function guarantee(): BelongsToMany
    {
        return $this->belongsToMany(CardCreditProductGuarantee::class, 'card_credit_product_terms_term_guarantee', 't_id', 'g_id');
    }
}
