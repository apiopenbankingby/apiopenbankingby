<?php

namespace App\Models\Card;

use App\Models\Directory\GuaranteeType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CardCreditProductGuarantee
 *
 * @package App\Models\Card
 */
class CardCreditProductGuarantee extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_credit_product_guarantees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['guaranteeType'];

    /**
     * Guarantee type
     *
     * @return belongsToMany
     */
    public function guaranteeType(): BelongsToMany
    {
        return $this->belongsToMany(GuaranteeType::class, 'card_credit_product_guarantees_type', 'g_id', 't_id');
    }
}
