<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardExpirationDate
 * @package App\Models\Card
 */
class CardExpirationDate extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['months'];
}
