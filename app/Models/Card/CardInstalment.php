<?php

namespace App\Models\Card;

use App\Models\Partner;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class CardInstalment
 *
 * @package App\Models\Card
 */
class CardInstalment extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'term_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['terms', 'partners'];

    /**
     * Terms
     *
     * @return HasOne
     */
    public function terms(): HasOne
    {
        return $this->hasOne(CardInstalmentTerms::class, 'id', 'term_id');
    }

    /**
     * Partners
     *
     * @return BelongsToMany
     */
    public function partners(): BelongsToMany
    {
        return $this->belongsToMany(Partner::class, 'card_instalment_partner', 'i_id', 'p_id');
    }
}
