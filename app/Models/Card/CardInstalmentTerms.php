<?php

namespace App\Models\Card;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CardInstalmentTerms
 *
 * @package App\Models\Card
 */
class CardInstalmentTerms extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_instalment_terms';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Term
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(CardInstalmentTermsTerm::class, 'card_instalment_terms_term', 'ts_id', 't_id');
    }
}
