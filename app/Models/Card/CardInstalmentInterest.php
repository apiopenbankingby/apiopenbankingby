<?php

namespace App\Models\Card;

use App\Models\BaseModel;

/**
 * Class CardInstalmentInterest
 *
 * @package App\Models\Card
 */
class CardInstalmentInterest extends BaseModel
{
    protected $casts = [
        'rate' => 'float',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'card_instalment_interests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate',
        'early_repayment_interest',
        'description',
    ];
}
