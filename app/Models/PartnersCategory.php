<?php

namespace App\Models;

use App\Models\Directory\PartnerCategoryType;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class PartnersCategory
 *
 * @package App\Models
 */
class PartnersCategory extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partners_categories';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_discount_rate' => 'float',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'category_discount_rate'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['category', 'partner'];

    /**
     * Category
     *
     * @return HasOne
     */
    public function category(): HasOne
    {
        return $this->hasOne(PartnerCategoryType::class, 'id', 'category_id');
    }

    /**
     * Partner
     *
     * @return BelongsToMany
     */
    public function partner(): BelongsToMany
    {
        return $this->belongsToMany(PartnersPartner::class, 'partners_category_partner', 'c_id', 'p_id');
    }
}
