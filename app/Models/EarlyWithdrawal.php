<?php

namespace App\Models;

use App\Models\Directory\WithdrawalType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class EarlyWithdrawal
 *
 * @package App\Models
 */
class EarlyWithdrawal extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'is_partitional_withdrawal',
        'early_withdrawal_fee',
        'description',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['withdrawalType'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['is_partitional_withdrawal' => 'boolean'];

    /**
     * Withdrawal type
     *
     * @return HasOne
     */
    public function withdrawalType(): HasOne
    {
        return $this->hasOne(WithdrawalType::class, 'id', 'type_id');
    }
}
