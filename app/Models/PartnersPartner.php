<?php

namespace App\Models;

/**
 * Class PartnersPartner
 *
 * @package App\Models
 */
class PartnersPartner extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partners_partners';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['partner_rate' => 'array'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'partner_uin',
        'partner_name',
        'partner_rate'
    ];
}
