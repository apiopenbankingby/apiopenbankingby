<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Partner
 *
 * @package App\Models
 */
class Partner extends BaseModel
{
    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['partnersCategory'];

    /**
     * Partners category
     *
     * @return BelongsToMany
     */
    public function partnersCategory(): BelongsToMany
    {
        return $this->belongsToMany(PartnersCategory::class, 'partners_category', 'p_id', 'c_id');
    }
}
