<?php

namespace App\Models\Transfer;

use App\Models\BaseModel;
use App\Models\Directory\Country;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class TransferTerm
 *
 * @package App\Models\Transfer
 */
class TransferTerm extends BaseModel
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount_min' => 'float',
        'amount_max' => 'float',
        'free_rate' => 'float',
        'free_amount' => 'float',
        'additional_data' => 'array'
    ];

    protected $table = 'transfer_terms_terms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_destination_id',
        'transfer_period',
        'amount_min',
        'amount_max',
        'free_rate',
        'free_amount',
        'additional_data'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['countryDestinantion'];

    /**
     * Country destination
     *
     * @return HasOne
     */
    public function countryDestinantion(): HasOne
    {
        return $this->hasOne(Country::class, 'id', 'country_destination_id');
    }
}
