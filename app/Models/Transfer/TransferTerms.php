<?php

namespace App\Models\Transfer;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class TransferTerms
 *
 * @package App\Models\Transfer
 */
class TransferTerms extends BaseModel
{
    protected $table = 'transfer_terms';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['term'];

    /**
     * Terms
     *
     * @return BelongsToMany
     */
    public function term(): BelongsToMany
    {
        return $this->belongsToMany(TransferTerm::class, 'transfer_terms_term', 'terms_id', 'term_id');
    }
}
