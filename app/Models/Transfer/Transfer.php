<?php

namespace App\Models\Transfer;

use App\Models\BaseModel;
use App\Models\Directory\TransferType;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Availability;
use App\Models\Currency;
use App\Models\ContactDetails;

/**
 * Class Transfer
 *
 * @package App\Models\Transfer
 */
class Transfer extends BaseModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transfer_type_id',
        'additional_services',
        'can_receive',
        'cont_det_id',
        'availability_id',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['transferSystem'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'additional_services' => 'array',
        'can_receive' => 'boolean',
    ];

    /**
     * Transfer type
     *
     * @return hasOne
     */
    public function transferSystem(): HasOne
    {
        return $this->hasOne(TransferType::class, 'id', 'transfer_type_id');
    }

    /**
     * Availability
     *
     * @return HasOne
     */
    public function availability(): HasOne
    {
        return $this->hasOne(Availability::class, 'id', 'availability_id');
    }

    /**
     * ContactDetails
     *
     * @return HasOne
     */
    public function contactDetails(): HasOne
    {
        return $this->hasOne(ContactDetails::class, 'id', 'cont_det_id');
    }

    /**
     * Currency
     *
     * @return BelongsToMany
     */
    public function currency(): BelongsToMany
    {
        return $this->belongsToMany(Currency::class, 'currency_transfer', 'transfer_id', 'currency_id');
    }
}
