<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class NonStandardAvailability
 *
 * @package App\Models
 */
class NonStandardAvailability extends BaseModel
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'from_date',
        'to_date'
    ];

    protected  $table = 'non_standard_availabilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'from_date',
        'to_date',
        'description'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['day'];

    /**
     * Day
     *
     * @return BelongsToMany
     */
    public function day(): BelongsToMany
    {
        return $this->belongsToMany(Day::class, 'day_non_standard_availability', 'a_id', 'day_id');
    }
}
