<?php

namespace App\Models\Atm;

use App\Models\BaseModel;

/**
 * Class AtmStatus
 *
 * @package App\Models\Atm
 */
class AtmStatus extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['code', 'name'];
}
