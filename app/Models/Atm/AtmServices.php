<?php

namespace App\Models\Atm;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class AtmServices
 *
 * @package App\Models\Atm
 */
class AtmServices extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'atm_services';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['atmService'];

    /**
     * Atm Service
     *
     * @return BelongsToMany
     */
    public function atmService(): BelongsToMany
    {
        return $this->belongsToMany(AtmService::class, 'atm_services_atm_service', 'atm_services_id', 'atm_service_id');
    }
}
