<?php

namespace App\Models\Atm;

use App\Filters\ATMFilter;
use App\Models\Accessibilities;
use App\Models\Availability;
use App\Models\ContactDetails;
use App\Models\CurrencyExchange;
use App\Models\Directory\AtmType;
use App\Models\Directory\CurrencyType;
use App\Models\Directory\PaymentSystem;
use App\Models\Location;
use App\Models\BaseModel;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Atm
 *
 * @property mixed id
 * @package App\Models\Atm
 * @method static findOrFail($id)
 */
class Atm extends BaseModel
{
    use Filterable;

    /**
     * Model filter to default
     *
     * @return string
     */
    public function modelFilter()
    {
        return $this->provideFilter(ATMFilter::class);
    }

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'id',
        'base_curr_id',
        'curr_sts_id',
        'location_id',
        'availability_id',
        'services_id',
        'cont_det_id',
        'accessibility_id',
        'description',
        'additional_data'
    ];

    protected $casts = [
        'additional_data' => 'array'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'type',
        'baseCurrency',
        'currency',
        'cards',
        'currentStatus',
        'location',
        'services',
        'availability',
        'contactDetails',
        'accessibilities',
        'currencyExchange'
    ];

    /**
     * ATM type
     *
     * @return HasOne
     */
    public function type(): HasOne
    {
        return $this->hasOne(AtmType::class, 'id', 'type_id');
    }

    /**
     * BaseCurrency
     *
     * @return HasOne
     */
    public function baseCurrency(): HasOne
    {
        return $this->hasOne(CurrencyType::class, 'id', 'base_curr_id');
    }

    /**
     * Currency
     *
     * @return BelongsToMany
     */
    public function currency(): BelongsToMany
    {
        return $this->belongsToMany(CurrencyType::class, 'atm_currency', 'atm_id', 'curr_id');
    }

    /**
     * Cards
     *
     * @return BelongsToMany
     */
    public function cards(): BelongsToMany
    {
        return $this->belongsToMany(PaymentSystem::class, 'atm_card', 'atm_id', 'card_id');
    }

    /**
     * CurrentStatus
     *
     * @return HasOne
     */
    public function currentStatus(): HasOne
    {
        return $this->hasOne(AtmStatus::class, 'id', 'curr_sts_id');
    }

    /**
     * Location
     *
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class, 'id', 'location_id');
    }

    /**
     * Services
     *
     * @return HasOne
     */
    public function services(): HasOne
    {
        return $this->hasOne(AtmServices::class, 'id', 'services_id');
    }

    /**
     * Availability
     *
     * @return HasOne
     */
    public function availability(): HasOne
    {
        return $this->hasOne(Availability::class, 'id', 'availability_id');
    }

    /**
     * ContactDetails
     *
     * @return HasOne
     */
    public function contactDetails(): HasOne
    {
        return $this->hasOne(ContactDetails::class, 'id', 'cont_det_id');
    }

    /**
     * Accessibilities
     *
     * @return HasOne
     */
    public function accessibilities(): HasOne
    {
        return $this->hasOne(Accessibilities::class, 'id', 'accessibility_id');
    }

    /**
     * CurrencyExchange
     *
     * @return BelongsToMany
     */
    public function currencyExchange(): BelongsToMany
    {
        return $this->belongsToMany(CurrencyExchange::class, 'atm_currency_exchange', 'atm_id', 'curr_exch_id');
    }
}
