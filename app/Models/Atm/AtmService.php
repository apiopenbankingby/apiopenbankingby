<?php

namespace App\Models\Atm;

use App\Models\Directory\AtmServiceType;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class AtmService
 *
 * @package App\Models\Atm
 */
class AtmService extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'atm_service';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['service_type_id', 'description', 'additional_data'];

    protected $casts = [
        'additional_data' => 'array'
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['atmServiceType'];

    /**
     * AtmService
     *
     * @return HasOne
     */
    public function atmServiceType(): HasOne
    {
        return $this->hasOne(AtmServiceType::class, 'id', 'service_type_id');
    }
}
