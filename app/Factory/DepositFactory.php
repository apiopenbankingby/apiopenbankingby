<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class DepositFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'Deposit',
            'Name' => 'Deposit name_test',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description Deposit',
            'URL' => 'URL:Deposit',
            'Deposit' => [
                'DepositType' => 'UrgentRevocable',
                'IsEarlyWithdrawalInterest' => false,
                'Currency' => [
                    [
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'Terms' => [
                            [
                                'AmountMin' => 150,
                                'AmountMax' => 20000,
                                'TaxIncluded' => false,
                                'AdditionalData' => false,
                                'Description' => null,
                                'Period' => [
                                    [
                                        'PeriodMin' => 6,
                                        'PeriodMax' => 6,
                                        'PeriodType' => 'Month',
                                        'Interest' => [
                                            [
                                                'InterestRate' => 'Refinance',
                                                'Rate' => -4.2,
                                                'Description' => 'Переменная годовая процентная ставка изменяется в случае изменения базового показателя со следующего дня после его изменения'
                                            ]
                                        ],
                                        'Replenishment' => [
                                            'Replenishment' => true,
                                            'AmountMin' => 0,
                                            'AmountMax' => 1000,
                                            'Description' => 'До дня наступления последнего месяца хранения'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'AmountMin' => 150,
                                'AmountMax' => 20000,
                                'TaxIncluded' => false,
                                'AdditionalData' => false,
                                'Description' => null,
                                'Period' => [
                                    [
                                        'PeriodMin' => 3,
                                        'PeriodMax' => 3,
                                        'PeriodType' => 'Month',
                                        'Interest' => [
                                            [
                                                'InterestRate' => 'Overnight',
                                                'Rate' => -4.2,
                                                'Description' => 'Переменная годовая процентная ставка изменяется в случае изменения базового показателя со следующего дня после его изменения'
                                            ]
                                        ],
                                        'Replenishment' => [
                                            'Replenishment' => true,
                                            'AmountMin' => 0,
                                            'AmountMax' => 1000,
                                            'Description' => 'До дня наступления последнего месяца хранения'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'EarlyWithdrawal' => [
                    'WithdrawalType' => 'WithoutLoses',
                    'IsPartitionalWithdrawal' => false,
                    'EarlyWithdrawalFee' => '0',
                    'Description' => null
                ],
                'Capitalization' => [
                    'CapitalizationType' => 'NoCapitalization',
                    'Description' => null
                ],
                'ContactDetails' => [
                    'Name' => 'test',
                    'PhoneNumber' => '+375172899090',
                    'MobileNumber' => null,
                    'FaxNumber' => null,
                    'EmailAddress' => null,
                    'Other' => null,
                    'SocialNetworks' => [
                        [
                            'Network' => 'Facebook',
                            'URL' => '@priorbankby'
                        ],
                        [
                            'Network' => 'Viber',
                            'URL' => '375172899090'
                        ]
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
