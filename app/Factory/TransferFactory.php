<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class TransferFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();
        $data = [
            'Id' => $faker->uuid,
            'Type' => 'Transfer',
            'Name' => 'Transfer name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description Transfer',
            'URL' => 'URL=>Transfer',
            'Transfer' => [
                'TransferSystem' => 'Hutki',
                'AdditionalServices' => [
                    'Доставка чека домой - 3 руб'
                ],
                'CanReceive' => true,
                'Currency' => [
                    [
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'Terms' => [
                            [
                                'CountryDestination' => 'AZ',
                                'AmountMin' => 10,
                                'AmountMax' => 100
                            ]
                        ]
                    ]
                ],
                'ContactDetails' => [
                    'Name' => 'Name - test Transfer',
                    'PhoneNumber' => '1234567',
                    'SocialNetworks' => [
                        [
                            'Network' => 'Twitter',
                            'URL' => 'twitter.com'
                        ]
                    ]
                ],
                'Availability' => [
                    'Access24Hours' => true,
                    'IsRestricted' => true,
                    'SameAsOrganization' => false,
                    'StandardAvailability' => [
                        'Day' => [
                            [
                                'DayCode' => '01',
                                'OpeningTime' => $faker->time('H:i:s'),
                                'ClosingTime' => $faker->time('H:i:s'),
                                'Break' => [
                                    [
                                        'BreakFromTime' => $faker->time('H:i:s'),
                                        'BreakToTime' => $faker->time('H:i:s')
                                    ]
                                ]
                            ],
                            [
                                'DayCode' => '04',
                                'OpeningTime' => $faker->time('H:i:s'),
                                'ClosingTime' => $faker->time('H:i:s')
                            ]
                        ]
                    ],
                    'NonStandardAvailability' => [
                        [
                            'Name' => 'Test - test no standard',
                            'FromDate' => $faker->date('Y-m-d H:i:s'),
                            'ToDate' => $faker->date('Y-m-d H:i:s'),
                            'Day' => [
                                [
                                    'DayCode' => '02',
                                    'OpeningTime' => $faker->time('H:i:s'),
                                    'ClosingTime' => $faker->time('H:i:s'),
                                    'Break' => [
                                        [
                                            'BreakFromTime' => $faker->time('H:i:s'),
                                            'BreakToTime' => $faker->time('H:i:s')
                                        ]
                                    ]
                                ],
                                [
                                    'DayCode' => '03',
                                    'OpeningTime' => $faker->time('H:i:s'),
                                    'ClosingTime' => $faker->time('H:i:s')
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
