<?php

namespace App\Factory;

use App\Models\Bank;
use App\Services\Bank\CreateBank as ServiceCreateBank;
use Faker\Factory as Faker;

class BankFactory
{
    /**
     * Create service
     *
     * @return Bank
     */
    public static function create(): Bank
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'BICFI' => 'BPSBBY2X',
            'ClearingSystemMemberIdentification' => 1234567,
            'Code' => null,
            'Proprietary' => 'BYNBB',
            'MemberIdentification' => 'Банк',
            'Name' => 'БПС-Сбербанк',
            'LegalEntityIdentifier' => null,
            'PostalAddress' => [
                'StreetName' => 'бульвар имени Мулявина',
                'BuildingNumber' => '6',
                'Department' => null,
                'PostCode' => '220005',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '5',
                'Country' => 'BB',
                'AddressLine' => [
                    'Республика Беларусь, 220005  г. Минск, бульвар имени Мулявина, 6'
                ],
                'Description' => 'Республика Беларусь, 220005  г. Минск, бульвар имени Мулявина, 6'
            ]
        ];

        $bank = app(ServiceCreateBank::class)->execute($data);
        return $bank;
    }
}
