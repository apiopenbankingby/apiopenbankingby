<?php

namespace App\Factory;

use App\Models\Branch\Branch;
use App\Services\Branch\CreateBranch as ServiceCreateBranch;
use Faker\Factory as Faker;

class BranchFactory
{
    /**
     * Create service
     *
     * @return Branch
     */
    public static function create(): Branch
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Name' => 'Дополнительный офис №751 на Маяковского Региональной дирекции №700 по г. Минску и Минской области',
            'Accessibilities' => [
                [
                    'Type' => 'Braille',
                    'Description' => null
                ],
                [
                    'Type' => 'AudioCashMachine',
                    'Description' => null
                ]
            ],
            'PostalAddress' => [
                'StreetName' => 'ул. Маяковского',
                'BuildingNumber' => '154',
                'Department' => null,
                'PostCode' => '220028',
                'TownName' => 'г. Минск',
                'CountrySubDivision' => '5',
                'Country' => 'AF',
                'AddressLine' => [
                    'Республика Беларусь, 220002, г. Минск, ул. Маяковского, 154'
                ],
                'Description' => null,
                'Geolocation' => [
                    'Latitude' => 54.86857,
                    'Longitude' => 28.569547
                ]
            ],
            'Information' => [
                [
                    'Segment' => $faker->randomElement(['Business', 'Individual']),
                    'Availability' => [
                        [
                            'Access24Hours' => false,
                            'IsRestricted' => false,
                            'SameAsOrganization' => false,
                            'Description' => 'sample availability desc',
                            'StandardAvailability' => [
                                'Day' => [
                                    [
                                        'DayCode' => '01',
                                        'OpeningTime' => '08:00:00',
                                        'ClosingTime' => '17:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '13:30:00',
                                                'BreakToTime' => '14:30:00'
                                            ]
                                        ]
                                    ],
                                    [
                                        'DayCode' => '02',
                                        'OpeningTime' => '09:00:00',
                                        'ClosingTime' => '18:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '10:30:00',
                                                'BreakToTime' => '11:00:00'
                                            ],
                                            [
                                                'BreakFromTime' => '14:30:00',
                                                'BreakToTime' => '15:00:00'
                                            ]
                                        ]
                                    ],
                                    [
                                        'DayCode' => '03',
                                        'OpeningTime' => '09:00:00',
                                        'ClosingTime' => '18:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '13:00:00',
                                                'BreakToTime' => '14:00:00'
                                            ]
                                        ]
                                    ],
                                    [
                                        'DayCode' => '04',
                                        'OpeningTime' => '08:00:00',
                                        'ClosingTime' => '17:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '13:00:00',
                                                'BreakToTime' => '14:00:00'
                                            ]
                                        ]
                                    ],
                                    [
                                        'DayCode' => '05',
                                        'OpeningTime' => '08:00:00',
                                        'ClosingTime' => '17:00:00',
                                        'Break' => [
                                            [
                                                'BreakFromTime' => '13:00:00',
                                                'BreakToTime' => '14:00:00'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'NonStandardAvailability' => [
                                [
                                    'Name' => 'ExtraDayOff',
                                    'FromDate' => '2019-05-09 00:00:00',
                                    'ToDate' => '2019-05-09 00:00:00',
                                    'Description' => 'sample extra day off description',
                                    'Day' => [
                                        [
                                            'DayCode' => '03',
                                            'OpeningTime' => '09:00:00',
                                            'ClosingTime' => '18:00:00',
                                            'Break' => [
                                                [
                                                    'BreakFromTime' => '10:30:00',
                                                    'BreakToTime' => '13:30:00'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'ContactDetails' => [
                        [
                            'Name' => null,
                            'PhoneNumber' => '5-148-148',
                            'MobileNumber' => '5-148-148',
                            'FaxNumber' => null,
                            'EmailAddress' => 'inbox@bps-sberbank.by',
                            'Other' => null,
                            'SocialNetworks' => [
                                [
                                    'Network' => 'VK',
                                    'URL' => 'URL',
                                    'Description' => [
                                        'Description1',
                                        'Description2'
                                    ]
                                ],
                                [
                                    'Network' => 'Facebook',
                                    'URL' => 'URL'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $branch = app(ServiceCreateBranch::class)->execute($data);
        return $branch;
    }
}
