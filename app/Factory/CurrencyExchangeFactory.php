<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class CurrencyExchangeFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'CurrencyExchange',
            'Name' => 'CurrencyExchange name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description CurrencyExchange',
            'URL' => 'URL=>CurrencyExchange',
            'CurrencyExchange' => [
                'Terms' => [
                    [
                        'ExchangeType' => 'Online',
                        'SourceCurrency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'TargetCurrency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'ScaleCurrency' => 10,
                        'Scale' => 1,
                        'ExchangeRate' => 2.26,
                        'AmountMin' => 5.5,
                        'AmountMax' => 500,
                        'Direction' => 'buy',
                        'DateTime' => $faker->date('Y-m-d H:i:s'),
                        'UpdatedDateTime' => $faker->date('Y-m-d H:i:s'),
                        'AdditionalData' => null
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
