<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class DirectDebitFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'DirectDebit',
            'Name' => 'DirectDebit name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description DirectDebit',
            'URL' => 'URL:DirectDebit',
            'DirectDebit' => [
                'Terms' => [
                    [
                        'FeeType' => 'TransactionFee',
                        'FeeRate' => 2,
                        'FeeAmount' => 10,
                        'FeeAmountMin' => 1,
                        'FeeAmountMax' => 5.99,
                        'Description' => 'Описание прямого дебетования'
                    ],
                    [
                        'FeeType' => 'TransactionFee',
                        'Frequency' => 'OneTime'
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
