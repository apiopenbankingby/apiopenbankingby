<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class JewelFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'Jewel',
            'Name' => 'Jewel name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description Jewel',
            'URL' => 'URL:Jewel',
            'Jewel' => [
                'Coin' => [
                    [
                        'Name' => '75 лет освобождения Беларуси от немецко-фашистских захватчиков',
                        'Value' => 21,
                        'Quality' => 'пруф',
                        'SellingPrice' => 115,
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'DateTime' => $faker->date('Y-m-d H:i:s'),
                        'CoinMaterial' => [
                            [
                                'Material' => 'Platinum',
                                'Sample' => 10
                            ]
                        ]
                    ],
                    [
                        'Name' => 'освобождения Беларуси от немецко-фашистских захватчиков',
                        'Value' => 20,
                        'Quality' => 'пруф',
                        'SellingPrice' => 115,
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'DateTime' => $faker->date('Y-m-d H:i:s')
                    ]
                ],
                'Stone' => [
                    [
                        'Material' => 'Platinum',
                        'Weight' => 100,
                        'SellingPrice' => 80,
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR'])
                    ]
                ],
                'PreciousMetal' => [
                    [
                        'Material' => 'Platinum',
                        'Weight' => 100,
                        'Sample' => 90,
                        'SellingPrice' => 80,
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'PurchasePrice' => 9
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
