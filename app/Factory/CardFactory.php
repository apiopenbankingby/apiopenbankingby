<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class CardFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'Card',
            'Name' => 'Card name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description Card',
            'URL' => 'URL:Card',
            'AdditionalData' => null,
            'Card' => [
                'PaymentCardSystem' => ['Visa', 'MasterCard'],
                'CardCategory' => 'ClassicCard',
                'CardType' => 'CreditCard',
                'ConsumerCard' => 'CorporateCard',
                'CardDataEntryMode' => 'MagneticStripeProximityEMV',
                'ForeignCountryPayments' => true,
                'CustomDesign' => false,
                'InternetPayments' => true,
                'InstantIssue' => false,
                'UrgentIssue' => false,
                'PermanentBalance' => false,
                'IssueAdditionalCard' => true,
                'Notifications' => true,
                'InterestRateOnBalance' => 0,
                'BankPartners' => ['Partners'],
                'FeeCharges' => [
                    [
                        'IssueFee' => 0,
                        'InstantIssueFee' => null,
                        'UrgentIssueFee' => null,
                        'AdditionalCardIssueFee' => null,
                        'NotificationFee' => 1.99,
                        'ReIssueFee' => [
                            'ChangeDataFee' => 2,
                            'VOIDFee' => 3
                        ],
                        'ReplenishmentFee' => [
                            [
                                'FeeAmountMin' => 100,
                                'FeeAmountMax' => 100,
                                'FeeRate' => 1.99,
                                'Description' => null
                            ]
                        ],
                        'ServiceFee' => [
                            'FeeAnnual' => 0,
                            'FeeMonthly' => 0,
                            'FeeOneTime' => 0,
                            'FeeEIssue' => 0
                        ],
                        'BalanceCheckFee' => [
                            'IssuerBank' => [
                                'FeeAmount' => 10
                            ],
                            'BankPartners' => [
                                'FeeAmount' => 10
                            ],
                            'OtherBank' => [
                                'FeeAmount' => 5
                            ],
                            'NonResidentBank' => [
                                'FeeAmount' => 10
                            ]
                        ],
                        'BankWithdrawalFee' => [
                            'IssuerBank' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 100
                            ],
                            'BankPartners' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ],
                            'OtherBank' => [
                                'FeeRate' => 5,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 20
                            ],
                            'NonResidentBank' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ]
                        ],
                        'ATMWithdrawalFee' => [
                            'IssuerATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 100
                            ],
                            'BankPartnersATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ],
                            'OtherATM' => [
                                'FeeRate' => 5,
                                'FeeAmountMin' => 1,
                                'FeeAmountMax' => 20
                            ],
                            'NonResidentATM' => [
                                'FeeRate' => 10,
                                'FeeAmountMin' => 5,
                                'FeeAmountMax' => 20
                            ]
                        ],
                        'CustomFee' => []
                    ]
                ],
                'CardExpirationDate' => [
                    'Months' => 36
                ],
                'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                'CashBack' => [
                    'AmountMin' => 1,
                    'AmountMax' => 10,
                    'Frequency' => 'Quarterly',
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'LoyaltyProgram' => [
                    'LoyaltyServices' => [
                        'LoyaltyServices'
                    ],
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'PaymentSystemLoyaltyURL' => null
                ],
                'Instalment' => [
                    'Partners' => [
                        [
                            'PartnerCategory' => [
                                [
                                    'Category' => 'Гродненский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'ГОУП «УКС Гродненского облисполкома»'
                                        ],
                                        [
                                            'PartnerName' => 'Волковысская ОАО «СМТ-32»'
                                        ]
                                    ]
                                ],
                                [
                                    'Category' => 'Минский район',
                                    'Partner' => [
                                        [
                                            'PartnerName' => 'Группа компаний «Дана Холдингс»'
                                        ],
                                        [
                                            'PartnerName' => 'ГК «А-100 Девелопмент»'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'Terms' => [
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 1000,
                            'PeriodMin' => 2,
                            'PeriodMax' => 12,
                            'Description' => 'В магазинах-партнерах',
                            'Interest' => [
                                'Rate' => 1.0e-6
                            ]
                        ],
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 1000,
                            'PeriodMin' => 2,
                            'PeriodMax' => 2,
                            'Description' => 'Во всех магазинах',
                            'Interest' => [
                                'Rate' => 1.0e-6
                            ]
                        ]
                    ]
                ],
                'CreditProduct' => [
                    'Terms' => [
                        [
                            'AmountMin' => 100,
                            'AmountMax' => 999.99,
                            'Period' => 12,
                            'Description' => 'В магазинах-партнерах',
                            'Interest' => [
                                'InterestRate' => 'Refinance',
                                'Rate' => 1.0e-6,
                                'EarlyRepaymentInterest' => null,
                                'GracePeriodInterest' => null,
                                'Description' => null
                            ],
                            'Guarantee' => [
                                [
                                    'GuaranteeType' => [
                                        'NoGuarantee'
                                    ],
                                    'Description' => null
                                ]
                            ]
                        ],
                        [
                            'AmountMin' => 10,
                            'AmountMax' => 99,
                            'Period' => 2,
                            'Description' => 'Во всех магаззинах',
                            'Interest' => [
                                'InterestRate' => 'Refinance',
                                'Rate' => 1.0e-6,
                                'EarlyRepaymentInterest' => null,
                                'GracePeriodInterest' => null,
                                'Description' => null
                            ],
                            'Guarantee' => [
                                [
                                    'GuaranteeType' => [
                                        'NoGuarantee'
                                    ],
                                    'Description' => null
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
