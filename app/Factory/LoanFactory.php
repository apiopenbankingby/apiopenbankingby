<?php

namespace App\Factory;

use App\Models\Service\ServicesService;
use App\Services\Service\CreateService;
use Faker\Factory as Faker;

class LoanFactory
{
    /**
     * Create service
     *
     * @return ServicesService
     */
    public static function create(): ServicesService
    {
        $faker = Faker::create();

        $data = [
            'Id' => $faker->uuid,
            'Type' => 'Loan',
            'Name' => 'Loan name',
            'Segment' => $faker->randomElement(['Business', 'Individual']),
            'DateTime' => $faker->date('Y-m-d H:i:s'),
            'CurrentStatus' => 'Active',
            'Description' => 'Description Loan',
            'URL' => 'URL:Loan',
            'Loan' => [
                'LoanCategory' => 'Consumer',
                'LoanType' => [
                    'Building'
                ],
                'LoanForm' => [
                    'Cashless'
                ],
                'ApplicationType' => [
                    'Bank'
                ],
                'AdditionalServices' => [
                    'InsuranceProperty'
                ],
                'IsRenewable' => false,
                'Currency' => [
                    [
                        'Currency' => $faker->randomElement(['BYN', 'USD', 'EUR']),
                        'Terms' => [
                            [
                                'AmountMin' => 10,
                                'AmountMax' => 20,
                                'Period' => 20,
                                'PeriodType' => 'Day',
                                'Interest' => [
                                    'InterestRate' => 'FixRate',
                                    'Rate' => 10,
                                    'EarlyRepaymentInterest' => 11
                                ],
                                'Guarantee' => [
                                    [
                                        'GuaranteeType' => 'Penalty'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'ContactDetails' => [
                    'Name' => 'Name - test Transfer',
                    'PhoneNumber' => '1234567',
                    'MobileNumber' => null,
                    'FaxNumber' => null,
                    'EmailAddress' => null,
                    'Other' => null,
                    'SocialNetworks' => [
                        [
                            'Network' => 'Twitter',
                            'URL' => 'twitter.com'
                        ]
                    ]
                ],
                'Eligibility' => [
                    [
                        'AgeMin' => 10,
                        'AgeMax' => 20,
                        'IsSalaryProject' => true,
                        'IncomeMin' => 10,
                        'WorkPeriod' => 5,
                        'NeedIncomeCertificate' => true
                    ]
                ],
                'Repayment' => [
                    [
                        'RepaymentType' => 'EqualPayments',
                        'IsEarlyRepayment' => true
                    ]
                ],
                'Partners' => [
                    [
                        'Category' => 'Минск',
                        'Partner' => [
                            [
                                'PartnerUIN' => 12345
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $service = app(CreateService::class)->execute($data);
        return $service;
    }
}
