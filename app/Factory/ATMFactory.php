<?php

namespace App\Factory;

use App\Models\Atm\Atm;
use \App\Services\ATM\CreateATM as ServiceCreateATM;
use Faker\Factory as Faker;

class ATMFactory
{
    /**
     * Create service
     *
     * @return Atm
     */
    public static function create(): Atm
    {
        $faker = Faker::create();

        $data = [
            "Id" => $faker->uuid,
            "Type" => "ATM",
            "BaseCurrency" => $faker->randomElement(['BYN', 'USD', 'EUR']),
            "Currency" => [
                $faker->randomElement(['BYN', 'USD', 'EUR']),
                $faker->randomElement(['BYN', 'USD', 'EUR'])
            ],
            "Cards" => [
                "Visa",
                "MasterCard"
            ],
            "CurrentStatus" => "On",
            "Description" => "Sample atm description",
            "Location" => [
                "StreetName" => "бульвар имени Банкомата",
                "BuildingNumber" => "13",
                "Department" => null,
                "PostCode" => "220145",
                "TownName" => "г. Минск",
                "CountrySubDivision" => "5",
                "Country" => "AF",
                "AddressLine" => [
                    "Address line: Республика Беларусь, 220145  г. Минск, бульвар имени Банкомата, 13",
                    "Address line: Республика Беларусь, 220145  г. Минск, бульвар имени Банкомата, 13"
                ],
                "Description" => [
                    "desc1",
                    "desc2"
                ],
                "Geolocation" => [
                    "Latitude" => 54.86857,
                    "Longitude" => 28.569547
                ]
            ],
            "ServicesATM" => [
                [
                    "ServiceType" => "CashWithdrawal",
                    "Description" => null,
                    "AdditionalData" => null
                ],
                [
                    "ServiceType" => "Balance",
                    "Description" => null,
                    "AdditionalData" => null
                ]
            ],
            "Availability" => [
                "Access24Hours" => false,
                "IsRestricted" => false,
                "SameAsOrganization" => false,
                "Description" => "sample availability desc",
                "StandardAvailability" => [
                    "Day" => [
                        [
                            "DayCode" => "01",
                            "OpeningTime" => $faker->time('H:i:s'),
                            "ClosingTime" => $faker->time('H:i:s'),
                            "Break" => [
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ]
                            ]
                        ],
                        [
                            "DayCode" => "02",
                            "OpeningTime" => $faker->time('H:i:s'),
                            "ClosingTime" => $faker->time('H:i:s'),
                            "Break" => [
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ],
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ]
                            ]
                        ],
                        [
                            "DayCode" => "03",
                            "OpeningTime" => $faker->time('H:i:s'),
                            "ClosingTime" => $faker->time('H:i:s'),
                            "Break" => [
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ]
                            ]
                        ],
                        [
                            "DayCode" => "04",
                            "OpeningTime" => $faker->time('H:i:s'),
                            "ClosingTime" => $faker->time('H:i:s'),
                            "Break" => [
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ]
                            ]
                        ],
                        [
                            "DayCode" => "05",
                            "OpeningTime" => $faker->time('H:i:s'),
                            "ClosingTime" => $faker->time('H:i:s'),
                            "Break" => [
                                [
                                    "BreakFromTime" => $faker->time('H:i:s'),
                                    "BreakToTime" => $faker->time('H:i:s')
                                ]
                            ]
                        ]
                    ]
                ],
                "NonStandardAvailability" => [
                    [
                        "Name" => "ExtraDayOff",
                        "FromDate" => $faker->date('Y-m-d H:i:s'),
                        "ToDate" => $faker->date('Y-m-d H:i:s'),
                        "Description" => "sample extra day off description",
                        "Day" => [
                            [
                                "DayCode" => "03",
                                "OpeningTime" => $faker->time('H:i:s'),
                                "ClosingTime" => $faker->time('H:i:s'),
                                "Break" => [
                                    [
                                        "BreakFromTime" => $faker->time('H:i:s'),
                                        "BreakToTime" => $faker->time('H:i:s')
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "ContactDetails" => [
                "PhoneNumber" => "+375232984532",
                "Other" => "sample other"
            ],
            "Accessibilities" => [
                [
                    "Type" => "Braille",
                    "Description" => null
                ],
                [
                    "Type" => "AudioCashMachine",
                    "Description" => null
                ]
            ],
            "CurrencyExchange" => [
                [
                    "ExchangeType" => "Online",
                    "SourceCurrency" => $faker->randomElement(['BYN', 'USD', 'EUR']),
                    "TargetCurrency" => $faker->randomElement(['BYN', 'USD', 'EUR']),
                    "UnitCurrency" => 2000,
                    "ExchangeRate" => 2.26,
                    "AmountMin" => 5.5,
                    "AmountMax" => 500,
                    "Direction" => "buy",
                    "DateTime" => $faker->date('Y-m-d H:i:s'),
                    "UpdatedDateTime" => $faker->date('Y-m-d H:i:s')
                ],
                [
                    "ExchangeType" => "Cash",
                    "SourceCurrency" => "BYN",
                    "TargetCurrency" => "EUR",
                    "UnitCurrency" => 3000,
                    "ExchangeRate" => 2.06,
                    "AmountMin" => 5,
                    "AmountMax" => 750.5,
                    "Direction" => "sell",
                    "DateTime" => $faker->date('Y-m-d H:i:s'),
                    "UpdatedDateTime" => $faker->date('Y-m-d H:i:s')
                ]
            ]
        ];

        $atm = app(ServiceCreateATM::class)->execute($data);
        return $atm;
    }
}
