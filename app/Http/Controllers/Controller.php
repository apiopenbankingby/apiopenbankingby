<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\Info(
 *      title="api.openbanking.by",
 *      version="1.0.0",
 *      @OA\License(
 *          name="Спецификация стандарта «Открытые информационные банковские API»",
 *          url="https://docs.google.com/document/d/1P4LCj0ULGYFXzzLucZXP4qT33EJEirAY4rEdBRCfowA/edit"
 *      )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:api')->only(['show', 'index']);
    }
}
