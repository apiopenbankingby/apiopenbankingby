<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Bank;
use App\Http\Controllers\Controller;
use App\Models\Service\ServicesService;
use Illuminate\Http\Request;
use App\Http\Resources\Service\ServicesService as ServicesServiceResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class ServiceController
 *
 * @package App\Http\Controllers\Api\V1
 */
class ServiceController extends Controller
{
    /**
     * Получить данные всех услуг определённого банка.
     *
     * @OA\Get(
     *     tags={"Service"},
     *     path="/banks/{id}/services",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="branch_id",
     *         in="query",
     *         description="Идентификатор банковского отделения",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="service_id",
     *         in="query",
     *         description="Идентификатор услуги",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="Фильтр по значению type",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="segment",
     *         in="query",
     *         description="Фильтр по значению segment",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="current_status",
     *         in="query",
     *         description="Filter by current status",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="currency",
     *         in="query",
     *         description="Фильтр по значению currency",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Services")
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  ref="#/components/schemas/Link"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  ref="#/components/schemas/Meta"
     *              )
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @param Bank $bank
     * @return ResourceCollection
     */
    public function index(Request $request, Bank $bank)
    {
        $serviceId = isset($bank->services->id) ? $bank->services->id : null;
        $paginate = $request->input('limit', config('app.page_limit', ''));
        $input = array_change_key_case($request->query());

        $servicesService = ServicesService::servicesId($serviceId)->filter($input)->paginate($paginate);

        return ServicesServiceResource::collection($servicesService);
    }
}
