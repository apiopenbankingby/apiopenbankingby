<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Http\Resources\Bank as BankResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BankController
 *
 * @package App\Http\Controllers\Api\V1
 */
class BankController extends Controller
{
    /**
     * Получить данные всех банков.
     *
     * @OA\Get(
     *     tags={"Bank"},
     *     path="/banks",
     *     @OA\Parameter(
     *         name="bicfi",
     *         in="query",
     *         description="Фильтр по значению BICFI",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="clearing_system_member_identification",
     *         in="query",
     *         description="Фильтр по значению clearingSystemMemberIdentification",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="clearing_system_identification",
     *         in="query",
     *         description="Фильтр по значению clearingSystemIdentification",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="code",
     *         in="query",
     *         description="Фильтр по значению code",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="proprietary",
     *         in="query",
     *         description="Фильтр по значению proprietary",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="member_identification",
     *         in="query",
     *         description="Фильтр по значению memberIdentification",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="LegalEntityIdentifier",
     *         in="query",
     *         description="Фильтр по значению legal_entity_identifier",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Bank")
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  ref="#/components/schemas/Link"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  ref="#/components/schemas/Meta"
     *              )
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @return ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        $paginate = $request->input('limit', config('app.page_limit', ''));
        $input = array_change_key_case($request->query());

        return BankResource::collection(Bank::filter($input)->paginate($paginate));
    }

    /**
     * Получить данные определённого банка.
     *
     * @OA\Get(
     *     tags={"Bank"},
     *     path="/banks/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(ref="#/components/schemas/Bank")
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found"
     *      )
     * )
     *
     * @param Bank $bank
     * @return BankResource
     */
    public function show(Bank $bank): BankResource
    {
        return BankResource::make($bank);
    }
}
