<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Atm\Atm;
use App\Http\Resources\Atm\Atm as AtmResource;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class AtmController
 *
 * @package App\Http\Controllers\Api\V1
 */
class AtmController extends Controller
{
    /**
     * Получить данные всех ATM определённого банка.
     *
     * @OA\Get(
     *     tags={"ATM"},
     *     path="/banks/{id}/atms",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="Фильтр по значению type",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="current_status",
     *         in="query",
     *         description="Фильтр по значению currentStatus",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="base_currency",
     *         in="query",
     *         description="Фильтр по значению baseCurrency",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="currency",
     *         in="query",
     *         description="Фильтр по значению currency",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="cards",
     *         in="query",
     *         description="Фильтр по значению cards",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/ATM")
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  ref="#/components/schemas/Link"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  ref="#/components/schemas/Meta"
     *              )
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @param Bank $bank
     * @return ResourceCollection
     */
    public function index(Request $request, Bank $bank): ResourceCollection
    {
        $paginate = $request->input('limit', config('app.page_limit', ''));
        $input = array_change_key_case($request->query());

        return AtmResource::collection($bank->atm()->filter($input)->paginate($paginate));
    }

    /**
     * Получить данные определённого ATM определённого банка.
     *
     * @OA\Get(
     *     tags={"ATM"},
     *     path="/banks/{id}/atms/{atmId}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="atmId",
     *         in="path",
     *         required=true,
     *         description="Идентификатор ATM",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(ref="#/components/schemas/ATM")
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found"
     *      )
     * )
     *
     * @param $bankId
     * @param Atm $atm
     * @return AtmResource
     */
    public function show($bankId, Atm $atm): AtmResource
    {
        return AtmResource::make($atm);
    }
}
