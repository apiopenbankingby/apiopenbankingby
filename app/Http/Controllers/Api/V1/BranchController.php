<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\Branch\Branch;
use App\Http\Resources\Branch\Branch as BranchResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class BranchController
 *
 * @package App\Http\Controllers\Api\V1
 */
class BranchController extends Controller
{
    /**
     * Получить данные всех банковских отделений определённого банка.
     *
     * @OA\Get(
     *     tags={"Branch"},
     *     path="/banks/{id}/branches",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Branch")
     *              ),
     *              @OA\Property(
     *                  property="links",
     *                  ref="#/components/schemas/Link"
     *              ),
     *              @OA\Property(
     *                  property="meta",
     *                  ref="#/components/schemas/Meta"
     *              )
     *          )
     *      )
     * )
     *
     * @param Request $request
     * @param Bank $bank
     * @return ResourceCollection
     */
    public function index(Request $request, Bank $bank)
    {
        $paginate = $request->input('limit', config('app.page_limit', ''));

        return BranchResource::collection($bank->branch()->paginate($paginate));
    }

    /**
     * Получить данные определённого банковского отделения определённого банка.
     *
     * @OA\Get(
     *     tags={"Branch"},
     *     path="/banks/{id}/branches/{branchId}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банка",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="branchId",
     *         in="path",
     *         required=true,
     *         description="Идентификатор банковского отделения",
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(ref="#/components/schemas/Branch")
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Not Found"
     *      )
     * )
     *
     * @param $bankId
     * @param Branch $branch
     * @return BranchResource
     */
    public function show($bankId, Branch $branch): BranchResource
    {
        return BranchResource::make($branch);
    }
}
