<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class TokenController extends Controller
{

    public function store(Request $request)
    {
        if (!$user = User::where('email', $request->email)->first()) {
            $user = User::create([
                'email' => $request->email,
            ]);
        }

        $token = $user->createToken('app')->accessToken;

        return view('token.show', compact('token'));
    }

}
