<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Geolocation", type="object")
 */
class Geolocation extends JsonResource
{
    /**
     * @OA\Property(property="Latitude",type="number")
     * @OA\Property(property="Longitude",type="number")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Latitude' => $this->latitude,
            'Longitude' => $this->longitude,
        ];
    }
}
