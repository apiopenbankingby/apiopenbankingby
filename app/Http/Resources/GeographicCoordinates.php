<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="GeographicCoordinates", type="object")
 */
class GeographicCoordinates extends JsonResource
{
    /**
     * @OA\Property(property="Geolocation",ref="#/components/schemas/Geolocation")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Geolocation' => Geolocation::make($this->geolocation),
        ];
    }
}
