<?php

namespace App\Http\Resources\Directory;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class Currency extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->code;
    }
}
