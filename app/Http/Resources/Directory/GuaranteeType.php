<?php

namespace App\Http\Resources\Directory;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class GuaranteeType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return string
     */
    public function toArray($request): string
    {
        return $this->code;
    }
}
