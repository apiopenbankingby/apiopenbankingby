<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="SocialNetwork", type="object")
 */
class SocialNetwork extends JsonResource
{
    /**
     * @OA\Property(property="Network",type="string")
     * @OA\Property(property="URL",type="string")
     * @OA\Property(property="Description",type={"array", "null"})
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Network' => $this->social->code,
            'URL' => $this->url,
            'Description' => $this->description,
        ];
    }
}
