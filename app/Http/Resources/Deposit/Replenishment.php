<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositReplenishment", type="object")
 */
class Replenishment extends JsonResource
{
    /**
     * @OA\Property(property="Replenishment",type="boolean")
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Replenishment' => $this->replenishment,
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'Description' => $this->description,
        ];
    }
}
