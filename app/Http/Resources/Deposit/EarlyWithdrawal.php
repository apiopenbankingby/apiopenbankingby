<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="EarlyWithdrawal", type="object")
 */
class EarlyWithdrawal extends JsonResource
{
    /**
     * @OA\Property(property="WithdrawalType",type="string")
     * @OA\Property(property="IsPartitionalWithdrawal",type="boolean")
     * @OA\Property(property="EarlyWithdrawalFee",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'WithdrawalType' => $this->when($this->type_id, $this->WithdrawalType->code),
            'IsPartitionalWithdrawal' => $this->is_partitional_withdrawal,
            'EarlyWithdrawalFee' => $this->early_withdrawal_fee,
            'Description' => $this->description,
        ];
    }
}
