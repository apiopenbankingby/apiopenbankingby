<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositCurrencyTerm", type="object")
 */
class CurrencyTerm extends JsonResource
{
    /**
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="TaxIncluded",type="boolean")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Period",type="array",@OA\Items(ref="#/components/schemas/DepositPeriod"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'TaxIncluded' => $this->tax_included,
            'AdditionalData' => $this->tax_included,
            'Description' => $this->description,
            'Period' => Period::collection($this->period),
        ];
    }
}
