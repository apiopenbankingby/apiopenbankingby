<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositPeriod", type="object")
 */
class Period extends JsonResource
{
    /**
     * @OA\Property(property="PeriodMin",type="integer")
     * @OA\Property(property="PeriodMax",type="integer")
     * @OA\Property(property="PeriodType",type="string")
     * @OA\Property(property="Interest",type="array",@OA\Items(ref="#/components/schemas/DepositInterest"))
     * @OA\Property(property="Replenishment",ref="#/components/schemas/DepositReplenishment")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PeriodMin' => $this->period_min,
            'PeriodMax' => $this->period_max,
            'PeriodType' => $this->when($this->type_id, $this->periodType->code),
            'Interest' => Interest::collection($this->interest),
            'Replenishment' => $this->when($this->replenishment_id, Replenishment::make($this->replenishment)),
        ];
    }
}
