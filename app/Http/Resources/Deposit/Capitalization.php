<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Capitalization", type="object")
 */
class Capitalization extends JsonResource
{
    /**
     * @OA\Property(property="Capitalization",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'CapitalizationType' => $this->when($this->type_id, $this->capitalizationType->code),
            'Description' => $this->description,
        ];
    }
}
