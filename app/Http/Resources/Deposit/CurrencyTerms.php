<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositCurrencyTerms", type="object")
 */
class CurrencyTerms extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/DepositCurrencyTerm"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => CurrencyTerm::collection($this->term),
        ];
    }
}
