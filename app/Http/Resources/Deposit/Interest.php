<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositInterest", type="object")
 */
class Interest extends JsonResource
{
    /**
     * @OA\Property(property="InterestRate",type="string")
     * @OA\Property(property="Rate",type="number")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'InterestRate' => $this->when($this->rate_id, $this->interestRate->code),
            'Rate' => $this->rate,
            'Description' => $this->description,
        ];
    }
}
