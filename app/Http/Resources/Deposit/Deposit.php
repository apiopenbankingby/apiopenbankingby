<?php

namespace App\Http\Resources\Deposit;

use App\Http\Resources\BranchContactDetails;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Deposit", type="object")
 */
class Deposit extends JsonResource
{
    /**
     * @OA\Property(property="DepositType",type="string")
     * @OA\Property(property="IsEarlyWithdrawalInterest",type="boolean")
     * @OA\Property(property="Currency",type="array",@OA\Items(ref="#/components/schemas/DepositCurrency"))
     * @OA\Property(property="EarlyWithdrawal",ref="#/components/schemas/EarlyWithdrawal")
     * @OA\Property(property="Capitalization",ref="#/components/schemas/Capitalization")
     * @OA\Property(property="ContactDetails",ref="#/components/schemas/ContactDetails")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'DepositType' => $this->when($this->type_id, $this->depositType->code),
            'IsEarlyWithdrawalInterest' => $this->is_early_withdrawal_interest,
            'Currency' => Currency::collection($this->currency),
            'EarlyWithdrawal' => EarlyWithdrawal::make($this->earlyWithdrawal),
            'Capitalization' => Capitalization::make($this->capitalization),
            'ContactDetails' => BranchContactDetails::make($this->contactDetails),
        ];
    }
}
