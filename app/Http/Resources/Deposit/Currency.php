<?php

namespace App\Http\Resources\Deposit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DepositCurrency", type="object")
 */
class Currency extends JsonResource
{
    /**
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="Terms",ref="#/components/schemas/DepositCurrencyTerms")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Currency' => $this->when($this->curr_id, $this->currency->code),
            'Terms' => $this->when($this->deposit_term_id, CurrencyTerms::make($this->depositTerm)),
        ];
    }
}
