<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Directory\Currency;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="BaseCurrencyExchange", type="object")
 */
class CurrencyExchange extends JsonResource
{
    /**
     * @OA\Property(property="ExchangeType",type="string")
     * @OA\Property(property="SourceCurrency",type={"string","null"},nullable=true)
     * @OA\Property(property="TargetCurrency",type={"string","null"},nullable=true)
     * @OA\Property(property="UnitCurrency",type="integer")
     * @OA\Property(property="ExchangeRate",type="number")
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="Direction",type="string")
     * @OA\Property(property="DateTime",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ExchangeType' => $this->exchangeType->code,
            'SourceCurrency' => isset($this->sourceCurrency->code) ? $this->sourceCurrency->code : null,
            'TargetCurrency' => isset($this->targetCurrency->code) ? $this->targetCurrency->code : null,
            'UnitCurrency' => $this->unit_currency,
            'ExchangeRate' => $this->exchange_rate,
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'Direction' => $this->direction,
            'DateTime' => $this->date_time,
            'UpdatedDateTime' => $this->when($this->updated_date_time, $this->updated_date_time),
        ];
    }
}
