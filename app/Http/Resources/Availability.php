<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Availability", type="object")
 */
class Availability extends JsonResource
{
    /**
     * @OA\Property(property="Access24Hours",type="boolean")
     * @OA\Property(property="IsRestricted",type="boolean")
     * @OA\Property(property="SameAsOrganization",type="boolean")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="StandardAvailability",ref="#/components/schemas/StandardAvailability")
     * @OA\Property(property="NonStandardAvailability",type="array",@OA\Items(ref="#/components/schemas/NonStandardAvailability"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Access24Hours' => $this->access_24_hours,
            'IsRestricted' => $this->is_restricted,
            'SameAsOrganization' => $this->same_as_organization,
            'Description' => $this->description,
            'StandardAvailability' => StandardAvailability::make($this->standardAvailability),
            'NonStandardAvailability' => NonStandardAvailability::collection($this->nonStandardAvailability)
        ];
    }
}
