<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Accessibility", type="object")
 */
class Accessibility extends JsonResource
{
    /**
     * @OA\Property(property="Type",type="string")
     * @OA\Property(property="Description",type={"string","null"},nullable=true)
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Type' => $this->accessibilityType->code,
            'Description' => $this->description,
        ];
    }
}
