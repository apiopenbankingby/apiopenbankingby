<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Day", type="object")
 */
class Day extends JsonResource
{
    /**
     * @OA\Property(property="DayCode",type="string")
     * @OA\Property(property="OpeningTime",type="string")
     * @OA\Property(property="ClosingTime",type="string")
     * @OA\Property(property="Break",type="array",@OA\Items(ref="#/components/schemas/Break"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'DayCode' => $this->dayCode->code,
            'OpeningTime' => $this->opening_time,
            'ClosingTime' => $this->closing_time,
            'Break' => DBreak::collection($this->break),
        ];
    }
}
