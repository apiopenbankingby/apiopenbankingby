<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Partners", type="object")
 */
class Partners extends JsonResource
{
    /**
     * @OA\Property(property="PartnerCategory",type="array",@OA\Items(ref="#/components/schemas/PartnersCategory"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PartnerCategory' => $this->when($this->partnersCategory, PartnersCategory::collection($this->partnersCategory)),
        ];
    }
}
