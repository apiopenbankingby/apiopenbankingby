<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Location", type="object")
 */
class Location extends JsonResource
{
    /**
     * @OA\Property(property="StreetName",type="string")
     * @OA\Property(property="BuildingNumber",type="string")
     * @OA\Property(property="Department",type={"string","null"},nullable=true)
     * @OA\Property(property="PostCode",type="string")
     * @OA\Property(property="TownName",type="string")
     * @OA\Property(property="CountrySubDivision",type="string")
     * @OA\Property(property="Country",type="string")
     * @OA\Property(property="AddressLine",type={"array","null"})
     * @OA\Property(property="Description",type={"array","null"})
     * @OA\Property(property="GeographicCoordinates",ref="#/components/schemas/GeographicCoordinates")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'StreetName' => $this->street_name,
            'BuildingNumber' => $this->building_number,
            'Department' => $this->department,
            'PostCode' => $this->post_code,
            'TownName' => $this->town_name,
            'CountrySubDivision' => $this->country_sub_division,
            'Country' => $this->when($this->country, isset($this->country->code2) ? $this->country->code2 : null),
            'AddressLine' => $this->address_line,
            'Description' => $this->description,
            'GeographicCoordinates' => $this->when($this->geographicCoordinates, GeographicCoordinates::make($this->geographicCoordinates)),
        ];
    }
}
