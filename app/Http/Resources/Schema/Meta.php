<?php

namespace App\Http\Resources\Schema;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed current_page
 * @property mixed from
 * @property mixed last_page
 * @property mixed path
 * @property mixed per_page
 * @property mixed to
 * @property mixed total
 * @OA\Schema(schema="Meta", type="object")
 */
class Meta extends JsonResource
{
    /**
     * @OA\Property(property="current_page",type="integer")
     * @OA\Property(property="from",type="integer")
     * @OA\Property(property="last_page",type="integer")
     * @OA\Property(property="path",type="string")
     * @OA\Property(property="per_page",type="integer")
     * @OA\Property(property="to",type="integer")
     * @OA\Property(property="total",type="integer")
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "current_page" => $this->current_page,
            "from" => $this->from,
            "last_page" => $this->last_page,
            "path" => $this->path,
            "per_page" => $this->per_page,
            "to" => $this->to,
            "total" => $this->total
        ];
    }
}
