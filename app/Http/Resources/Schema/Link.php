<?php

namespace App\Http\Resources\Schema;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed first
 * @property mixed last
 * @property mixed prev
 * @property mixed next
 * @OA\Schema(schema="Link", type="object")
 */
class Link extends JsonResource
{
    /**
     * @OA\Property(property="first",type="string")
     * @OA\Property(property="last",type="string")
     * @OA\Property(property="prev",type={"string","null"},nullable=true)
     * @OA\Property(property="next",type={"string","null"},nullable=true)
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first' => $this->first,
            'last' => $this->last,
            'prev' => $this->prev,
            'next' => $this->next
        ];
    }
}
