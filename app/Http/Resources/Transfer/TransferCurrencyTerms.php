<?php

namespace App\Http\Resources\Transfer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="TransferCurrencyTerms", type="object")
 */
class TransferCurrencyTerms extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/TransferCurrencyTerm"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => TransferCurrencyTerm::collection($this->term),
        ];
    }
}
