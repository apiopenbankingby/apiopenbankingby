<?php

namespace App\Http\Resources\Transfer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="TransferCurrency", type="object")
 */
class TransferCurrency extends JsonResource
{
    /**
     * @OA\Property(property="Terms",type="array",@OA\Items(ref="#/components/schemas/TransferCurrencyTerms"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Currency' => $this->currency->code,
            'Terms' => TransferCurrencyTerms::make($this->transferTerm),
        ];
    }
}
