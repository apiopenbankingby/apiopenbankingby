<?php

namespace App\Http\Resources\Transfer;

use App\Http\Resources\Availability;
use App\Http\Resources\BranchContactDetails;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Transfer", type="object")
 */
class Transfer extends JsonResource
{
    /**
     * @OA\Property(property="TransferSystem",type="string")
     * @OA\Property(property="AdditionalServices",type="string")
     * @OA\Property(property="CanReceive",type="boolean")
     * @OA\Property(property="Currency",type="array",@OA\Items(ref="#/components/schemas/TransferCurrency"))
     * @OA\Property(property="ContactDetails",type="array",@OA\Items(ref="#/components/schemas/ContactDetails"))
     * @OA\Property(property="Availability",type="array",@OA\Items(ref="#/components/schemas/Availability"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'TransferSystem' => $this->transferSystem->name,
            'AdditionalServices' => $this->additional_services,
            'CanReceive' => $this->can_receive,
            'Currency' => TransferCurrency::collection($this->currency),
            'ContactDetails' => BranchContactDetails::make($this->contactDetails),
            'Availability' => Availability::make($this->availability),
        ];
    }
}
