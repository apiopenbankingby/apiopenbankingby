<?php

namespace App\Http\Resources\Transfer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="TransferCurrencyTerm", type="object")
 */
class TransferCurrencyTerm extends JsonResource
{
    /**
     * @OA\Property(property="CountryDestination",type="object")
     * @OA\Property(property="TransferPeriod",type="integer")
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="FreeRate",type="number")
     * @OA\Property(property="FreeAmount",type="number")
     * @OA\Property(property="AdditionalData",type="object")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'CountryDestination' => isset($this->countryDestinantion->code2) ? $this->countryDestinantion->code2 : null,
            'TransferPeriod' => $this->transfer_period,
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'FreeRate' => $this->free_rate,
            'FreeAmount' => $this->free_amount,
            'AdditionalData' => $this->additional_data,
        ];
    }
}
