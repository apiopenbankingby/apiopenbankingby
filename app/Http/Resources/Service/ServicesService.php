<?php

namespace App\Http\Resources\Service;

use App\Http\Resources\Card\Card;
use App\Http\Resources\CurrencyExchange\CurrencyExchange;
use App\Http\Resources\Deposit\Deposit;
use App\Http\Resources\DirectDebit\DirectDebit;
use App\Http\Resources\Jewel\Jewel;
use App\Http\Resources\Loan\Loan;
use App\Http\Resources\Transfer\Transfer;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Service", type="object")
 */
class ServicesService extends JsonResource
{
    /**
     * @OA\Property(property="Id",type="string")
     * @OA\Property(property="Branches",type="string")
     * @OA\Property(property="Type",type="string")
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="Segment",type="string")
     * @OA\Property(property="DateTime",type="string")
     * @OA\Property(property="CurrentStatus",type="string")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="URL",type="string")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Deposit",ref="#/components/schemas/Deposit")
     * @OA\Property(property="Loan",ref="#/components/schemas/Loan")
     * @OA\Property(property="Card",ref="#/components/schemas/Card")
     * @OA\Property(property="Jewel",ref="#/components/schemas/Jewel")
     * @OA\Property(property="Transfer",ref="#/components/schemas/Transfer")
     * @OA\Property(property="CurrencyExchange",ref="#/components/schemas/CurrencyExchange")
     * @OA\Property(property="DirectDebit",ref="#/components/schemas/DirectDebit")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $type = isset($this->type->code) ? $this->type->code : null;
        return [
            'Id' => $this->id,
            'Branches' => $this->branches,
            'Type' => $type,
            'Name' => $this->name,
            'Segment' => $this->when($this->segment, isset($this->segment->code) ? $this->segment->code : null),
            'DateTime' => $this->date_time,
            'CurrentStatus' => $this->when($this->currentStatus, isset($this->currentStatus->code) ? $this->currentStatus->code : null),
            'Description' => $this->description,
            'URL' => $this->url,
            'AdditionalData' => $this->additional_data,
            'Deposit' => $this->when($type === 'Deposit', Deposit::make($this->whenLoaded('deposit'))),
            'Loan' => $this->when($type === 'Loan', Loan::make($this->whenLoaded('loan'))),
            'Card' => $this->when($type === 'Card', Card::make($this->whenLoaded('card'))),
            'Jewel' => $this->when($type === 'Jewel', Jewel::make($this->whenLoaded('jewel'))),
            'Transfer' => $this->when($type === 'Transfer', Transfer::make($this->whenLoaded('transfer'))),
            'CurrencyExchange' => $this->when($type === 'CurrencyExchange', CurrencyExchange::make($this->whenLoaded('currencyExchange'))),
            'DirectDebit' => $this->when($type === 'DirectDebit', DirectDebit::make($this->whenLoaded('directDebit'))),
        ];
    }
}
