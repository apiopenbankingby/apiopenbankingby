<?php

namespace App\Http\Resources\Service;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Services", type="object")
 */
class Service extends JsonResource
{
    /**
     * @OA\Property(property="Service",type="array",@OA\Items(ref="#/components/schemas/Service"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Service' => $this->when($this->service, ServicesService::collection($this->service)),
        ];
    }
}
