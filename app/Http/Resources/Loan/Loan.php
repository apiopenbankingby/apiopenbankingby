<?php

namespace App\Http\Resources\Loan;

use App\Http\Resources\BranchContactDetails;
use App\Http\Resources\Partners;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Loan", type="object")
 */
class Loan extends JsonResource
{
    /**
     * @OA\Property(property="LoanCategory",type="string")
     * @OA\Property(property="LoanType",type="array",@OA\Items(type="string"))
     * @OA\Property(property="LoanForm",type="array",@OA\Items(type="string"))
     * @OA\Property(property="ApplicationType",type="array",@OA\Items(type="string"))
     * @OA\Property(property="AdditionalServices",type="array",@OA\Items(type="string"))
     * @OA\Property(property="IsRenewable",type="boolean")
     * @OA\Property(property="Currency",type="array",@OA\Items(ref="#/components/schemas/LoanCurrency"))
     * @OA\Property(property="Eligibility",type="array",@OA\Items(ref="#/components/schemas/LoanEligibility"))
     * @OA\Property(property="ContactDetails",ref="#/components/schemas/ContactDetails")
     * @OA\Property(property="Repayment",type="array",@OA\Items(ref="#/components/schemas/LoanRepayment"))
     * @OA\Property(property="Partners",type="array",@OA\Items(ref="#/components/schemas/Partners"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'LoanCategory' => $this->when($this->loanCategory, $this->loanCategory->name),
            'LoanType' => $this->when($this->loanType, $this->loanType->pluck('code')),
            'LoanForm' => $this->when($this->loanForm, $this->loanForm->pluck('code')),
            'ApplicationType' => $this->when($this->applicationType, $this->applicationType->pluck('code')),
            'AdditionalServices' => $this->when($this->additionalServices, $this->additionalServices->pluck('code')),
            'IsRenewable' => $this->is_renewable,
            'Currency' => $this->when($this->currency, Currency::collection($this->currency)),
            'Eligibility' => $this->when($this->eligibility, Eligibility::collection($this->eligibility)),
            'ContactDetails' => BranchContactDetails::make($this->contactDetails),
            'Repayment' => $this->when($this->repayment, Repayment::collection($this->repayment)),
            'Partners' => $this->when($this->partners, Partners::make($this->partners)),
        ];
    }
}
