<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanEligibility", type="object")
 */
class Eligibility extends JsonResource
{
    /**
     * @OA\Property(property="AgeMin",type="number")
     * @OA\Property(property="AgeMax",type="number")
     * @OA\Property(property="IsSalaryProject",type="boolean")
     * @OA\Property(property="IncomeMin",type="number")
     * @OA\Property(property="WorkPeriod",type="number")
     * @OA\Property(property="IsRelativeIncome",type="boolean")
     * @OA\Property(property="IsResident",type="boolean")
     * @OA\Property(property="NeedIncomeCertificate",type="boolean")
     * @OA\Property(property="IsRetire",type="boolean")
     * @OA\Property(property="IsEntrepreneur",type="boolean")
     * @OA\Property(property="AdditionalData",type="boolean")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AgeMin' => $this->age_min,
            'AgeMax' => $this->age_max,
            'IsSalaryProject' => $this->is_salary_project,
            'IncomeMin' => $this->income_min,
            'WorkPeriod' => $this->work_period,
            'IsRelativeIncome' => $this->is_relative_income,
            'IsResident' => $this->is_resident,
            'NeedIncomeCertificate' => $this->need_income_certificate,
            'IsRetire' => $this->is_retired,
            'IsEntrepreneur' => $this->is_entrepreneur,
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
        ];
    }
}
