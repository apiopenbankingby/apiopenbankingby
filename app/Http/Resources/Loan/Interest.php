<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanInterest", type="object")
 */
class Interest extends JsonResource
{
    /**
     * @OA\Property(property="InterestRate",type="string")
     * @OA\Property(property="Rate",type="number")
     * @OA\Property(property="EarlyRepaymentInterest",type="string")
     * @OA\Property(property="GracePeriodInterest",type="integer")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'InterestRate' => $this->when($this->rate_id, $this->interestRate->code),
            'Rate' => $this->rate,
            'EarlyRepaymentInterest' => $this->early_repayment_interest,
            'GracePeriodInterest' => $this->grace_period_interest,
            'Description' => $this->description,
        ];
    }
}
