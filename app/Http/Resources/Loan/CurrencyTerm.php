<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanCurrencyTerm", type="object")
 */
class CurrencyTerm extends JsonResource
{
    /**
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="Period",type="integer")
     * @OA\Property(property="PeriodType",type="string")
     * @OA\Property(property="OwnFunds",type="number")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Guarantee",type="array",@OA\Items(ref="#/components/schemas/LoanGuarantee"))
     * @OA\Property(property="Interest",ref="#/components/schemas/LoanInterest")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'Period' => $this->period,
            'PeriodType' => $this->when($this->period_type_id, $this->periodType->code),
            'OwnFunds' => $this->own_funds,
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
            'Guarantee' => $this->when($this->guarantee, Guarantee::collection($this->guarantee)),
            'Interest' => $this->when($this->interest, Interest::make($this->interest)),
        ];
    }
}
