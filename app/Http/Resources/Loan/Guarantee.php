<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanGuarantee", type="object")
 */
class Guarantee extends JsonResource
{
    /**
     * @OA\Property(property="GuaranteeType",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'GuaranteeType' => $this->when($this->guaranteeType, $this->guaranteeType->code),
            'Description' => $this->description
        ];
    }
}
