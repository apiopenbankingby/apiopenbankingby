<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanCurrency", type="object")
 */
class Currency extends JsonResource
{
    /**
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="Terms",ref="#/components/schemas/LoanCurrencyTerms")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Currency' => $this->when($this->curr_id, $this->currency->code),
            'Terms' => $this->when($this->loan_term_id, CurrencyTerms::make($this->loanTerm)),
        ];
    }
}
