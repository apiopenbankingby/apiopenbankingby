<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanCurrencyTerms", type="object")
 */
class CurrencyTerms extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/LoanCurrencyTerm"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => CurrencyTerm::collection($this->term),
        ];
    }
}
