<?php

namespace App\Http\Resources\Loan;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoanRepayment", type="object")
 */
class Repayment extends JsonResource
{
    /**
     * @OA\Property(property="RepaymentType",type="string")
     * @OA\Property(property="GracePeriod",type="integer")
     * @OA\Property(property="IsEarlyRepayment",type="boolean")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'RepaymentType' => $this->when($this->repayment_type_id, $this->repaymentType->code),
            'GracePeriod' => $this->grace_period,
            'IsEarlyRepayment' => $this->is_early_repayment,
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
        ];
    }
}
