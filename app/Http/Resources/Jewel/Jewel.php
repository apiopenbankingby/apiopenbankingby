<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Jewel", type="object")
 */
class Jewel extends JsonResource
{
    /**
     * @OA\Property(property="PreciousMetals",type="array",@OA\Items(ref="#/components/schemas/JewelPreciousMetals"))
     * @OA\Property(property="Coins",type="array",@OA\Items(ref="#/components/schemas/JewelCoins"))
     * @OA\Property(property="Stones",type="array",@OA\Items(ref="#/components/schemas/JewelStones"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PreciousMetals' => JewelPreciousMetals::make($this->whenLoaded('preciousMetal')),
            'Coins' => JewelCoins::make($this->whenLoaded('coins')),
            'Stones' => JewelStones::make($this->whenLoaded('stones')),
        ];
    }
}
