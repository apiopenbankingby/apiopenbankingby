<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelStone", type="object")
 */
class JewelStone extends JsonResource
{
    /**
     * @OA\Property(property="Material",type="string")
     * @OA\Property(property="Weight",type="number")
     * @OA\Property(property="Certificate",type="string")
     * @OA\Property(property="Form",type="string")
     * @OA\Property(property="Color",type="string")
     * @OA\Property(property="Clarity",type="string")
     * @OA\Property(property="SellingPrice",type="number")
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="DateTime",type="string")
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Material' => $this->material->code,
            'Weight' => $this->weight,
            'Certificate' => $this->certificate,
            'Form' => $this->form,
            'Color' => $this->color,
            'Clarity' => $this->clarity,
            'SellingPrice' => $this->selling_price,
            'Currency' => $this->currency->code,
            'DateTime' => $this->date_time,
        ];
    }
}
