<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelPreciousMetals", type="object")
 */
class JewelPreciousMetals extends JsonResource
{
    /**
     * @OA\Property(property="PreciousMetal",type="array",@OA\Items(ref="#/components/schemas/JewelPreciousMetal"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PreciousMetal' => JewelPreciousMetal::collection($this->preciousMetal)
        ];
    }
}
