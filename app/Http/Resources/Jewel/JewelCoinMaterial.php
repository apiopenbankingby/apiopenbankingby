<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelCoinMaterial", type="object")
 */
class JewelCoinMaterial extends JsonResource
{
    /**
     * @OA\Property(property="Material",type="string")
     * @OA\Property(property="MetalCustom",type="string")
     * @OA\Property(property="Sample",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Material' => $this->jewelType->code,
            'MetalCustom' => $this->custom_metal,
            'Sample' => $this->sample,
            'Description' => $this->description,
        ];
    }
}
