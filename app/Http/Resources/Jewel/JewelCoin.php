<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelCoin", type="object")
 */
class JewelCoin extends JsonResource
{
    /**
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="Value",type="integer")
     * @OA\Property(property="Quality",type="string")
     * @OA\Property(property="SellingPrice",type="number")
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="DateTime",type="string")
     * @OA\Property(property="Descriptions",type="string")
     * @OA\Property(property="CoinMaterial",type="array",@OA\Items(ref="#/components/schemas/JewelCoinMaterial"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Name' => $this->name,
            'Value' => $this->value,
            'Quality' => $this->quality,
            'SellingPrice' => $this->selling_price,
            'Currency' => $this->currency->code,
            'DateTime' => $this->date_time,
            'Descriptions' => $this->descriptin,
            'CoinMaterial' => JewelCoinMaterial::collection($this->coinMaterial)
        ];
    }
}
