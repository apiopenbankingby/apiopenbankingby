<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelPreciousMetal", type="object")
 */
class JewelPreciousMetal extends JsonResource
{
    /**
     * @OA\Property(property="Material",type="string")
     * @OA\Property(property="Weight",type="number")
     * @OA\Property(property="Sample",type="number")
     * @OA\Property(property="SellingPrice",type="number")
     * @OA\Property(property="PurchasePrice",type="number")
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="DateTime",type="string")
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Material' => $this->material->code,
            'Weight' => $this->weight,
            'Sample' => $this->sample,
            'SellingPrice' => $this->selling_price,
            'PurchasePrice' => $this->purchase_price,
            'Currency' => $this->currency->code,
            'DateTime' => $this->date_time,
        ];
    }
}
