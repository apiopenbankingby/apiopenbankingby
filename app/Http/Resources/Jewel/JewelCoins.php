<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelCoins", type="object")
 */
class JewelCoins extends JsonResource
{
    /**
     * @OA\Property(property="Coin",type="array",@OA\Items(ref="#/components/schemas/JewelCoin"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Coin' => JewelCoin::collection($this->coin)
        ];
    }
}
