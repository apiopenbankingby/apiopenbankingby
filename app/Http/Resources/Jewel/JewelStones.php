<?php

namespace App\Http\Resources\Jewel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(schema="JewelStones", type="object")
 */
class JewelStones extends JsonResource
{
    /**
     * @OA\Property(property="Stone",type="array",@OA\Items(ref="#/components/schemas/JewelStone"))
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Stone' => JewelStone::collection($this->stone),
        ];
    }
}
