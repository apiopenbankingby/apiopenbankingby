<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CustomFee", type="object")
 */
class CustomFee extends JsonResource
{
    /**
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="FeeRate",type="number")
     * @OA\Property(property="FeeAmount",type="number")
     * @OA\Property(property="FeeAmountMin",type="number")
     * @OA\Property(property="FeeAmountMax",type="number")
     * @OA\Property(property="Frequency",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Name' => $this->name,
            'FeeRate' => $this->fee_rate,
            'FeeAmount' => $this->fee_amount,
            'FeeAmountMin' => $this->fee_amount_min,
            'FeeAmountMax' => $this->fee_amount_max,
            'Frequency' => $this->when($this->frequency, isset($this->frequency->code) ? $this->frequency->code : null),
            'Description' => $this->description,
        ];
    }
}
