<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Directory\Currency;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Card", type="object")
 */
class Card extends JsonResource
{
    /**
     * @OA\Property(property="PaymentCardSystem",type="array",@OA\Items(type="string"))
     * @OA\Property(property="CardCategory",type="string")
     * @OA\Property(property="CardType",type="string")
     * @OA\Property(property="ConsumerCard",type="string")
     * @OA\Property(property="CardDataEntryMode",type="string")
     * @OA\Property(property="ForeignCountryPayments",type="boolean")
     * @OA\Property(property="CustomDesign",type="boolean")
     * @OA\Property(property="InternetPayments",type="boolean")
     * @OA\Property(property="InstantIssue",type="boolean")
     * @OA\Property(property="UrgentIssue",type="boolean")
     * @OA\Property(property="PermanentBalance",type="boolean")
     * @OA\Property(property="IssueAdditionalCard",type="boolean")
     * @OA\Property(property="Notifications",type="boolean")
     * @OA\Property(property="InterestRateOnBalance",type="boolean")
     * @OA\Property(property="BankPartners",type="array",@OA\Items(type="string"))
     * @OA\Property(property="MobilePayments",type="string")
     * @OA\Property(property="FeeCharges",type="array",@OA\Items(ref="#/components/schemas/FeeCharges"))
     * @OA\Property(property="CardExpirationDate",ref="#/components/schemas/ExpirationDate")
     * @OA\Property(property="Currency",type="string")
     * @OA\Property(property="CashBack",ref="#/components/schemas/CashBack")
     * @OA\Property(property="LoyaltyProgram",ref="#/components/schemas/LoyaltyProgram")
     * @OA\Property(property="Instalment",ref="#/components/schemas/Instalment")
     * @OA\Property(property="CreditProduct",ref="#/components/schemas/CreditProduct")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PaymentCardSystem' => $this->when($this->paymentCardSystem, PaymentCardSystem::collection($this->paymentCardSystem)),
            'CardCategory' => $this->when($this->cardCategory, $this->cardCategory->code),
            'CardType' => $this->when($this->cardType, $this->cardType->code),
            'ConsumerCard' => $this->when($this->consumerCard, isset($this->consumerCard->code) ? $this->consumerCard->code : null),
            'CardDataEntryMode' => $this->when($this->cardDataEntryMode, $this->cardDataEntryMode->code),
            'ForeignCountryPayments' => $this->foreign_country_payments,
            'CustomDesign' => $this->custom_design,
            'InternetPayments' => $this->internet_payments,
            'InstantIssue' => $this->instant_issue,
            'UrgentIssue' => $this->urgent_issue,
            'PermanentBalance' => $this->permanent_balance,
            'IssueAdditionalCard' => $this->issue_additional_card,
            'Notifications' => $this->notifications,
            'InterestRateOnBalance' => $this->interest_rate_on_balance,
            'BankPartners' => $this->bank_partners,
            'MobilePayments' => $this->mobile_payments,
            'FeeCharges' => $this->when($this->feeCharges, FeeCharges::collection($this->feeCharges)),
            'CardExpirationDate' => $this->when($this->cardExpirationDate, ExpirationDate::make($this->cardExpirationDate)),
            'Currency' => $this->when($this->currency, isset($this->currency->code) ? $this->currency->code : null),
            'CashBack' => $this->when($this->cashBack, CashBack::make($this->cashBack)),
            'LoyaltyProgram' => $this->when($this->loyaltyProgram, LoyaltyProgram::make($this->loyaltyProgram)),
            'Instalment' => $this->when($this->instalment, Instalment::make($this->instalment)),
            'CreditProduct' => $this->when($this->creditProduct, CreditProduct::make($this->creditProduct)),
        ];
    }
}
