<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="ATMWithdrawalFee", type="object")
 */
class ATMWithdrawalFee extends JsonResource
{
    /**
     * @OA\Property(property="IssuerATM",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="BankPartnersATM",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="OtherATM",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="NonResidentATM",ref="#/components/schemas/IssuerBank")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'IssuerATM' => $this->when($this->issuerATM, IssuerBank::make($this->issuerATM)),
            'BankPartnersATM' => $this->when($this->bankPartnersATM, IssuerBank::make($this->bankPartnersATM)),
            'OtherATM' => $this->when($this->otherATM, IssuerBank::make($this->otherATM)),
            'NonResidentATM' => $this->when($this->nonResidentATM, IssuerBank::make($this->nonResidentATM)),
        ];
    }
}
