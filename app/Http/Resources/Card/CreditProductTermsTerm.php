<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CreditProductTermsTerm", type="object")
 */
class CreditProductTermsTerm extends JsonResource
{
    /**
     * @OA\Property(property="AmountMin", type="number")
     * @OA\Property(property="AmountMax", type="number")
     * @OA\Property(property="Period", type="integer")
     * @OA\Property(property="Description", type="string")
     * @OA\Property(property="AdditionalData", type="object")
     * @OA\Property(property="Interest", ref="#/components/schemas/CreditProductInterest")
     * @OA\Property(property="Guarantee",type="array",@OA\Items(ref="#/components/schemas/CreditProductGuarantee"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'Period' => $this->period,
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
            'Interest' => $this->when($this->interest, CreditProductInterest::make($this->interest)),
            'Guarantee' => $this->when($this->guarantee, CreditProductGuarantee::collection($this->guarantee)),
        ];
    }
}
