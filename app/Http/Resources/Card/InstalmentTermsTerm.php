<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="InstalmentTermsTerm", type="object")
 */
class InstalmentTermsTerm extends JsonResource
{
    /**
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="PeriodMin",type="integer")
     * @OA\Property(property="PeriodMax",type="integer")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Interest",ref="#/components/schemas/Interest")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'PeriodMin' => $this->period_min,
            'PeriodMax' => $this->period_max,
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
            'Interest' => $this->when($this->interest, Interest::make($this->interest)),
        ];
    }
}
