<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CheckFee", type="object")
 */
class CheckFee extends JsonResource
{
    /**
     * @OA\Property(property="IssuerBank",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="BankPartners",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="OtherBank",ref="#/components/schemas/IssuerBank")
     * @OA\Property(property="NonResidentBank",ref="#/components/schemas/IssuerBank")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'IssuerBank' => $this->when($this->issuerBank, IssuerBank::make($this->issuerBank)),
            'BankPartners' => $this->when($this->bankPartners, IssuerBank::make($this->bankPartners)),
            'OtherBank' => $this->when($this->otherBank, IssuerBank::make($this->otherBank)),
            'NonResidentBank' => $this->when($this->nonResidentBank, IssuerBank::make($this->nonResidentBank)),
        ];
    }
}
