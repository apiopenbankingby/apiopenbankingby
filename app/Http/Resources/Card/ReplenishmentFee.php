<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed fee_amount_min
 * @property mixed fee_amount_max
 * @property mixed fee_rate
 * @property mixed description
 * @OA\Schema(schema="ReplenishmentFee", type="object")
 */
class ReplenishmentFee extends JsonResource
{
    /**
     * @OA\Property(property="FeeAmountMin",type="number")
     * @OA\Property(property="FeeAmountMax",type="number")
     * @OA\Property(property="FeeRate",type="number")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'FeeAmountMin' => $this->fee_amount_min,
            'FeeAmountMax' => $this->fee_amount_max,
            'FeeRate' => $this->fee_rate,
            'Description' => $this->description,
        ];
    }
}
