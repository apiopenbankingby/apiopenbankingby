<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="InstalmentTerm", type="object")
 */
class InstalmentTerm extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/InstalmentTermsTerm"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => $this->when($this->term, InstalmentTermsTerm::collection($this->term)),
        ];
    }
}
