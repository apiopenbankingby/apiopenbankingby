<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="ReIssueFee", type="object")
 */
class ReIssueFee extends JsonResource
{
    /**
     * @OA\Property(property="ChangeDataFee",type="number")
     * @OA\Property(property="VOIDFee",type="number")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ChangeDataFee' => $this->change_data_fee,
            'VOIDFee' => $this->VOID_fee,
        ];
    }
}
