<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed fee_e_issue
 * @property mixed fee_one_time
 * @property mixed fee_monthly
 * @property mixed fee_annual
 * @OA\Schema(schema="ServiceFee", type="object")
 */
class ServiceFee extends JsonResource
{
    /**
     * @OA\Property(property="FeeAnnual",type="number")
     * @OA\Property(property="FeeMonthly",type="number")
     * @OA\Property(property="FeeOneTime",type="number")
     * @OA\Property(property="FeeEIssue",type="number")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'FeeAnnual' => $this->fee_annual,
            'FeeMonthly' => $this->fee_monthly,
            'FeeOneTime' => $this->fee_one_time,
            'FeeEIssue' => $this->fee_e_issue,
        ];
    }
}
