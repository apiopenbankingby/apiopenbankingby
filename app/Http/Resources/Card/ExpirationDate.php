<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="ExpirationDate", type="object")
 */
class ExpirationDate extends JsonResource
{
    /**
     * @OA\Property(property="Months",type="integer")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Months' => $this->months
        ];
    }
}
