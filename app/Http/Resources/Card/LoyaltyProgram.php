<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Partners;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="LoyaltyProgram", type="object")
 */
class LoyaltyProgram extends JsonResource
{
    /**
     * @OA\Property(property="Partners",type="array",@OA\Items(ref="#/components/schemas/Partners"))
     * @OA\Property(property="PaymentSystemLoyaltyURL",type="string")
     * @OA\Property(property="LoyaltyServices",type="array",@OA\Items(type="string"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Partners' => $this->when($this->partners, Partners::collection($this->partners)),
            'PaymentSystemLoyaltyURL' => $this->payment_system_loyalty_url,
            'LoyaltyServices' => $this->loyalty_services,
        ];
    }
}
