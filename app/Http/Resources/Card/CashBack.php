<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Partners;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CashBack", type="object")
 */
class CashBack extends JsonResource
{
    /**
     * @OA\Property(property="AmountMin",type="integer")
     * @OA\Property(property="AmountMax",type="integer")
     * @OA\Property(property="TransactionMin",type="integer")
     * @OA\Property(property="NonPartnersRate",type="number")
     * @OA\Property(property="Frequency",type="string")
     * @OA\Property(property="AdditionalData",type="object")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Partners",type="array",@OA\Items(ref="#/components/schemas/Partners"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'TransactionMin' => $this->transaction_min,
            'NonPartnersRate' => $this->non_partners_rate,
            'Frequency' => $this->when($this->frequency, isset($this->frequency->code) ? $this->frequency->code : null),
            'AdditionalData' => $this->additional_data,
            'Description' => $this->description,
            'Partners' => $this->when($this->partners, Partners::collection($this->partners)),
        ];
    }
}
