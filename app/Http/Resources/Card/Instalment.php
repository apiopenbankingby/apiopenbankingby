<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Partners;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Instalment", type="object")
 */
class Instalment extends JsonResource
{
    /**
     * @OA\Property(property="Partners",type="array",@OA\Items(ref="#/components/schemas/Partners"))
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Terms",ref="#/components/schemas/InstalmentTerm")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Partners' => $this->when($this->partners, Partners::collection($this->partners)),
            'Description' => $this->description,
            'Terms' => $this->when($this->terms, InstalmentTerm::make($this->terms)),
        ];
    }
}
