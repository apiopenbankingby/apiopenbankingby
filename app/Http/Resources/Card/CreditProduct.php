<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CreditProduct", type="object")
 */
class CreditProduct extends JsonResource
{
    /**
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Terms",ref="#/components/schemas/CreditProductTerm")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Description' => $this->description,
            'Terms' => $this->when($this->terms, CreditProductTerm::make($this->terms)),
        ];
    }
}
