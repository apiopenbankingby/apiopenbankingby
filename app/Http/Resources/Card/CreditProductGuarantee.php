<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Directory\GuaranteeType;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CreditProductGuarantee", type="object")
 */
class CreditProductGuarantee extends JsonResource
{
    /**
     * @OA\Property(property="GuaranteeType",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'GuaranteeType' => $this->when($this->guaranteeType, GuaranteeType::collection($this->guaranteeType)),
            'Description' => $this->description,
        ];
    }
}
