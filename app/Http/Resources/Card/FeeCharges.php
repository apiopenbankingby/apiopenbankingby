<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="FeeCharges", type="object")
 */
class FeeCharges extends JsonResource
{
    /**
     * @OA\Property(property="IssueFee",type="number")
     * @OA\Property(property="InstantIssueFee",type="number")
     * @OA\Property(property="UrgentIssueFee",type="number")
     * @OA\Property(property="AdditionalCardIssueFee",type="number")
     * @OA\Property(property="NotificationFee",type="number")
     * @OA\Property(property="ReIssueFee",ref="#/components/schemas/ReIssueFee")
     * @OA\Property(property="ReplenishmentFee",type="array",@OA\Items(ref="#/components/schemas/ReplenishmentFee"))
     * @OA\Property(property="ServiceFee",ref="#/components/schemas/ServiceFee")
     * @OA\Property(property="BalanceCheckFee",ref="#/components/schemas/CheckFee")
     * @OA\Property(property="CardTransferFee",ref="#/components/schemas/CheckFee")
     * @OA\Property(property="BankWithdrawalFee",ref="#/components/schemas/CheckFee")
     * @OA\Property(property="ATMWithdrawalFee",ref="#/components/schemas/ATMWithdrawalFee")
     * @OA\Property(property="CustomFee",type="array",@OA\Items(ref="#/components/schemas/CustomFee"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'IssueFee' => $this->issue_fee,
            'InstantIssueFee' => $this->instant_issue_fee,
            'UrgentIssueFee' => $this->urgent_issue_fee,
            'AdditionalCardIssueFee' => $this->additional_card_issue_fee,
            'NotificationFee' => $this->notification_fee,
            'ReIssueFee' => $this->when($this->reIssueFee, ReIssueFee::make($this->reIssueFee)),
            'ReplenishmentFee' => $this->when($this->replenishmentFee, ReplenishmentFee::collection($this->replenishmentFee)),
            'ServiceFee' => $this->when($this->serviceFee, ServiceFee::make($this->serviceFee)),
            'BalanceCheckFee' => $this->when($this->balanceCheckFee, CheckFee::make($this->balanceCheckFee)),
            'CardTransferFee' => $this->when($this->cardTransferFee, CheckFee::make($this->cardTransferFee)),
            'BankWithdrawalFee' => $this->when($this->bankWithdrawalFee, CheckFee::make($this->bankWithdrawalFee)),
            'ATMWithdrawalFee' => $this->when($this->ATMWithdrawalFee, ATMWithdrawalFee::make($this->ATMWithdrawalFee)),
            'CustomFee' => $this->when($this->customFee, CustomFee::collection($this->customFee)),
        ];
    }
}
