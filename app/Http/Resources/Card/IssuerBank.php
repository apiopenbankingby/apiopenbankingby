<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="IssuerBank", type="object")
 */
class IssuerBank extends JsonResource
{
    /**
     * @OA\Property(property="FeeAmount",type="number")
     * @OA\Property(property="FeeRate",type="number")
     * @OA\Property(property="FeeAmountMin",type="number")
     * @OA\Property(property="FeeAmountMax",type="number")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'FeeAmount' => $this->when($this->fee_amount, $this->fee_amount),
            'FeeRate' => $this->when($this->fee_rate, $this->fee_rate),
            'FeeAmountMin' => $this->when($this->fee_amount_min, $this->fee_amount_min),
            'FeeAmountMax' => $this->when($this->fee_amount_max, $this->fee_amount_max),
        ];
    }
}
