<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Deposit\Period;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Interest", type="object")
 */
class Interest extends JsonResource
{
    /**
     * @OA\Property(property="Rate",type="number")
     * @OA\Property(property="EarlyRepaymentInterest",type="string")
     * @OA\Property(property="Description",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Rate' => $this->rate,
            'EarlyRepaymentInterest' => $this->early_repayment_interest,
            'Description' => $this->description,
        ];
    }
}
