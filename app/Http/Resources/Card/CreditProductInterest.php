<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CreditProductInterest", type="object")
 */
class CreditProductInterest extends JsonResource
{
    /**
     * @OA\Property(property="InterestRate", type="string")
     * @OA\Property(property="Rate", type="number")
     * @OA\Property(property="EarlyRepaymentInterest", type="string")
     * @OA\Property(property="GracePeriodInterest", type="integer")
     * @OA\Property(property="Description", type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'InterestRate' => $this->when($this->interestRate, isset($this->interestRate->code) ? $this->interestRate->code : null),
            'Rate' => $this->rate,
            'EarlyRepaymentInterest' => $this->early_repayment_interest,
            'GracePeriodInterest' => $this->grace_period_interest,
            'Description' => $this->description,
        ];
    }
}
