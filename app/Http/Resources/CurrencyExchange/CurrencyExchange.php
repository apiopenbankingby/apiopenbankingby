<?php

namespace App\Http\Resources\CurrencyExchange;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CurrencyExchange", type="object")
 */
class CurrencyExchange extends JsonResource
{
    /**
     * @OA\Property(property="Terms",type="array",@OA\Items(ref="#/components/schemas/CurrencyExchangeTerms"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Terms' => CurrencyExchangeTerms::make($this->terms),
        ];
    }
}
