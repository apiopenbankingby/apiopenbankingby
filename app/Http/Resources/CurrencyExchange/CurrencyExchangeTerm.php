<?php

namespace App\Http\Resources\CurrencyExchange;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CurrencyExchangeTerm", type="object")
 */
class CurrencyExchangeTerm extends JsonResource
{
    /**
     * @OA\Property(property="ExchangeType",type="string")
     * @OA\Property(property="SourceCurrency",type="string")
     * @OA\Property(property="TargetCurrency",type="string")
     * @OA\Property(property="Scale",type="number")
     * @OA\Property(property="ExchangeRate",type="number")
     * @OA\Property(property="AmountMin",type="number")
     * @OA\Property(property="AmountMax",type="number")
     * @OA\Property(property="Direction",type="string")
     * @OA\Property(property="DateTime",type="string")
     * @OA\Property(property="UpdatedTime",type="string")
     * @OA\Property(property="AdditionalData",type="object")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ExchangeType' => $this->exchangeType->name,
            'SourceCurrency' => $this->sourceCurrency->code,
            'TargetCurrency' => $this->targetCurrency->code,
            'Scale' => $this->scale_currency,
            'ExchangeRate' => $this->exchange_rate,
            'AmountMin' => $this->amount_min,
            'AmountMax' => $this->amount_max,
            'Direction' => $this->direction,
            'DateTime' => $this->date_time,
            'UpdatedDateTime' => $this->updated_date_time,
            'AdditionalData' => $this->additional_data,
        ];
    }
}
