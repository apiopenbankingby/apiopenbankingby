<?php

namespace App\Http\Resources\CurrencyExchange;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="CurrencyExchangeTerms", type="object")
 */
class CurrencyExchangeTerms extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/CurrencyExchangeTerm"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => CurrencyExchangeTerm::collection($this->term),
        ];
    }
}
