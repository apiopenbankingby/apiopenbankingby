<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Break", type="object")
 */
class DBreak extends JsonResource
{
    /**
     * @OA\Property(property="BreakFromTime",type="string")
     * @OA\Property(property="BreakToTime",type="string")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'BreakFromTime' => $this->break_from_time,
            'BreakToTime' => $this->break_to_time,
        ];
    }
}
