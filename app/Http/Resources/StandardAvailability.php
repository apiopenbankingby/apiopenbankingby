<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="StandardAvailability", type="object")
 */
class StandardAvailability extends JsonResource
{
    /**
     * @OA\Property(property="Day",type="array",@OA\Items(ref="#/components/schemas/Day"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Day' => Day::collection($this->day),
        ];
    }
}
