<?php

namespace App\Http\Resources\Atm;

use App\Http\Resources\Accessibilities;
use App\Http\Resources\Directory\Currency;
use App\Http\Resources\Directory\Card;
use App\Http\Resources\Availability;
use App\Http\Resources\CurrencyExchange;
use App\Http\Resources\Location;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed type
 * @property mixed baseCurrency
 * @property mixed currency
 * @property mixed cards
 * @property mixed currentStatus
 * @property mixed description
 * @property mixed additional_data
 * @property mixed location
 * @property mixed services
 * @property mixed availability
 * @property mixed contactDetails
 * @property mixed accessibilities
 * @property mixed currencyExchange
 * @OA\Schema(schema="ATM", type="object")
 */
class Atm extends JsonResource
{
    /**
     * @OA\Property(property="Id",type="string")
     * @OA\Property(property="Type",type="string")
     * @OA\Property(property="BaseCurrency",type="string")
     * @OA\Property(property="Currency",type={"array","null"})
     * @OA\Property(property="Cards",type={"array","null"})
     * @OA\Property(property="CurrentStatus",type="string")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Location",ref="#/components/schemas/Location")
     * @OA\Property(property="Services",ref="#/components/schemas/AtmServices")
     * @OA\Property(property="Availability",ref="#/components/schemas/Availability")
     * @OA\Property(property="ContactDetails",ref="#/components/schemas/AtmContactDetails")
     * @OA\Property(property="Accessibilities",ref="#/components/schemas/Accessibilities")
     * @OA\Property(property="CurrencyExchange",type="array",@OA\Items(ref="#/components/schemas/BaseCurrencyExchange"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Id' => $this->id,
            'Type' => $this->when($this->type, isset($this->type->code) ? $this->type->code : null),
            'BaseCurrency' => $this->baseCurrency->code,
            'Currency' => Currency::collection($this->currency),
            'Cards' => Card::collection($this->cards),
            'CurrentStatus' => $this->currentStatus->code,
            'Description' => $this->description,
            'AdditionalData' => $this->additional_data,
            'Location' => Location::make($this->location),
            'ServicesATM' => AtmServices::make($this->services),
            'Availability' => Availability::make($this->availability),
            'ContactDetails' => AtmContactDetails::make($this->contactDetails),
            'Accessibilities' => Accessibilities::make($this->accessibilities),
            'CurrencyExchange' => CurrencyExchange::collection($this->currencyExchange)
        ];
    }
}
