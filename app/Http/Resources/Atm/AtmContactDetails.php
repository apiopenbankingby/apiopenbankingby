<?php

namespace App\Http\Resources\Atm;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed phone_number
 * @property mixed other
 * @OA\Schema(schema="AtmContactDetails", type="object")
 */
class AtmContactDetails extends JsonResource
{
    /**
     * @OA\Property(property="PhoneNumber",type="string")
     * @OA\Property(property="Other",type="string")
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PhoneNumber' => $this->phone_number,
            'Other' => $this->other,
        ];
    }
}
