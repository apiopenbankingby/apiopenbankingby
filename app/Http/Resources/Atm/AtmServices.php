<?php

namespace App\Http\Resources\Atm;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed atmService
 * @OA\Schema(schema="AtmServices", type="object")
 */
class AtmServices extends JsonResource
{
    /**
     * @OA\Property(property="ServiceATM",type="array",@OA\Items(ref="#/components/schemas/AtmService"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ServiceATM' => AtmService::collection($this->atmService),
        ];
    }
}
