<?php

namespace App\Http\Resources\Atm;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed atmServiceType
 * @property mixed description
 * @property mixed additional_data
 * @OA\Schema(schema="AtmService", type="object")
 */
class AtmService extends JsonResource
{
    /**
     * @OA\Property(property="ServiceName",type="string")
     * @OA\Property(property="Description",type={"string","null"},nullable=true)
     * @OA\Property(property="AdditionalData",type={"object","null"})
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ServiceType' => $this->atmServiceType->code,
            'Description' => $this->description,
            'AdditionalData' => $this->additional_data,
        ];
    }
}
