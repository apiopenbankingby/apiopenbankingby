<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Accessibilities", type="object")
 */
class Accessibilities extends JsonResource
{
    /**
     * @OA\Property(property="Accessibility",type="array",@OA\Items(ref="#/components/schemas/Accessibility"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Accessibility' => Accessibility::collection($this->accessibility)
        ];
    }
}
