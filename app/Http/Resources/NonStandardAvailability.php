<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="NonStandardAvailability", type="object")
 */
class NonStandardAvailability extends JsonResource
{
    /**
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="FromDate",type="string")
     * @OA\Property(property="ToDate",type="string")
     * @OA\Property(property="Description",type="string")
     * @OA\Property(property="Day",type="array",@OA\Items(ref="#/components/schemas/Day"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Name' => $this->name,
            'FromDate' => $this->from_date,
            'ToDate' => $this->to_date,
            'Description' => $this->description,
            'Day' => Day::collection($this->day),
        ];
    }
}
