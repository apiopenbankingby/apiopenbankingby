<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Partner", type="object")
 */
class PartnersPartner extends JsonResource
{
    /**
     * @OA\Property(property="PartnerName",type="number")
     * @OA\Property(property="PartnerUIN",type="string")
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'PartnerName' => $this->when($this->partner_name, $this->partner_name),
            'PartnerUIN' => $this->when($this->partner_uin, $this->partner_uin),
        ];
    }
}
