<?php

namespace App\Http\Resources;

use App\Http\Resources\Atm\Atm;
use App\Http\Resources\Branch\Branch;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed clearing_system_identification
 * @property mixed bicfi
 * @property mixed id
 * @property mixed clearing_system_member_identification
 * @property mixed code
 * @property mixed proprietary
 * @property mixed member_identification
 * @property mixed name
 * @property mixed legal_entity_identifier
 * @property mixed location
 * @OA\Schema(schema="Bank", type="object")
 */
class Bank extends JsonResource
{
    /**
     * @OA\Property(property="ID",type="string")
     * @OA\Property(property="BICFI",type="string")
     * @OA\Property(property="ClearingSystemIdentification",type={"integer","null"},nullable=true)
     * @OA\Property(property="ClearingSystemMemberIdentification",type="integer")
     * @OA\Property(property="Code",type={"string","null"},nullable=true)
     * @OA\Property(property="Proprietary",type="string")
     * @OA\Property(property="MemberIdentification",type="string")
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="LegalEntityIdentifier",type={"string","null"},nullable=true)
     * @OA\Property(property="PostalAddress",ref="#/components/schemas/PostalAddress")
     *
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Id' => $this->id,
            'BICFI' => $this->bicfi,
            'ClearingSystemIdentification' => $this->clearing_system_identification,
            'ClearingSystemMemberIdentification' => $this->clearing_system_member_identification,
            'Code' => $this->code,
            'Proprietary' => $this->proprietary,
            'MemberIdentification' => $this->member_identification,
            'Name' => $this->name,
            'LegalEntityIdentifier' => $this->legal_entity_identifier,
            'PostalAddress' => $this->when($this->location, PostalAddress::make($this->location))
        ];
    }
}
