<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="PartnersCategory", type="object")
 */
class PartnersCategory extends JsonResource
{
    /**
     * @OA\Property(property="Category",type="string")
     * @OA\Property(property="Partner",type="array",@OA\Items(ref="#/components/schemas/Partner"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Category' => $this->when($this->category, $this->category->code),
            'Partner' => $this->when($this->partner, PartnersPartner::collection($this->partner)),
        ];
    }
}
