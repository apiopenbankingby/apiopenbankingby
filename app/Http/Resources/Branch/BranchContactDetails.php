<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="ContactDetails", type="object")
 */
class BranchContactDetails extends JsonResource
{
    /**
     * @OA\Property(property="Name",type={"string", "null"})
     * @OA\Property(property="PhoneNumber",type="string")
     * @OA\Property(property="MobileNumber",type="string")
     * @OA\Property(property="FaxNumber",type={"string", "null"})
     * @OA\Property(property="EmailAddress",type="string")
     * @OA\Property(property="Other",type={"string", "null"})
     * @OA\Property(property="SocialNetworks",type="array",@OA\Items(ref="#/components/schemas/SocialNetwork"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Name' => $this->name,
            'PhoneNumber' => $this->phone_number,
            'MobileNumber' => $this->mobile_number,
            'FaxNumber' => $this->fax_number,
            'EmailAddress' => $this->email_address,
            'Other' => $this->other,
            'SocialNetworks' => SocialNetwork::collection($this->socialNetwork),
        ];
    }
}
