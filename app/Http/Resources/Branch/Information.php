<?php

namespace App\Http\Resources\Branch;

use App\Http\Resources\Availability;
use App\Http\Resources\BranchContactDetails;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Information", type="object")
 */
class Information extends JsonResource
{
    /**
     * @OA\Property(property="Segment",type="string")
     * @OA\Property(property="Availability",type="array",@OA\Items(ref="#/components/schemas/Availability"))
     * @OA\Property(property="ContactDetails",type="array",@OA\Items(ref="#/components/schemas/ContactDetails"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Segment' => $this->when($this->segment, isset($this->segment->code) ? $this->segment->code : null),
            'Availability' => Availability::collection($this->availability),
            'ContactDetails' => BranchContactDetails::collection($this->contactDetails),
        ];
    }
}
