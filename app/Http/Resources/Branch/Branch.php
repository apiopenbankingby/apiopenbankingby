<?php

namespace App\Http\Resources\Branch;

use App\Http\Resources\Accessibilities;
use App\Http\Resources\Location;
use App\Http\Resources\Service\Service;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="Branch", type="object")
 */
class Branch extends JsonResource
{
    /**
     * @OA\Property(property="Id",type="string")
     * @OA\Property(property="Name",type="string")
     * @OA\Property(property="WiFi",type={"boolean","null"},nullable=true)
     * @OA\Property(property="EQueue",type={"boolean","null"},nullable=true)
     * @OA\Property(property="Accessibilities",ref="#/components/schemas/Accessibilities")
     * @OA\Property(property="PostalAddress",ref="#/components/schemas/Location")
     * @OA\Property(property="Information",type="array",@OA\Items(ref="#/components/schemas/Information"))
     * @OA\Property(property="Services",ref="#/components/schemas/Services",type={"object","null"})
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Id' => $this->id,
            'Name' => $this->name,
            'WiFi' => $this->wifi,
            'EQueue' => $this->equeue,
            'Accessibilities' => Accessibilities::make($this->accessibilities),
            'PostalAddress' => Location::make($this->location),
            'Information' => Information::collection($this->information),
            'Services' => Service::make($this->services),
        ];
    }
}
