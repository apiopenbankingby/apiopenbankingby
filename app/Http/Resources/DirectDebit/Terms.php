<?php

namespace App\Http\Resources\DirectDebit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DirectDebitTerms", type="object")
 */
class Terms extends JsonResource
{
    /**
     * @OA\Property(property="Term",type="array",@OA\Items(ref="#/components/schemas/DirectDebitTerm"))
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Term' => $this->when($this->term, Term::collection($this->term)),
        ];
    }
}
