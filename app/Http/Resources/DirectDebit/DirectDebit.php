<?php

namespace App\Http\Resources\DirectDebit;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @OA\Schema(schema="DirectDebit", type="object")
 */
class DirectDebit extends JsonResource
{
    /**
     * @OA\Property(property="Terms",ref="#/components/schemas/DirectDebitTerms")
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Terms' => $this->when($this->terms, Terms::make($this->terms)),
        ];
    }
}
