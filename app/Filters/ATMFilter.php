<?php

namespace App\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ATMFilter
 *
 * @package App\Filters
 */
class ATMFilter extends ModelFilter
{
    /**
     * Filter by type
     *
     * @param $value
     * @return ATMFilter | Builder
     */
    public function type($value)
    {
        return $this->whereHas('type', function ($q) use ($value) {
            return $q->where('code', $value);
        });
    }

    /**
     * Filter by current status
     *
     * @param $value
     * @return ATMFilter | Builder
     */
    public function currentStatus($value)
    {
        return $this->whereHas('currentStatus', function ($currentStatus) use ($value) {
            return $currentStatus->where('code', $value);
        });
    }

    /**
     * Filter by base currency
     *
     * @param $value
     * @return ATMFilter | Builder
     */
    public function baseCurrency($value)
    {
        return $this->whereHas('baseCurrency', function ($currentStatus) use ($value) {
            return $currentStatus->where('code', $value);
        });
    }

    /**
     * Filter by currency
     *
     * @param $value
     * @return ATMFilter | Builder
     */
    public function currency($value)
    {
        return $this->whereHas('currency', function ($currentStatus) use ($value) {
            return $currentStatus->where('code', $value);
        });
    }

    /**
     * Filter by cards
     *
     * @param $value
     * @return ATMFilter | Builder
     */
    public function cards($value)
    {
        return $this->whereHas('cards', function ($currentStatus) use ($value) {
            return $currentStatus->where('code', $value);
        });
    }
}
