<?php

namespace App\Filtres;

use App\Filtres\Traits\Card;
use App\Filtres\Traits\Deposit;
use App\Filtres\Traits\Jewel;
use App\Filtres\Traits\Loan;
use App\Filtres\Traits\Transfer;
use EloquentFilter\ModelFilter;
use Illuminate\Support\Str;

/**
 * Class ServiceFilter
 *
 * @package App\Filtres
 */
class ServiceFilter extends ModelFilter
{
    use Deposit, Loan, Card, Jewel, Transfer;

    protected $drop_id = false;

    /**
     * Filter to type
     *
     * @param $value
     * @return ServiceFilter
     */
    public function type($value)
    {
        return $this->whereHas('type', function ($q) use ($value) {
            return $q->where('code', $value);
        });
    }

    /**
     * Filter to Segment
     *
     * @param $value
     * @return ServiceFilter
     */
    public function segment($value)
    {
        return $this->whereHas('segment', function ($q) use ($value) {
            return $q->where('code', $value);
        });
    }

    /**
     * Filter to service id
     *
     * @param $value
     * @return ServiceFilter
     */
    public function serviceId($value)
    {
        return $this->where('id', $value);
    }

    /**
     * Filter to service id
     *
     * @param $value
     * @return ServiceFilter
     */
    public function branchId($value)
    {
        return $this->whereHas('services', function ($services) use ($value) {
            return $services->where('s_id', $value);
        });
    }

    /**
     * Filter to current status
     *
     * @param $value
     * @return ServiceFilter
     */
    public function currentStatus($value)
    {
        return $this->whereHas('currentStatus', function ($currentStatus) use ($value) {
            return $currentStatus->where('code', $value);
        });
    }

    /**
     * Filter to jewel type
     *
     * @param $value
     * @return ServiceFilter
     */
    public function currency($value)
    {
        $type = Str::lower($this->input('type'));

        switch ($type) {
            case 'deposit':
            {
                return $this->whereHas('deposit', function ($deposit) use ($value) {
                    return $deposit->whereHas('currency', function ($currency) use ($value) {
                        return $currency->whereHas('currency', function ($currencyType) use ($value) {
                            return $currencyType->where('code', $value);
                        });
                    });
                });
            }
            case 'loan':
            {
                return $this->whereHas('loan', function ($loan) use ($value) {
                    return $loan->whereHas('currency', function ($currency) use ($value) {
                        return $currency->whereHas('currency', function ($currencyType) use ($value) {
                            return $currencyType->where('code', $value);
                        });
                    });
                });
            }
            case 'card':
            {
                return $this->whereHas('card', function ($card) use ($value) {
                    return $card->whereHas('currency', function ($currency) use ($value) {
                        return $currency->where('code', $value);
                    });
                });
            }
            case 'transfer':
            {
                return $this->whereHas('transfer', function ($card) use ($value) {
                    return $card->whereHas('currency', function ($currency) use ($value) {
                        return $currency->where('code', $value);
                    });
                });
            }
            default:
            {
                return $this;
            }
        }
    }
}
