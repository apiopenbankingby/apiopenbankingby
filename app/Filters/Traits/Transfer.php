<?php

namespace App\Filtres\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait Transfer
 *
 * @package App\Filtres\Traits
 */
trait Transfer
{
    /**
     * Transfer system
     *
     * @param $value
     * @return $this|Builder
     */
    public function TransferSystem($value)
    {
        if (Str::lower($this->input('type')) === 'transfer') {
            return $this->whereHas('transfer', function ($loan) use ($value) {
                return $loan->whereHas('transferSystem', function ($loanCategory) use ($value) {
                    return $loanCategory->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Can receive
     *
     * @param $value
     * @return $this|Builder
     */
    public function CanReceive($value)
    {
        if (Str::lower($this->input('type')) === 'transfer') {
            return $this->where('can_receive', $value);
        }

        return $this;
    }
}
