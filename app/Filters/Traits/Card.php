<?php

namespace App\Filtres\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait Card
 *
 * @package App\Filtres\Traits
 */
trait Card
{
    /**
     * Payment card system
     *
     * @param $value
     * @return $this|Builder
     */
    public function paymentCardSystem($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->whereHas('paymentCardSystem', function ($paymentCardSystem) use ($value) {
                    return $paymentCardSystem->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Card category
     *
     * @param $value
     * @return $this|Builder
     */
    public function cardCategory($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->whereHas('cardCategory', function ($cardCategory) use ($value) {
                    return $cardCategory->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Card type
     *
     * @param $value
     * @return $this|Builder
     */
    public function cardType($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->whereHas('cardType', function ($cardType) use ($value) {
                    return $cardType->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Consumer card
     *
     * @param $value
     * @return $this|Builder
     */
    public function consumerCard($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->whereHas('consumerCard', function ($consumerCard) use ($value) {
                    return $consumerCard->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Card data entry mode
     *
     * @param $value
     * @return $this|Builder
     */
    public function cardDataEntryMode($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->whereHas('cardDataEntryMode', function ($cardDataEntryMode) use ($value) {
                    return $cardDataEntryMode->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Foreign country payments
     *
     * @param $value
     * @return $this|Builder
     */
    public function foreignCountryPayments($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('foreign_country_payments', $value);
            });
        }

        return $this;
    }

    /**
     * Custom design
     *
     * @param $value
     * @return $this|Builder
     */
    public function customDesign($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('custom_design', $value);
            });
        }

        return $this;
    }

    /**
     * Internet payments
     *
     * @param $value
     * @return $this|Builder
     */
    public function internetPayments($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('internet_payments', $value);
            });
        }

        return $this;
    }

    /**
     * Instant issue
     *
     * @param $value
     * @return $this|Builder
     */
    public function instantIssue($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('instant_issue', $value);
            });
        }

        return $this;
    }

    /**
     * Urgent issue
     *
     * @param $value
     * @return $this|Builder
     */
    public function urgentIssue($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('urgent_issue', $value);
            });
        }

        return $this;
    }

    /**
     * Permanent balance
     *
     * @param $value
     * @return $this|Builder
     */
    public function permanentBalance($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('permanent_balance', $value);
            });
        }

        return $this;
    }

    /**
     * Issue additional card
     *
     * @param $value
     * @return $this|Builder
     */
    public function issueAdditionalCard($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('issue_additional_card', $value);
            });
        }

        return $this;
    }

    /**
     * Notifications
     *
     * @param $value
     * @return $this|Builder
     */
    public function notifications($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('notifications', $value);
            });
        }

        return $this;
    }

    /**
     * Mobile payments
     *
     * @param $value
     * @return $this|Builder
     */
    public function mobilePayments($value)
    {
        if (Str::lower($this->input('type')) === 'card') {
            return $this->whereHas('card', function ($card) use ($value) {
                return $card->where('mobile_payments', $value);
            });
        }

        return $this;
    }
}
