<?php

namespace App\Filtres\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait Loan
 *
 * @package App\Filtres\Traits
 */
trait Loan
{
    /**
     * Loan category
     *
     * @param $value
     * @return $this|Builder
     */
    public function loanCategory($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->whereHas('loanCategory', function ($loanCategory) use ($value) {
                    return $loanCategory->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Loan type
     *
     * @param $value
     * @return $this|Builder
     */
    public function loanType($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->whereHas('loanType', function ($loanType) use ($value) {
                    return $loanType->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Loan form
     *
     * @param $value
     * @return $this|Builder
     */
    public function loanForm($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->whereHas('loanForm', function ($loanForm) use ($value) {
                    return $loanForm->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Application type
     *
     * @param $value
     * @return $this|Builder
     */
    public function applicationType($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->whereHas('applicationType', function ($applicationType) use ($value) {
                    return $applicationType->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Additional services
     *
     * @param $value
     * @return $this|Builder
     */
    public function additionalServices($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->whereHas('additionalServices', function ($additionalServices) use ($value) {
                    return $additionalServices->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Is renewable
     *
     * @param $value
     * @return $this|Builder
     */
    public function isRenewable($value)
    {
        if (Str::lower($this->input('type')) === 'loan') {
            return $this->whereHas('loan', function ($loan) use ($value) {
                return $loan->where('is_renewable', $value);
            });
        }

        return $this;
    }
}
