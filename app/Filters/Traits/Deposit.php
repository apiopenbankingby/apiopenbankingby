<?php

namespace App\Filtres\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait Deposit
 *
 * @package App\Filtres\Traits
 */
trait Deposit
{
    /**
     * Filter to deposit type
     *
     * @param $value
     * @return $this|Builder
     */
    public function depositType($value)
    {
        if (Str::lower($this->input('type')) === 'deposit') {
            return $this->whereHas('deposit', function ($deposit) use ($value) {
                return $deposit->whereHas('depositType', function ($depositType) use ($value) {
                    return $depositType->where('code', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Filter to is early withdrawal interest
     *
     * @param $value
     * @return $this|Builder
     */
    public function earlyWithDrawalInterest($value)
    {
        if (Str::lower($this->input('type')) === 'deposit') {
            return $this->whereHas('deposit', function ($deposit) use ($value) {
                return $deposit->where('is_early_withdrawal_interest', $value);
            });
        }

        return $this;
    }

    /**
     * Withdrawal Type deposit
     *
     * @param $value
     * @return $this|Builder
     */
    public function withdrawalType($value)
    {
        if (Str::lower($this->input('type')) === 'deposit') {
            return $this->whereHas('deposit', function ($deposit) use ($value) {
                return $deposit->whereHas('earlyWithdrawal', function ($earlyWithdrawal) use ($value) {
                    return $earlyWithdrawal->whereHas('withdrawalType', function ($withdrawalType) use ($value) {
                        return $withdrawalType->where('code', $value);
                    });
                });
            });
        }

        return $this;
    }

    /**
     * Partition withdrawal
     *
     * @param $value
     * @return $this|Builder
     */
    public function partitionalWithdrawal($value)
    {
        if (Str::lower($this->input('type')) === 'deposit') {
            return $this->whereHas('deposit', function ($deposit) use ($value) {
                return $deposit->whereHas('earlyWithdrawal', function ($earlyWithdrawal) use ($value) {
                    return $earlyWithdrawal->where('is_partitional_withdrawal', $value);
                });
            });
        }

        return $this;
    }

    /**
     * Capitalization type
     *
     * @param $value
     * @return $this|Builder
     */
    public function capitalizationType($value)
    {
        if (Str::lower($this->input('type')) === 'deposit') {
            return $this->whereHas('deposit', function ($deposit) use ($value) {
                return $deposit->whereHas('capitalization', function ($capitalization) use ($value) {
                    return $capitalization->whereHas('capitalizationType', function ($capitalizationType) use ($value) {
                        return $capitalizationType->where('code', $value);
                    });
                });
            });
        }

        return $this;
    }
}
