<?php

namespace App\Filtres\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Trait Jewel
 *
 * @package App\Filtres\Traits
 */
trait Jewel
{
    /**
     * Filter to jewel type
     *
     * @param $value
     * @return $this|Builder
     */
    public function jewelType($value)
    {
        if (Str::lower($this->input('type')) === 'jewel') {
            $value = $value === 'preciousmetal' || $value === 'precious_metal' ? 'preciousMetal' : $value;
            return $this->with('jewel.' . $value);
        }

        return $this;
    }
}
