<?php

namespace App\Filters;

use EloquentFilter\ModelFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BankFilter
 *
 * @package App\Filters
 */
class BankFilter extends ModelFilter
{

    /**
     * Filter by BICFI
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function bicfi($value)
    {
        return $this->where('bicfi', $value);
    }

    /**
     * Filter by clearingSystemMemberIdentification
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function clearingSystemMemberIdentification($value)
    {
        return $this->where('clearing_system_member_identification', $value);
    }

    /**
     * Filter by clearingSystemIdentification
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function clearingSystemIdentification($value)
    {
        return $this->where('clearing_system_identification', $value);
    }

    /**
     * Filter by code
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function code($value)
    {
        return $this->where('code', $value);
    }

    /**
     * Filter by proprietary
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function proprietary($value)
    {
        return $this->where('proprietary', $value);
    }

    /**
     * Filter by memberIdentification
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function memberIdentification($value)
    {
        return $this->where('member_identification', $value);
    }

    /**
     * Filter by LegalEntityIdentifier
     *
     * @param $value
     * @return BankFilter | Builder
     */
    public function legalEntityIdentifier($value)
    {
        return $this->where('legal_entity_identifier', $value);
    }
}
