<?php

use App\Models\Directory\GuaranteeType;
use Illuminate\Database\Seeder;

class GuaranteeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guaranteeType = [
            ['code' => 'PersonGuarantee', 'name' => 'Поручительство'],
            ['code' => 'Collateral', 'name' => 'Залог'],
            ['code' => 'Penalty', 'name' => 'Неустойка'],
            ['code' => 'Deposit', 'name' => 'Гарантийный депозит денег'],
            ['code' => 'TitulTransfer', 'name' => 'Перевод правового титула на имущество'],
            ['code' => 'NoGuarantee', 'name' => 'Без обеспечения'],
        ];

        foreach ($guaranteeType as $value) {
            GuaranteeType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
