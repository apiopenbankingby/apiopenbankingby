<?php

use App\Models\Directory\LoanForm;
use Illuminate\Database\Seeder;

class LoanFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanForm = [
            ['code' => 'Cashless', 'name' => 'Безналичная'],
            ['code' => 'Cash', 'name' => 'Наличная'],
            ['code' => 'CashCashless', 'name' => 'Безналичная и наличная'],
        ];

        foreach ($loanForm as $value) {
            LoanForm::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
