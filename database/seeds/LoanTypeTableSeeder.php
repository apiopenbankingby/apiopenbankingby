<?php

use App\Models\Directory\LoanType;
use Illuminate\Database\Seeder;

class LoanTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanType = [
            ['code' => 'Building', 'name' => 'На строительство и реконструкцию жилья'],
            ['code' => 'BuildingOther', 'name' => 'На строительство и реконструкцию иной недвижимости'],
            ['code' => 'RealtyBuy', 'name' => 'На приобретение жилья'],
            ['code' => 'OtherRealtyBuy', 'name' => 'На приобретение иной недвижимости'],
            ['code' => 'CarBuy', 'name' => 'На приобретение автомобиля'],
            ['code' => 'GoodsBuy', 'name' => 'На приобретение товаров (работ, услуг)'],
            ['code' => 'Ovedraft', 'name' => 'Овердрафтный кредит'],
            ['code' => 'Other', 'name' => 'Нецелевой потребительский кредит'],
            ['code' => 'RefinanceRealty', 'name' => 'Рефинансирование полученного ранее кредита на финансирование недвижимости: полученного ранее кредита на финансирование жилья. полученного ранее кредита на финансирование иной недвижимости'],
            ['code' => 'Refinance', 'name' => 'Рефинансирование полученного ранее кредита на потребительские нужды'],
        ];

        foreach ($loanType as $value) {
            LoanType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
