<?php

use App\Models\Atm\AtmStatus;
use Illuminate\Database\Seeder;

class AtmStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $atmStatus = [
            ['code' => 'On', 'name' => 'Доступно'],
            ['code' => 'Off', 'name' => 'Не доступно'],
            ['code' => 'TempOff', 'name' => 'Временно недоступен'],
        ];

        foreach ($atmStatus as $value) {
            AtmStatus::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
