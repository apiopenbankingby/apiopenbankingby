<?php

use App\Models\Directory\AdditionalService;
use Illuminate\Database\Seeder;

class AdditionalServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $additionalService = [
            ['code' => 'InsuranceProperty', 'name' => 'Страхование приобретенного в кредит имущества'],
            ['code' => 'InsuranceCreditor', 'name' => 'Добровольное страхование кредитополучателя'],
            ['code' => 'Other', 'name' => 'Иные'],
            ['code' => 'No', 'name' => 'Не предоставляются'],
        ];

        foreach ($additionalService as $value) {
            AdditionalService::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
