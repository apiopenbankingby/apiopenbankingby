<?php

use App\Models\Directory\CardDataEntry;
use Illuminate\Database\Seeder;

class CardDataEntriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardDataEntries = [
            ['code' => 'MagneticStripe', 'name' => 'Карточка с магнитной полосой'],
            ['code' => 'EMV', 'name' => 'Карточка с чипом'],
            ['code' => 'MagneticStripeEMV', 'name' => 'Карточка с магнитной полосой и чипом'],
            ['code' => 'Proximity', 'name' => 'Бесконтактная карточка'],
            ['code' => 'ProximityEMV', 'name' => 'Бесконтактная карточка с чипом'],
            ['code' => 'MagneticStripeProximityEMV', 'name' => 'Бесконтактная карточка с магнитной полосой и чипом'],
            ['code' => 'Virtual', 'name' => 'Виртуальная карточка'],
        ];

        foreach ($cardDataEntries as $value) {
            CardDataEntry::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
