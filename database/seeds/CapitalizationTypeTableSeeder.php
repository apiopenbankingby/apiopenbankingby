<?php

use App\Models\Directory\CapitalizationType;
use Illuminate\Database\Seeder;

class CapitalizationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $capitalizationType = [
            ['code' => 'Daily', 'name' => 'Ежедневная'],
            ['code' => 'Weekly', 'name' => 'Еженедельная'],
            ['code' => 'EachTenDays', 'name' => 'Ежедекадная'],
            ['code' => 'Monthly', 'name' => 'Ежемесячная'],
            ['code' => 'Annual', 'name' => 'Ежегодная'],
            ['code' => 'Other', 'name' => 'Иная'],
            ['code' => 'NoCapitalization', 'name' => 'Не предусмотрена'],
        ];

        foreach ($capitalizationType as $value) {
            CapitalizationType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
