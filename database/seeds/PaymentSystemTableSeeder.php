<?php

use App\Models\Directory\PaymentSystem;
use Illuminate\Database\Seeder;

class PaymentSystemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        PaymentSystem::where('code', 'VisaElectron')->delete();
        PaymentSystem::where('code', 'Maestro')->delete();
        PaymentSystem::where('code', 'Belcart')->delete();

        $paymentSystem = [
            ['code' => 'Visa', 'name' => 'Visa'],
            ['code' => 'MasterCard', 'name' => 'MasterCard'],
            ['code' => 'Belkart', 'name' => 'Белкaрт'],
            ['code' => 'AmericanExpress', 'name' => 'AmericanExpress'],
            ['code' => 'UnionPay', 'name' => 'UnionPay'],
            ['code' => 'JCB', 'name' => 'JCB'],
            ['code' => 'Mir', 'name' => 'Мир'],
        ];

        foreach ($paymentSystem as $value) {
            PaymentSystem::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
