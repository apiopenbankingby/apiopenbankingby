<?php

use App\Models\Directory\CardProductPersonalType;
use Illuminate\Database\Seeder;

class CardProductPersonalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardProductPersonalTypes = [
            ['code' => 'ChildPaymentCard', 'name' => 'Детская карточка'],
            ['code' => 'RetirementPaymentCard', 'name' => 'Пенсионная карточка'],
            ['code' => 'StudentPaymentCard', 'name' => 'Студенческая карточка'],
            ['code' => 'OtherPaymentCard', 'name' => 'Иная карточка'],
        ];

        foreach ($cardProductPersonalTypes as $value) {
            CardProductPersonalType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
