<?php

use App\Models\Directory\DepositType;
use Illuminate\Database\Seeder;

class DepositTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $depositType = [
            ['code' => 'UrgentRevocable', 'name' => 'Срочный отзывный'],
            ['code' => 'UrgentIrrevocable', 'name' => 'Срочный безотзывный'],
            ['code' => 'NominalRevocable', 'name' => 'Условный отзывный'],
            ['code' => 'NominalIrrevocable', 'name' => 'Условный безотзывный'],
            ['code' => 'OnDemand', 'name' => 'До востребования'],
        ];

        foreach ($depositType as $value) {
            DepositType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
