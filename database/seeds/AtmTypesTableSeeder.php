<?php

use Illuminate\Database\Seeder;
use App\Models\Directory\AtmType;

class AtmTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $atmTypes = [
            ['code' => 'ATM', 'name' => 'Банкомат'],
            ['code' => 'PST', 'name' => 'Платежно-справочный терминал'],
        ];

        foreach ($atmTypes as $value) {
            AtmType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
