<?php

use App\Models\Directory\ServiceStatus;
use App\Models\Directory\ServiceType;
use Illuminate\Database\Seeder;

class ServiceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceType = [
            ['code' => 'Loan', 'name' => 'Кредит'],
            ['code' => 'Deposit', 'name' => 'Депозит'],
            ['code' => 'Card', 'name' => 'Карта'],
            ['code' => 'Transfer', 'name' => 'Денежный перевод'],
            ['code' => 'Jewel', 'name' => 'Драгоценности'],
            ['code' => 'CurrencyExchange', 'name' => 'Обмен валют'],
            ['code' => 'DirectDebit', 'name' => 'Прямое дебетование'],
            ['code' => 'Other', 'name' => 'Другая услуга'],
        ];

        foreach ($serviceType as $value) {
            ServiceType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
