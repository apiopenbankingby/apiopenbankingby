<?php

use App\Models\Directory\PeriodType;
use Illuminate\Database\Seeder;

class PeriodTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $periodType = [
            ['code' => 'Day', 'name' => 'День'],
            ['code' => 'Month', 'name' => 'Месяц'],
            ['code' => 'Quarter', 'name' => 'Квартал'],
            ['code' => 'SemiYears', 'name' => 'Полугодие'],
            ['code' => 'Years', 'name' => 'Год'],
        ];

        foreach ($periodType as $value) {
            PeriodType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
