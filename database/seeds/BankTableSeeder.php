<?php

use App\Factory\ATMFactory;
use App\Factory\BankFactory;
use App\Factory\BranchFactory;
use App\Factory\CardFactory;
use App\Factory\CurrencyExchangeFactory;
use App\Factory\DepositFactory;
use App\Factory\DirectDebitFactory;
use App\Factory\JewelFactory;
use App\Factory\LoanFactory;
use App\Factory\TransferFactory;
use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $bank = BankFactory::create();
            $atm = ATMFactory::create();

            $branchOne = BranchFactory::create();
            $branchTwo = BranchFactory::create();
            $branchThree = BranchFactory::create();

            $cardServiceId = CardFactory::create()->id;
            $currencyExchangeServiceId = CurrencyExchangeFactory::create()->id;
            $depositServiceId = DepositFactory::create()->id;
            $directDebitServiceId = DirectDebitFactory::create()->id;
            $jewelServiceId = JewelFactory::create()->id;
            $loanServiceId = LoanFactory::create()->id;
            $transferServiceId = TransferFactory::create()->id;

            $branchOne->services->service()->attach([$cardServiceId]);
            $branchOne->services->service()->attach([$currencyExchangeServiceId]);
            $branchOne->services->service()->attach([$depositServiceId]);

            $branchTwo->services->service()->attach([$directDebitServiceId]);
            $branchTwo->services->service()->attach([$jewelServiceId]);
            $branchTwo->services->service()->attach([$loanServiceId]);

            $branchThree->services->service()->attach([$cardServiceId]);
            $branchThree->services->service()->attach([$currencyExchangeServiceId]);
            $branchThree->services->service()->attach([$transferServiceId]);


            $bank->branch()->attach([$branchOne->id, $branchTwo->id, $branchThree->id]);
            $bank->atm()->attach([$atm->id]);
            $bank->services->service()->sync([
                $cardServiceId,
                $currencyExchangeServiceId,
                $depositServiceId,
                $directDebitServiceId,
                $jewelServiceId,
                $loanServiceId,
                $transferServiceId
            ]);
        }
    }
}
