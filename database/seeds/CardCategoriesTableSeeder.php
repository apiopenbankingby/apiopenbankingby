<?php

use App\Models\Directory\CardCategory;
use Illuminate\Database\Seeder;

class CardCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardCategories = [
            ['code' => 'BasicCard', 'name' => 'Базовая карточка'],
            ['code' => 'ClassicCard', 'name' => 'Классическая карточка'],
            ['code' => 'PremiumCard', 'name' => 'Премиальная карточка'],
        ];

        foreach ($cardCategories as $value) {
            CardCategory::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
