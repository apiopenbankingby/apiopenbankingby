<?php

use App\Models\Directory\RateType;
use Illuminate\Database\Seeder;

class RateTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $depositType = [
            ['code' => 'Refinance', 'name' => 'Ставка привязана к ставке рефинансирования'],
            ['code' => 'Overnight', 'name' => 'Ставка привязана к ставке овернайт'],
            ['code' => 'FixRate', 'name' => 'Фиксированная ставка'],
            ['code' => 'RefinanceCoeff', 'name' => 'Ставка привязана к ставке рефинансирования (с коэффициентом)'],
            ['code' => 'OvernightCoeff', 'name' => 'Ставка привязана к ставке овернайт (с коэффициентом)'],
        ];

        foreach ($depositType as $value) {
            RateType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
