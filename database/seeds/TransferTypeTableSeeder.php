<?php

use App\Models\Directory\TransferType;
use Illuminate\Database\Seeder;

class TransferTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transferTypes = [
            ['code' => 'Ztrizh', 'name' => 'Стриж'],
            ['code' => 'Express', 'name' => 'Экспресс-перевод'],
            ['code' => 'Hutki', 'name' => 'Хуткія грошы'],
            ['code' => 'Skarahod', 'name' => 'Грошы-скараходы'],
            ['code' => 'WesternUnion', 'name' => 'Western Union'],
            ['code' => 'Korona', 'name' => 'Золотая корона'],
            ['code' => 'Unistream', 'name' => 'Юнистрим'],
            ['code' => 'MoneyGram', 'name' => 'Money Gram'],
            ['code' => 'Blizko', 'name' => 'BLIZKO'],
            ['code' => 'Contact', 'name' => 'Contact'],
            ['code' => 'Colibri', 'name' => 'Колибри'],
            ['code' => 'Faster', 'name' => 'Faster'],
            ['code' => 'RiaMoneyTransfer', 'name' => 'Ria Money Transfer'],
            ['code' => 'Swift', 'name' => 'Swift'],
        ];

        foreach ($transferTypes as $value) {
            TransferType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
