<?php

use App\Models\Directory\LoanCategory;
use Illuminate\Database\Seeder;

class LoanCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanCategory = [
            ['code' => 'Realty', 'name' => 'На недвижимость'],
            ['code' => 'Consumer', 'name' => 'На потребительские нужды'],
            ['code' => 'Refinancing', 'name' => 'На рефинансирование'],
        ];

        foreach ($loanCategory as $value) {
            LoanCategory::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
