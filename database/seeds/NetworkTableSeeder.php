<?php

use App\Models\Directory\Network;
use Illuminate\Database\Seeder;

class NetworkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $network = [
            ['code' => 'VK', 'name' => 'Вконтакте'],
            ['code' => 'Facebook', 'name' => 'Facebook'],
            ['code' => 'OK', 'name' => 'Одноклассники'],
            ['code' => 'Instagram', 'name' => 'Instagram'],
            ['code' => 'Twitter', 'name' => 'Twitter'],
            ['code' => 'Messenger', 'name' => 'Messenger'],
            ['code' => 'iMessage', 'name' => 'iMessage'],
            ['code' => 'Viber', 'name' => 'Viber'],
            ['code' => 'WhatsApp', 'name' => 'WhatsApp'],
        ];

        foreach ($network as $value) {
            Network::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
