<?php

use App\Models\Directory\PartnerCategoryType;
use Illuminate\Database\Seeder;

class PartnersCategoryTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentSystem = [
            ['code' => 'Минский район', 'name' => 'Минский район'],
            ['code' => 'Гродненский район', 'name' => 'Гродненский район'],
        ];

        foreach ($paymentSystem as $value) {
            PartnerCategoryType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
