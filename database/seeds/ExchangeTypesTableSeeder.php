<?php

use App\Models\Directory\ExchangeType;
use Illuminate\Database\Seeder;

class ExchangeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchangeTypes = [
            ['code' => 'Online', 'name' => 'Онлайн'],
            ['code' => 'Cash', 'name' => 'Наличные'],
            ['code' => 'ATM', 'name' => 'Банкомат'],
            ['code' => 'Cashless', 'name' => 'Безналичный'],
        ];

        foreach ($exchangeTypes as $value) {
            ExchangeType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
