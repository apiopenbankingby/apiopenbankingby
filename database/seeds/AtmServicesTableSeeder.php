<?php

use App\Models\Directory\AtmServiceType;
use Illuminate\Database\Seeder;

class AtmServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceName = [
            ['code' => 'CashWithdrawal', 'name' => 'Снятие наличных'],
            ['code' => 'PINChange', 'name' => 'Смена ПИН'],
            ['code' => 'PINUnblock', 'name' => 'Разблокировка ПИН'],
            ['code' => 'PINActivation', 'name' => 'Активация ПИН'],
            ['code' => 'Balance', 'name' => 'Просмотр баланса'],
            ['code' => 'MiniStatement', 'name' => 'Выписка'],
            ['code' => 'BillPayments', 'name' => 'Платежи'],
            ['code' => 'MobileBankingRegistration', 'name' => 'Регистрация мобильного банка'],
            ['code' => 'Other', 'name' => 'Другие'],
            ['code' => 'CurrencyExchange', 'name' => 'Обмен валют'],
            ['code' => 'CashIn', 'name' => 'Пополнение наличными'],
        ];

        foreach ($serviceName as $value) {
            AtmServiceType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
