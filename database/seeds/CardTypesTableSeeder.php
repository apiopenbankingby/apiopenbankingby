<?php

use App\Models\Directory\CardType;
use Illuminate\Database\Seeder;

class CardTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardTypes = [
            ['code' => 'DebetCard', 'name' => 'Дебетовая карточка'],
            ['code' => 'DebetOverdraftCard', 'name' => 'Дебетовая карточка с овердрафтом'],
            ['code' => 'CreditCard', 'name' => 'Кредитная карточка'],
        ];

        foreach ($cardTypes as $value) {
            CardType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
