<?php

use App\Models\Directory\WeekDay;
use Illuminate\Database\Seeder;

class WeekDayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weekDays = [
            ['code' => '01', 'name' => 'Понедельник'],
            ['code' => '02', 'name' => 'Вторник'],
            ['code' => '03', 'name' => 'Среда'],
            ['code' => '04', 'name' => 'Четверг'],
            ['code' => '05', 'name' => 'Пятница'],
            ['code' => '06', 'name' => 'Суббота'],
            ['code' => '07', 'name' => 'Воскресенье'],
        ];

        foreach ($weekDays as $value) {
            WeekDay::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
