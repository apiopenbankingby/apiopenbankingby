<?php

use App\Models\Directory\WithdrawalType;
use Illuminate\Database\Seeder;

class WithdrawalTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $withdrawalType = [
            ['code' => 'WithLoses', 'name' => 'Предусмотрена (без потери процентов)'],
            ['code' => 'WithoutLoses', 'name' => 'Предусмотрена (с пересчетом процентов по пониженной процентной ставке)'],
            ['code' => 'No', 'name' => 'Не предусмотрена'],
        ];

        foreach ($withdrawalType as $value) {
            WithdrawalType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
