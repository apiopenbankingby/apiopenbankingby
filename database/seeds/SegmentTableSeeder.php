<?php

use App\Models\Directory\Segment;
use Illuminate\Database\Seeder;

class SegmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $segment = [
            ['code' => 'Business', 'name' => 'Для юридических лиц'],
            ['code' => 'Individual', 'name' => 'Для физических лиц'],
        ];

        foreach ($segment as $value) {
            Segment::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
