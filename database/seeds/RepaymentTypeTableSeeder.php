<?php

use App\Models\Directory\RepaymentType;
use Illuminate\Database\Seeder;

class RepaymentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanForm = [
            ['code' => 'Differential', 'name' => 'Дифференцированные платежи'],
            ['code' => 'Annuity', 'name' => 'Аннуитетные платежи'],
            ['code' => 'EqualPayments', 'name' => 'Платежи равными долями'],
            ['code' => 'Other', 'name' => 'Иные'],
        ];

        foreach ($loanForm as $value) {
            RepaymentType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
