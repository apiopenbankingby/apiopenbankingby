<?php

use App\Models\Directory\ServiceStatus;
use Illuminate\Database\Seeder;

class ServiceStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceStatus::where('code', 'Archive')->delete();

        $serviceStatus = [
            ['code' => 'Active', 'name' => 'Доступно'],
            ['code' => 'Inactive', 'name' => 'Не доступно'],
            ['code' => 'Archived', 'name' => 'Архив'],
        ];

        foreach ($serviceStatus as $value) {
            ServiceStatus::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
