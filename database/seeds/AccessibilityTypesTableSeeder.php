<?php

use App\Models\Directory\AccessibilityType;
use Illuminate\Database\Seeder;

class AccessibilityTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accessibilityType = [
            ['code' => 'Braille', 'name' => 'Шрифт Брайля'],
            ['code' => 'AudioCashMachine', 'name' => 'Голосовое озвучивание'],
            ['code' => 'AutomaticDoors', 'name' => 'Автоматические двери'],
            ['code' => 'WheelchairAccess', 'name' => 'Доступность для инвалидных колясок'],
            ['code' => 'ExternalRamp', 'name' => 'Внешний пандус'],
            ['code' => 'InternalRamp', 'name' => 'Внутренний пандус'],
            ['code' => 'Other', 'name' => 'Другие'],
        ];

        foreach ($accessibilityType as $value) {
            AccessibilityType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
