<?php

use App\Models\Directory\JewelType;
use Illuminate\Database\Seeder;

class JewelTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jewelTypes = [
            ['code' => 'Platinum', 'name' => 'Платина'],
            ['code' => 'Silver', 'name' => 'Серебро'],
            ['code' => 'Gold', 'name' => 'Золото'],
            ['code' => 'Diamond', 'name' => 'Бриллиант'],
            ['code' => 'Coin', 'name' => 'Монета'],
        ];

        foreach ($jewelTypes as $value) {
            JewelType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
