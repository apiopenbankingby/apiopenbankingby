<?php

use App\Models\Directory\FeeType;
use Illuminate\Database\Seeder;

class FeeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $feeType = [
            ['code' => 'Subscription', 'name' => 'Абонентская плата'],
            ['code' => 'TransactionFee', 'name' => 'Плата за транзакцию'],
            ['code' => 'FailedFee', 'name' => 'Комиссия при ошибке платежа'],
            ['code' => 'ReturnedFee', 'name' => 'Комиссия за возврат платежа'],
        ];

        foreach ($feeType as $value) {
            FeeType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
