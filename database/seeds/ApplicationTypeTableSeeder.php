<?php

use App\Models\Directory\ApplicationType;
use Illuminate\Database\Seeder;

class ApplicationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $applicationType = [
            ['code' => 'Online', 'name' => 'Онлайн-заявка на кредит'],
            ['code' => 'Remote', 'name' => 'Удаленно через представителя банка'],
            ['code' => 'Bank', 'name' => 'В отделении банка'],
        ];

        foreach ($applicationType as $value) {
            ApplicationType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
