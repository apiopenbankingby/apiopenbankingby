<?php

use App\Models\Directory\Frequency;
use Illuminate\Database\Seeder;

class FrequencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $frequency = [
            ['code' => 'Annual', 'name' => 'Ежегодно'],
            ['code' => 'Monthly', 'name' => 'Ежемесячно'],
            ['code' => 'Quarterly', 'name' => 'Ежеквартально'],
            ['code' => 'SemiAnnual', 'name' => 'Раз в пол года'],
            ['code' => 'Weekly', 'name' => 'Еженедельно'],
            ['code' => 'Daily', 'name' => 'Ежедневно'],
            ['code' => 'TenDays', 'name' => 'Ежедекадно'],
            ['code' => 'OneTime', 'name' => 'Единоразово'],
        ];

        foreach ($frequency as $value) {
            Frequency::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
