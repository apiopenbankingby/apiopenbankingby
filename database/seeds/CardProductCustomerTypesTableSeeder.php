<?php

use App\Models\Directory\CardProductCustomerType;
use Illuminate\Database\Seeder;

class CardProductCustomerTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cardProductCustomerTypes = [
            ['code' => 'CorporateCard', 'name' => 'Корпоративная карточка'],
            ['code' => 'ConsumerCard', 'name' => 'Потребительская карточка'],
        ];

        foreach ($cardProductCustomerTypes as $value) {
            CardProductCustomerType::updateOrCreate(
                ['code' => $value['code']],
                ['name' => $value['name']]
            );
        }
    }
}
