<?php

use Illuminate\Database\Seeder;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CardDataEntriesTableSeeder::class,
            FeeTypeTableSeeder::class,
            RateTypeTableSeeder::class,
            TransferTypeTableSeeder::class,
            AccessibilityTypesTableSeeder::class,
            CardCategoriesTableSeeder::class,
            CardProductCustomerTypesTableSeeder::class,
            CardProductPersonalTypesTableSeeder::class,
            CardTypesTableSeeder::class,
            ExchangeTypesTableSeeder::class,
            FrequencyTableSeeder::class,
            NetworkTableSeeder::class,
            PaymentSystemTableSeeder::class,
            DepositTypeTableSeeder::class,
            CapitalizationTypeTableSeeder::class,
            WithdrawalTypeTableSeeder::class,
            PeriodTypeTableSeeder::class,
            LoanCategoryTableSeeder::class,
            LoanTypeTableSeeder::class,
            LoanFormTableSeeder::class,
            AdditionalServiceTableSeeder::class,
            ApplicationTypeTableSeeder::class,
            RepaymentTypeTableSeeder::class,
            GuaranteeTypeTableSeeder::class,
            SegmentTableSeeder::class,
            ServiceStatusTableSeeder::class,
            ServiceTypeTableSeeder::class,
            AtmServicesTableSeeder::class,
            AtmTypesTableSeeder::class,
            AtmStatusTableSeeder::class,
            CurrencyTypeTableSeeder::class,
            CountriesTableSeeder::class,
            JewelTypesTableSeeder::class,
            WeekDayTableSeeder::class,
            PartnersCategoryTypeTableSeeder::class,
        ]);
    }
}
