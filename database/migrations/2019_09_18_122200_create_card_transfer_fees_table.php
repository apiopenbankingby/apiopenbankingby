<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTransferFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_transfer_fees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('issuer_bank_id')->nullable();
            $table->foreign('issuer_bank_id')->references('id')->on('card_issuer_banks');

            $table->unsignedBigInteger('other_bank_id')->nullable();
            $table->foreign('other_bank_id')->references('id')->on('card_other_banks');

            $table->unsignedBigInteger('non_resident_bank')->nullable();
            $table->foreign('non_resident_bank')->references('id')->on('card_non_resident_banks');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_transfer_fees');
    }
}
