<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_exchanges', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('exchange_type_id');
            $table->foreign('exchange_type_id')->references('id')->on('exchange_types');

            $table->unsignedBigInteger('source_currency_id');
            $table->foreign('source_currency_id')->references('id')->on('currency_types');

            $table->unsignedBigInteger('target_currency_id');
            $table->foreign('target_currency_id')->references('id')->on('currency_types');

            $table->unsignedInteger('unit_currency')->nullable();
            $table->unsignedInteger('scale_currency')->nullable(); //вкладка CurrencyExchange
            $table->decimal('exchange_rate', 12,6);
            $table->decimal('amount_min', 12,6);
            $table->decimal('amount_max', 12,6);

            $table->enum('direction', ['buy', 'sell']);

            $table->dateTime('date_time');
            $table->dateTime('updated_date_time')->nullable(); //вкладка CurrencyExchange

            $table->json('additional_data')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_exchanges');
    }
}
