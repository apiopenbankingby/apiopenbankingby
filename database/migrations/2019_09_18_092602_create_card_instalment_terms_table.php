<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardInstalmentTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_instalment_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_instalment_terms_term', function (Blueprint $table) {
            $table->unsignedBigInteger('ts_id');
            $table->foreign('ts_id')->references('id')->on('card_instalment_terms')->onDelete('cascade');

            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('card_instalment_terms_terms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_instalment_terms_term');
        Schema::dropIfExists('card_instalment_terms');
    }
}
