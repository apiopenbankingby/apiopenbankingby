<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardInstalmentTermsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_instalment_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount_min', 12, 6);
            $table->decimal('amount_max', 12, 6);
            $table->integer('period_min');
            $table->integer('period_max');

            $table->unsignedBigInteger('interest_id');
            $table->foreign('interest_id')->references('id')->on('card_instalment_interests');

            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_instalment_terms_terms');
    }
}
