<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardAtmWithdrawalFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_atm_withdrawal_fees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('issuer_atm_id')->nullable();
            $table->foreign('issuer_atm_id')->references('id')->on('card_issuer_banks');

            $table->unsignedBigInteger('bank_partner_atm_id')->nullable();
            $table->foreign('bank_partner_atm_id')->references('id')->on('card_bank_partners');

            $table->unsignedBigInteger('other_atm_id')->nullable();
            $table->foreign('other_atm_id')->references('id')->on('card_other_banks');

            $table->unsignedBigInteger('non_resident_atm')->nullable();
            $table->foreign('non_resident_atm')->references('id')->on('card_non_resident_banks');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_atm_withdrawal_fees');
    }
}
