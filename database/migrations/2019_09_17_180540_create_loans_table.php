<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('loan_categories');

            $table->boolean('is_renewable')->default(false);

            $table->unsignedBigInteger('contact_id')->nullable();
            $table->foreign('contact_id')->references('id')->on('contact_details');

            $table->unsignedBigInteger('partner_id')->nullable();
            $table->foreign('partner_id')->references('id')->on('partners');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('loan_type', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id');
            $table->foreign('l_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('loan_types')->onDelete('cascade');
        });

        Schema::create('loan_form', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id');
            $table->foreign('l_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('f_id');
            $table->foreign('f_id')->references('id')->on('loan_forms')->onDelete('cascade');
        });

        Schema::create('loan_application_type', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id');
            $table->foreign('l_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('application_types')->onDelete('cascade');
        });

        Schema::create('loan_additional_service', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id');
            $table->foreign('l_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('additional_services')->onDelete('cascade');
        });

        Schema::create('loan_eligibility', function (Blueprint $table) {
            $table->unsignedBigInteger('loan_id');
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('eligibility_id');
            $table->foreign('eligibility_id')->references('id')->on('eligibilities')->onDelete('cascade');
        });

        Schema::create('loan_repayment', function (Blueprint $table) {
            $table->unsignedBigInteger('loan_id');
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('repayment_id');
            $table->foreign('repayment_id')->references('id')->on('repayments')->onDelete('cascade');
        });

        Schema::create('loan_currency', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id');
            $table->foreign('l_id')->references('id')->on('loans')->onDelete('cascade');

            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_type');
        Schema::dropIfExists('loan_form');
        Schema::dropIfExists('loan_application_type');
        Schema::dropIfExists('loan_additional_service');
        Schema::dropIfExists('loan_currency');
        Schema::dropIfExists('loan_repayment');
        Schema::dropIfExists('loan_eligibility');
        Schema::dropIfExists('loans');
    }
}
