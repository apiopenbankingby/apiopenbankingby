<?php

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyExchangeTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_exchange_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('currency_exchange_terms_term', function (Blueprint $table) {
            $table->unsignedBigInteger('terms_id');
            $table->foreign('terms_id')->references('id')->on('currency_exchange_terms')->onDelete('cascade');

            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('currency_exchanges')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_exchange_terms_term');
        Schema::dropIfExists('currency_exchange_terms');
    }
}
