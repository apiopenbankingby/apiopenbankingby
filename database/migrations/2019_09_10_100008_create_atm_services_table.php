<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtmServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atm_services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('atm_services_atm_service', function (Blueprint $table) {
            $table->unsignedBigInteger('atm_services_id');
            $table->foreign('atm_services_id')->references('id')->on('atm_services')->onDelete('cascade');

            $table->unsignedBigInteger('atm_service_id');
            $table->foreign('atm_service_id')->references('id')->on('atm_service')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atm_services_atm_service');
        Schema::dropIfExists('atm_services');
    }
}
