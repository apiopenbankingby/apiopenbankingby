<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('services_service', function (Blueprint $table) {
            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('services')->onDelete('cascade');

            $table->string('ss_id');
            $table->foreign('ss_id')->references('id')->on('services_services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_service');
        Schema::dropIfExists('services');
    }
}
