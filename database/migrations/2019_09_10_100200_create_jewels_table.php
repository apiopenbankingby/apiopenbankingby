<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJewelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jewels', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('precious_metals_id')->nullable();
            $table->foreign('precious_metals_id')->references('id')->on('precious_metals');

            $table->unsignedBigInteger('coins_id')->nullable();
            $table->foreign('coins_id')->references('id')->on('coins');

            $table->unsignedBigInteger('stones_id')->nullable();
            $table->foreign('stones_id')->references('id')->on('stones');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jewels');
    }
}
