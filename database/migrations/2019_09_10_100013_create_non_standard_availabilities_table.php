<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonStandardAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_standard_availabilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 140)->nullable();
            $table->date('from_date');
            $table->date('to_date');
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('day_non_standard_availability', function (Blueprint $table) {
            $table->unsignedBigInteger('day_id');
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');

            $table->unsignedBigInteger('a_id');
            $table->foreign('a_id')->references('id')->on('non_standard_availabilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_non_standard_availability');
        Schema::dropIfExists('non_standard_availabilities');
    }
}
