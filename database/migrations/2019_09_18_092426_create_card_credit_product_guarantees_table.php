<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCreditProductGuaranteesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_credit_product_guarantees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_credit_product_guarantees_type', function (Blueprint $table) {
            $table->unsignedBigInteger('g_id');
            $table->foreign('g_id')->references('id')->on('card_credit_product_guarantees')->onDelete('cascade');

            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('guarantee_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_credit_product_guarantees_type');
        Schema::dropIfExists('card_credit_product_guarantees');
    }
}
