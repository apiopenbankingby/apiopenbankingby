<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardLoyaltyProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_loyalty_programs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->json('loyalty_services')->nullable();
            $table->string('payment_system_loyalty_url')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_loyalty_program_partner', function (Blueprint $table) {
            $table->unsignedBigInteger('l_id')->nullable();
            $table->foreign('l_id')->references('id')->on('card_loyalty_programs')->onDelete('cascade');

            $table->unsignedBigInteger('p_id')->nullable();
            $table->foreign('p_id')->references('id')->on('partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_loyalty_program_partner');
        Schema::dropIfExists('card_loyalty_programs');
    }
}
