<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atms', function (Blueprint $table) {
            $table->string('id', 255)->unique();

            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('atm_types');

            $table->unsignedBigInteger('base_curr_id');
            $table->foreign('base_curr_id')->references('id')->on('currency_types');

            $table->unsignedBigInteger('curr_sts_id')->nullable();
            $table->foreign('curr_sts_id')->references('id')->on('atm_statuses');

            $table->string('description', 255)->nullable();
            $table->json('additional_data')->nullable();

            $table->unsignedBigInteger('location_id');
            $table->foreign('location_id')->references('id')->on('locations');

            $table->unsignedBigInteger('availability_id')->nullable();
            $table->foreign('availability_id')->references('id')->on('availabilities');

            $table->unsignedBigInteger('services_id')->nullable();
            $table->foreign('services_id')->references('id')->on('atm_services');

            $table->unsignedBigInteger('cont_det_id')->nullable();
            $table->foreign('cont_det_id')->references('id')->on('contact_details');

            $table->unsignedBigInteger('accessibility_id')->nullable();
            $table->foreign('accessibility_id')->references('id')->on('accessibilities');

            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });

        Schema::create('atm_card', function (Blueprint $table) {
            $table->string('atm_id');
            $table->foreign('atm_id')->references('id')->on('atms')->onDelete('cascade');

            $table->unsignedBigInteger('card_id');
            $table->foreign('card_id')->references('id')->on('payment_systems')->onDelete('cascade');
        });

        Schema::create('atm_currency', function (Blueprint $table) {
            $table->string('atm_id');
            $table->foreign('atm_id')->references('id')->on('atms')->onDelete('cascade');

            $table->unsignedBigInteger('curr_id');
            $table->foreign('curr_id')->references('id')->on('currency_types')->onDelete('cascade');
        });

        Schema::create('atm_currency_exchange', function (Blueprint $table) {
            $table->string('atm_id');
            $table->foreign('atm_id')->references('id')->on('atms')->onDelete('cascade');

            $table->unsignedBigInteger('curr_exch_id');
            $table->foreign('curr_exch_id')->references('id')->on('currency_exchanges')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atm_currency_exchange');
        Schema::dropIfExists('atm_currency');
        Schema::dropIfExists('atm_card');
        Schema::dropIfExists('atms');
    }
}
