<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('payment_card_system_id');
            $table->foreign('payment_card_system_id')->references('id')->on('payment_systems');

            $table->unsignedBigInteger('card_category_id');
            $table->foreign('card_category_id')->references('id')->on('card_categories');

            $table->unsignedBigInteger('card_type_id');
            $table->foreign('card_type_id')->references('id')->on('card_types');

            $table->unsignedBigInteger('consumer_card_id')->nullable();
            $table->foreign('consumer_card_id')->references('id')->on('card_product_personal_types');

            $table->unsignedBigInteger('card_data_entry_mode_id');
            $table->foreign('card_data_entry_mode_id')->references('id')->on('card_data_entries');

            $table->boolean('foreign_country_payments');
            $table->boolean('custom_design');
            $table->boolean('internet_payments')->nullable();
            $table->boolean('instant_issue')->nullable();
            $table->boolean('urgent_issue')->nullable();
            $table->boolean('permanent_balance')->nullable();
            $table->boolean('issue_additional_card')->nullable();
            $table->boolean('notifications')->nullable();
            $table->decimal('interest_rate_on_balance', 12, 6)->nullable();
            $table->json('bank_partners')->nullable();
            $table->enum('mobile_payments', ['ApplePay', 'SamsungPay', 'GooglePay', 'GarminPay'])->nullable();

            $table->unsignedBigInteger('card_expiration_date_id')->nullable();
            $table->foreign('card_expiration_date_id')->references('id')->on('card_expiration_dates');

            $table->unsignedBigInteger('currency_id')->nullable();
            $table->foreign('currency_id')->references('id')->on('currency_types');

            $table->unsignedBigInteger('instalment_id')->nullable();
            $table->foreign('instalment_id')->references('id')->on('card_instalments');

            $table->unsignedBigInteger('cash_back_id')->nullable();
            $table->foreign('cash_back_id')->references('id')->on('card_cash_backs');

            $table->unsignedBigInteger('loyalty_program_id')->nullable();
            $table->foreign('loyalty_program_id')->references('id')->on('card_loyalty_programs');

            $table->unsignedBigInteger('credit_product_id')->nullable();
            $table->foreign('credit_product_id')->references('id')->on('card_credit_products');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cards_fee_charges', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('cards')->onDelete('cascade');

            $table->unsignedBigInteger('f_id');
            $table->foreign('f_id')->references('id')->on('card_fee_charges')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards_fee_charges');
        Schema::dropIfExists('cards');
    }
}
