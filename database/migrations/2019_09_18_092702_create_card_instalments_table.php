<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardInstalmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_instalments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description', 255)->nullable();

            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('card_instalment_terms');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_instalment_partner', function (Blueprint $table) {
            $table->unsignedBigInteger('i_id');
            $table->foreign('i_id')->references('id')->on('card_instalments')->onDelete('cascade');

            $table->unsignedBigInteger('p_id');
            $table->foreign('p_id')->references('id')->on('partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_instalment_partner');
        Schema::dropIfExists('card_instalments');
    }
}
