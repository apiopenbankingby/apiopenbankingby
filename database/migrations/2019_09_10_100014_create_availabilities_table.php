<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->boolean('access_24_hours')->default(true);
            $table->boolean('is_restricted')->default(false);
            $table->boolean('same_as_organization')->nullable();
            $table->string('description', 255)->nullable();

            $table->unsignedBigInteger('standard_availability_id')->nullable();
            $table->foreign('standard_availability_id')->references('id')->on('standard_availabilities');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('availability_non_standard_availability', function (Blueprint $table) {
            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('availabilities')->onDelete('cascade');

            $table->unsignedBigInteger('n_id');
            $table->foreign('n_id')->references('id')->on('non_standard_availabilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availability_non_standard_availability');
        Schema::dropIfExists('availabilities');
    }
}
