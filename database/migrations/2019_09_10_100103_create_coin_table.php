<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 140);
            $table->unsignedInteger('value')->nullable();
            $table->string('quality', 140)->nullable();
            $table->decimal('selling_price', 14, 2);
            $table->unsignedBigInteger('curr_id');
            $table->foreign('curr_id')->references('id')->on('currency_types');
            $table->dateTime('date_time')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('coin_coin_material', function (Blueprint $table) {
            $table->unsignedBigInteger('coin_id');
            $table->foreign('coin_id')->references('id')->on('coin')->onDelete('cascade');

            $table->unsignedBigInteger('coin_material_id');
            $table->foreign('coin_material_id')->references('id')->on('coin_materials')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_coin_material');
        Schema::dropIfExists('coin');
    }
}
