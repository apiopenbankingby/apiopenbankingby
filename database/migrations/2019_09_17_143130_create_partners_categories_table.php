<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_categories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('partner_category_types');

            $table->decimal('category_discount_rate', 7,4)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('partners_category_partner', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('partners_categories')->onDelete('cascade');

            $table->unsignedBigInteger('p_id');
            $table->foreign('p_id')->references('id')->on('partners_partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_category_partner');
        Schema::dropIfExists('partners_categories');
    }
}
