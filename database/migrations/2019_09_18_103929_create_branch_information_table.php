<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_informations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('segment_id');
            $table->foreign('segment_id')->references('id')->on('segments');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('branch_information_availability', function (Blueprint $table) {
            $table->unsignedBigInteger('b_id');
            $table->foreign('b_id')->references('id')->on('branch_informations')->onDelete('cascade');

            $table->unsignedBigInteger('a_id');
            $table->foreign('a_id')->references('id')->on('contact_details')->onDelete('cascade');
        });

        Schema::create('branch_information_contact', function (Blueprint $table) {
            $table->unsignedBigInteger('b_id');
            $table->foreign('b_id')->references('id')->on('branch_informations')->onDelete('cascade');

            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('contact_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_information_contact');
        Schema::dropIfExists('branch_information_availability');
        Schema::dropIfExists('branch_informations');
    }
}
