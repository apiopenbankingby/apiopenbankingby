<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_services', function (Blueprint $table) {
            $table->string('id', 255)->unique();

            $table->json('branches')->nullable();

            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('service_types');

            $table->string('name', 140);

            $table->unsignedBigInteger('segment_id');
            $table->foreign('segment_id')->references('id')->on('segments');

            $table->dateTime('date_time');

            $table->unsignedBigInteger('status_id');
            $table->foreign('status_id')->references('id')->on('service_statuses');

            $table->string('description', 255)->nullable();
            $table->string('url', 2048)->nullable();
            $table->json('additional_data')->nullable();


            $table->unsignedBigInteger('deposit_id')->nullable();
            $table->foreign('deposit_id')->references('id')->on('deposits');

            $table->unsignedBigInteger('jewel_id')->nullable();
            $table->foreign('jewel_id')->references('id')->on('jewels');

            $table->unsignedBigInteger('transfer_id')->nullable();
            $table->foreign('transfer_id')->references('id')->on('transfers');

            $table->unsignedBigInteger('currency_exchange_id')->nullable();
            $table->foreign('currency_exchange_id')->references('id')->on('services_currency_exchanges');

            $table->unsignedBigInteger('direct_debit_id')->nullable();
            $table->foreign('direct_debit_id')->references('id')->on('direct_debits');

            $table->unsignedBigInteger('loan_id')->nullable();
            $table->foreign('loan_id')->references('id')->on('loans');

            $table->unsignedBigInteger('card_id')->nullable();
            $table->foreign('card_id')->references('id')->on('cards');

            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_services');
    }
}
