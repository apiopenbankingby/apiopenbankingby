<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('coins_coin', function (Blueprint $table) {
            $table->unsignedBigInteger('coins_id');
            $table->foreign('coins_id')->references('id')->on('coins')->onDelete('cascade');

            $table->unsignedBigInteger('coin_id');
            $table->foreign('coin_id')->references('id')->on('coin')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins_coin');
        Schema::dropIfExists('coins');
    }
}
