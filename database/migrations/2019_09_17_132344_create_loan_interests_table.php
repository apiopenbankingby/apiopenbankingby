<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_interests', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('rate_id');
            $table->foreign('rate_id')->references('id')->on('rate_types');
            $table->decimal('rate', 12, 6);
            $table->decimal('early_repayment_interest', 12, 6)->nullable();
            $table->unsignedInteger('grace_period_interest')->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_interests');
    }
}
