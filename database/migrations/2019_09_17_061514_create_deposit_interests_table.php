<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_interests', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('rate_id');
            $table->foreign('rate_id')->references('id')->on('rate_types');
            $table->decimal('rate', 7, 4);
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_interests');
    }
}
