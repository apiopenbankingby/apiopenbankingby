<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardFeeChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_fee_charges', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('issue_fee', 6, 2)->nullable();
            $table->decimal('instant_issue_fee', 6, 2)->nullable();
            $table->decimal('urgent_issue_fee', 6, 2)->nullable();
            $table->decimal('additional_card_issue_fee', 6, 2)->nullable();
            $table->decimal('notification_fee', 6, 2)->nullable();

            $table->unsignedBigInteger('re_issue_fee_id')->nullable();
            $table->foreign('re_issue_fee_id')->references('id')->on('card_re_issue_fees');

            $table->unsignedBigInteger('service_fee_id')->nullable();
            $table->foreign('service_fee_id')->references('id')->on('card_service_fees');

            $table->unsignedBigInteger('balance_check_fee_id')->nullable();
            $table->foreign('balance_check_fee_id')->references('id')->on('card_balance_check_fees');

            $table->unsignedBigInteger('transfer_fee_id')->nullable();
            $table->foreign('transfer_fee_id')->references('id')->on('card_transfer_fees');

            $table->unsignedBigInteger('bank_withdrawal_fee_id')->nullable();
            $table->foreign('bank_withdrawal_fee_id')->references('id')->on('card_bank_withdrawal_fees');

            $table->unsignedBigInteger('atm_withdrawal_fee_id')->nullable();
            $table->foreign('atm_withdrawal_fee_id')->references('id')->on('card_atm_withdrawal_fees');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_fee_charges_replenishment_fee', function (Blueprint $table) {
            $table->unsignedBigInteger('f_id');
            $table->foreign('f_id')->references('id')->on('card_fee_charges')->onDelete('cascade');

            $table->unsignedBigInteger('r_id');
            $table->foreign('r_id')->references('id')->on('card_replenishment_fees')->onDelete('cascade');
        });

        Schema::create('card_fee_charges_custom_fee', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('card_fee_charges')->onDelete('cascade');

            $table->unsignedBigInteger('cust_id');
            $table->foreign('cust_id')->references('id')->on('card_custom_fees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_fee_charges_custom_fee');
        Schema::dropIfExists('card_fee_charges_replenishment_fee');
        Schema::dropIfExists('card_fee_charges');
    }
}
