<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('country_destination_id');
            $table->unsignedInteger('transfer_period')->nullable();
            $table->decimal('amount_min', 12,6);
            $table->decimal('amount_max', 12,6);
            $table->decimal('free_rate', 12,6)->nullable();
            $table->decimal('free_amount', 12,6)->nullable();
            $table->json('additional_data')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_terms_terms');
    }
}
