<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMobilePaymentToCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn('mobile_payments');
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->json('mobile_payments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropColumn('mobile_payments');
        });

        Schema::table('cards', function (Blueprint $table) {
            $table->enum('mobile_payments', ['ApplePay', 'SamsungPay', 'GooglePay', 'GarminPay'])->nullable();
        });
    }
}
