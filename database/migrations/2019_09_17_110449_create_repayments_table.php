<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class   CreateRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('repayment_type_id');
            $table->foreign('repayment_type_id')->references('id')->on('repayment_types');

            $table->unsignedInteger('grace_period')->nullable();
            $table->boolean('is_early_repayment')->nullable();
            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayments');
    }
}
