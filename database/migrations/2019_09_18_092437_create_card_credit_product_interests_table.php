<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCreditProductInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_credit_product_interests', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('rate_id');
            $table->foreign('rate_id')->references('id')->on('rate_types');
            $table->string('rate');
            $table->string('early_repayment_interest')->nullable();
            $table->string('grace_period_interest')->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_credit_product_interests');
    }
}
