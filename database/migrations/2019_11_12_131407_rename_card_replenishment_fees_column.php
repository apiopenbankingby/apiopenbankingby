<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameCardReplenishmentFeesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('card_replenishment_fees', function (Blueprint $table) {
            $table->renameColumn('amount_min', 'fee_amount_min');
            $table->renameColumn('amount_max', 'fee_amount_max');
            $table->renameColumn('rate', 'fee_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_replenishment_fees', function (Blueprint $table) {
            $table->renameColumn('fee_amount_min', 'amount_min');
            $table->renameColumn('fee_amount_max', 'amount_max');
            $table->renameColumn('fee_rate', 'rate');
        });
    }
}
