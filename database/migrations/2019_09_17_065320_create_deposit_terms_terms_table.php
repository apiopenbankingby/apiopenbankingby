<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTermsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount_min', 12, 6);
            $table->decimal('amount_max', 12, 6);
            $table->boolean('tax_included')->default(false);
            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('deposit_terms_term_period', function (Blueprint $table) {
            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('deposit_terms_terms')->onDelete('cascade');

            $table->unsignedBigInteger('period_id');
            $table->foreign('period_id')->references('id')->on('deposit_periods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_terms_term_period');
        Schema::dropIfExists('deposit_terms_terms');
    }
}
