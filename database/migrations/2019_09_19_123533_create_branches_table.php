<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->string('id', 255)->unique();
            $table->string('name', 140);

            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('services');

            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('locations');

            $table->unsignedBigInteger('accessibility_id')->nullable();
            $table->foreign('accessibility_id')->references('id')->on('accessibilities');

            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });

        Schema::create('branch_information', function (Blueprint $table) {
            $table->string('b_id');
            $table->foreign('b_id')->references('id')->on('branches')->onDelete('cascade');

            $table->unsignedBigInteger('i_id');
            $table->foreign('i_id')->references('id')->on('branch_informations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_information');
        Schema::dropIfExists('branches');
    }
}
