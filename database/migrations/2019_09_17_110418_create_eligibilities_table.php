<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEligibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eligibilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('age_min');
            $table->unsignedInteger('age_max');
            $table->boolean('is_salary_project')->nullable();
            $table->decimal('income_min', 14, 2)->nullable();
            $table->unsignedInteger('work_period');
            $table->boolean('is_relative_income')->nullable();
            $table->boolean('is_resident')->nullable();
            $table->boolean('need_income_certificate')->default(false);
            $table->boolean('is_retired')->nullable();
            $table->boolean('is_entrepreneur')->nullable();
            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eligibilities');
    }
}
