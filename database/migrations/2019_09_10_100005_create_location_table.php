<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('street_name', 70)->nullable();
            $table->string('building_number', 16)->nullable();
            $table->string('department', 70)->nullable();
            $table->string('post_code', 16)->nullable();
            $table->string('town_name', 35)->nullable();
            $table->string('country_sub_division', 35)->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->json('address_line')->nullable();
            $table->json('description')->nullable();
            $table->unsignedBigInteger('geographic_coordinates_id')->nullable();
            $table->foreign('geographic_coordinates_id')->references('id')->on('geographic_coordinates');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
