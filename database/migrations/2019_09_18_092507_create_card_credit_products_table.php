<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCreditProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_credit_products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('description', 255)->nullable();

            $table->unsignedBigInteger('credit_products_term_id')->nullable();
            $table->foreign('credit_products_term_id')->references('id')->on('card_credit_product_terms');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_credit_products');
    }
}
