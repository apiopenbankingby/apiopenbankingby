<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('curr_id');

            $table->unsignedBigInteger('deposit_term_id')->nullable();
            $table->foreign('deposit_term_id')->references('id')->on('deposit_terms');

            $table->unsignedBigInteger('loan_term_id')->nullable();
            $table->foreign('loan_term_id')->references('id')->on('loan_terms');

            $table->unsignedBigInteger('transfer_terms_id')->nullable();
            $table->foreign('transfer_terms_id')->references('id')->on('transfer_terms_terms');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
