<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreciousMetalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precious_metals', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('precious_metals_precious_metal', function (Blueprint $table) {
            $table->unsignedBigInteger('precious_metals_id');
            $table->foreign('precious_metals_id')->references('id')->on('precious_metals')->onDelete('cascade');

            $table->unsignedBigInteger('precious_metal_id');
            $table->foreign('precious_metal_id')->references('id')->on('precious_metal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precious_metals_precious_metal');
        Schema::dropIfExists('precious_metals');
    }
}
