<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreciousMetalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precious_metal', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('material_id');
            $table->foreign('material_id')->references('id')->on('jewel_types');
            $table->decimal('weight', 14, 2);
            $table->decimal('sample', 8,2);
            $table->decimal('selling_price', 14, 2);
            $table->decimal('purchase_price', 14, 2);
            $table->unsignedBigInteger('curr_id');
            $table->foreign('curr_id')->references('id')->on('currency_types');
            $table->dateTime('date_time')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precious_metal');
    }
}
