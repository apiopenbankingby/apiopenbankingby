<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectDebitTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_debit_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('direct_debit_terms_term', function (Blueprint $table) {
            $table->unsignedBigInteger('d_id');
            $table->foreign('d_id')->references('id')->on('direct_debit_terms')->onDelete('cascade');

            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('direct_debit_terms_terms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_debit_terms_term');
        Schema::dropIfExists('direct_debit_terms');
    }
}
