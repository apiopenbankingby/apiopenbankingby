<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardServiceFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_service_fees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('annual_fee')->nullable();
            $table->string('monthly_fee')->nullable();
            $table->string('one_time_fee')->nullable();
            $table->string('e_issue_fee')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_service_fees');
    }
}
