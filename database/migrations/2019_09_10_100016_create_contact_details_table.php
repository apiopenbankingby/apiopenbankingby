<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 140)->nullable();
            $table->string('phone_number', 30)->nullable();
            $table->string('mobile_number', 30)->nullable();
            $table->string('fax_number', 30)->nullable();
            $table->string('email_address', 2048)->nullable();
            $table->string('other', 35)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('contact_detail_social', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('contact_details')->onDelete('cascade');

            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('social_networks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_detail_social');
        Schema::dropIfExists('contact_details');
    }
}
