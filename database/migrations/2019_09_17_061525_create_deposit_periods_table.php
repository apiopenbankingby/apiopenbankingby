<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_periods', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('period_min');
            $table->bigInteger('period_max');
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('period_types');

            $table->unsignedBigInteger('replenishment_id')->nullable();
            $table->foreign('replenishment_id')->references('id')->on('deposit_replenishments');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('deposit_period_interest', function (Blueprint $table) {
            $table->unsignedBigInteger('period_id');
            $table->foreign('period_id')->references('id')->on('deposit_periods')->onDelete('cascade');

            $table->unsignedBigInteger('interest_id');
            $table->foreign('interest_id')->references('id')->on('deposit_interests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_period_interest');
        Schema::dropIfExists('deposit_periods');
    }
}
