<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardExpirationDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_expiration_dates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('months');

            $table->timestamps();
            $table->softDeletes();

            $table->index('months');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_expiration_dates');
    }
}
