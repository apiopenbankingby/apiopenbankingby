<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessibilities', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('accessibilities_accessibility', function (Blueprint $table) {
            $table->unsignedBigInteger('acesibts_id');
            $table->foreign('acesibts_id')->references('id')->on('accessibilities')->onDelete('cascade');

            $table->unsignedBigInteger('acesibt_id');
            $table->foreign('acesibt_id')->references('id')->on('accessibility')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessibilities');
    }
}
