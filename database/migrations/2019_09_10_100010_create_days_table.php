<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('day_code_id')->nullable();
            $table->foreign('day_code_id')->references('id')->on('week_days');

            $table->time('opening_time');
            $table->time('closing_time');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('break_day', function (Blueprint $table) {
            $table->unsignedBigInteger('break_id');
            $table->foreign('break_id')->references('id')->on('dbreaks')->onDelete('cascade');

            $table->unsignedBigInteger('day_id');
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('break_day');
        Schema::dropIfExists('days');
    }
}
