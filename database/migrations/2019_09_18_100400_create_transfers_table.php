<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('transfer_type_id');
            $table->foreign('transfer_type_id')->references('id')->on('transfer_types');

            $table->json('additional_services')->nullable();
            $table->boolean('can_receive')->nullable();

            $table->unsignedBigInteger('cont_det_id')->nullable();
            $table->foreign('cont_det_id')->references('id')->on('contact_details');

            $table->unsignedBigInteger('availability_id')->nullable();
            $table->foreign('availability_id')->references('id')->on('availabilities');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('currency_transfer', function (Blueprint $table) {
            $table->unsignedBigInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');

            $table->unsignedBigInteger('transfer_id');
            $table->foreign('transfer_id')->references('id')->on('transfers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_transfer');
        Schema::dropIfExists('transfers');
    }
}
