<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardBankPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_bank_partners', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('fee_rate',12,6)->nullable();
            $table->decimal('fee_amount', 12, 6)->nullable();
            $table->decimal('fee_amount_min', 12, 6)->nullable();
            $table->decimal('fee_amount_max', 12, 6)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_bank_partners');
    }
}
