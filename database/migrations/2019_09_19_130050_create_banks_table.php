<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->string('id', 255)->unique();

            $table->string('bicfi')->nullable();
            $table->bigInteger('clearing_system_identification')->nullable();
            $table->bigInteger('clearing_system_member_identification')->nullable();

            $table->string('code', 35)->nullable();
            $table->enum('proprietary', ['BYNBB', 'UNKWN'])->default('UNKWN');

            $table->string('member_identification', 35)->nullable();
            $table->string('name', 140)->nullable();
            $table->string('legal_entity_identifier')->nullable();

            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->references('id')->on('locations');

            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('services');

            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });

        Schema::create('bank_atm', function (Blueprint $table) {
            $table->string('b_id', 255);
            $table->foreign('b_id')->references('id')->on('banks')->onDelete('cascade');

            $table->string('a_id');
            $table->foreign('a_id')->references('id')->on('atms')->onDelete('cascade');
        });

        Schema::create('bank_branch', function (Blueprint $table) {
            $table->string('b_id');
            $table->foreign('b_id')->references('id')->on('banks')->onDelete('cascade');

            $table->string('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_branch');
        Schema::dropIfExists('bank_atm');
        Schema::dropIfExists('banks');
    }
}
