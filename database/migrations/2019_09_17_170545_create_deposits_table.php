<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('type_id')->nullable();
            $table->foreign('type_id')->references('id')->on('deposit_types');

            $table->unsignedBigInteger('capitalization_id')->nullable();
            $table->foreign('capitalization_id')->references('id')->on('capitalizations');

            $table->unsignedBigInteger('early_withdrawal_id')->nullable();
            $table->foreign('early_withdrawal_id')->references('id')->on('early_withdrawals');

            $table->unsignedBigInteger('contact_id')->nullable();
            $table->foreign('contact_id')->references('id')->on('contact_details');

            $table->boolean('is_early_withdrawal_interest')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('deposit_currency', function (Blueprint $table) {
            $table->unsignedBigInteger('d_id');
            $table->foreign('d_id')->references('id')->on('deposits')->onDelete('cascade');

            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_currency');
        Schema::dropIfExists('deposits');
    }
}
