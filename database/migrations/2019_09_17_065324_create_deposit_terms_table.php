<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('deposit_terms_term', function (Blueprint $table) {
            $table->unsignedBigInteger('terms_id');
            $table->foreign('terms_id')->references('id')->on('deposit_terms')->onDelete('cascade');
            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('deposit_terms_terms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_terms_term');
        Schema::dropIfExists('deposit_terms');
    }
}
