<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTermsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount_min', 12, 6);
            $table->decimal('amount_max', 12, 6);
            $table->unsignedInteger('period');

            $table->unsignedBigInteger('period_type_id');
            $table->foreign('period_type_id')->references('id')->on('period_types');

            $table->unsignedBigInteger('interest_id')->nullable();
            $table->foreign('interest_id')->references('id')->on('loan_interests');

            $table->decimal('own_funds', 7, 4)->nullable();
            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('loan_terms_term_guarantee', function (Blueprint $table) {
            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('loan_terms_terms')->onDelete('cascade');

            $table->unsignedBigInteger('g_id');
            $table->foreign('g_id')->references('id')->on('loan_guarantees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_terms_term_guarantee');
        Schema::dropIfExists('loan_terms_terms');
    }
}
