<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCashBacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_cash_backs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('amount_min');
            $table->unsignedInteger('amount_max');
            $table->unsignedInteger('transaction_min')->nullable();
            $table->decimal('non_partners_rate', 12, 6)->nullable();

            $table->unsignedBigInteger('frequency_id')->nullable();
            $table->foreign('frequency_id')->references('id')->on('frequencies');

            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_cash_back_partner', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id')->nullable();
            $table->foreign('c_id')->references('id')->on('card_cash_backs')->onDelete('cascade');

            $table->unsignedBigInteger('p_id')->nullable();
            $table->foreign('p_id')->references('id')->on('partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_cash_back_partner');
        Schema::dropIfExists('card_cash_backs');
    }
}
