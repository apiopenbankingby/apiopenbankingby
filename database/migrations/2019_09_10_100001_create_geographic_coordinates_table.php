<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeographicCoordinatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geographic_coordinates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('geolocation_id')->nullable();
            $table->foreign('geolocation_id')->references('id')->on('geolocations');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geographic_coordinates');
    }
}
