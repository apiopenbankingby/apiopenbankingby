<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stones', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('stones_stone', function (Blueprint $table) {
            $table->unsignedBigInteger('stones_id');
            $table->foreign('stones_id')->references('id')->on('stones')->onDelete('cascade');

            $table->unsignedBigInteger('stone_id');
            $table->foreign('stone_id')->references('id')->on('stone')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stones_stone');
        Schema::dropIfExists('stones');
    }
}
