<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardReplenishmentFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_replenishment_fees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount_min', 12,6)->nullable();
            $table->decimal('amount_max', 12,6)->nullable();
            $table->decimal('rate', 12,6)->nullable();

            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_replenishment_fees');
    }
}
