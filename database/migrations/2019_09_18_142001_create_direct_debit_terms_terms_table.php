<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectDebitTermsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_debit_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('fee_type_id');
            $table->foreign('fee_type_id')->references('id')->on('fee_types');

            $table->decimal('fee_rate', 12, 6)->nullable();
            $table->decimal('fee_amount', 12, 6)->nullable();
            $table->decimal('fee_amount_min', 12, 6)->nullable();
            $table->decimal('fee_amount_max', 12, 6)->nullable();

            $table->unsignedBigInteger('frequency_id')->nullable();
            $table->foreign('frequency_id')->references('id')->on('frequencies');

            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_debit_terms_terms');
    }
}
