<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stone', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('material_id');
            $table->foreign('material_id')->references('id')->on('jewel_types');
            $table->decimal('weight', 14,2);
            $table->string('certificate', 140)->nullable();
            $table->string('form', 140)->nullable();
            $table->string('color', 140)->nullable();
            $table->string('clarity', 140)->nullable();
            $table->decimal('selling_price', 14, 2);
            $table->unsignedBigInteger('curr_id');
            $table->foreign('curr_id')->references('id')->on('currency_types');
            $table->dateTime('date_time')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stone');
    }
}
