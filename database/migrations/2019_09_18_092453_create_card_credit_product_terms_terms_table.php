<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCreditProductTermsTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_credit_product_terms_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->decimal('amount_min', 12, 6);
            $table->decimal('amount_max', 12, 6);
            $table->integer('period');

            $table->unsignedBigInteger('interest_id');
            $table->foreign('interest_id')->references('id')->on('card_credit_product_interests');

            $table->json('additional_data')->nullable();
            $table->string('description', 255)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('card_credit_product_terms_term_guarantee', function (Blueprint $table) {
            $table->unsignedBigInteger('t_id');
            $table->foreign('t_id')->references('id')->on('card_credit_product_terms_terms')->onDelete('cascade');

            $table->unsignedBigInteger('g_id');
            $table->foreign('g_id')->references('id')->on('card_credit_product_guarantees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_credit_product_terms_term_guarantee');
        Schema::dropIfExists('card_credit_product_terms_terms');
    }
}
