<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameCardServiceFeesColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('card_service_fees', function (Blueprint $table) {
            $table->renameColumn('annual_fee', 'fee_annual');
            $table->renameColumn('monthly_fee', 'fee_monthly');
            $table->renameColumn('one_time_fee', 'fee_one_time');
            $table->renameColumn('e_issue_fee', 'fee_e_issue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_service_fees', function (Blueprint $table) {
            $table->renameColumn('fee_annual', 'annual_fee');
            $table->renameColumn('fee_monthly', 'monthly_fee');
            $table->renameColumn('fee_one_time', 'one_time_fee');
            $table->renameColumn('fee_e_issue', 'e_issue_fee');
        });
    }
}
