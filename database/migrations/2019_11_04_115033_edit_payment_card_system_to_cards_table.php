<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditPaymentCardSystemToCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->dropForeign(['payment_card_system_id']);
            $table->dropColumn('payment_card_system_id');
        });

        Schema::create('cards_payment_systems', function (Blueprint $table) {
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('cards')->onDelete('cascade');

            $table->unsignedBigInteger('p_id');
            $table->foreign('p_id')->references('id')->on('payment_systems')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards', function (Blueprint $table) {
            $table->unsignedBigInteger('payment_card_system_id');
            $table->foreign('payment_card_system_id')->references('id')->on('payment_systems');
        });
        Schema::drop('cards_payment_systems');
    }
}
