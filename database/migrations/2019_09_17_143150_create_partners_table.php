<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('partners_category', function (Blueprint $table) {
            $table->unsignedBigInteger('p_id');
            $table->foreign('p_id')->references('id')->on('partners')->onDelete('cascade');
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('partners_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_category');
        Schema::dropIfExists('partners');
    }
}
