<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_terms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('loan_terms_term', function (Blueprint $table) {
            $table->unsignedBigInteger('terms_id');
            $table->foreign('terms_id')->references('id')->on('loan_terms')->onDelete('cascade');

            $table->unsignedBigInteger('term_id');
            $table->foreign('term_id')->references('id')->on('loan_terms_terms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_terms_term');
        Schema::dropIfExists('loan_terms');
    }
}
