<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesCurrencyExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_currency_exchanges', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('curr_ech_terms_id')->nullable();
            $table->foreign('curr_ech_terms_id')->references('id')->on('currency_exchange_terms');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_currency_exchanges');
    }
}
