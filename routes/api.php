<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api\V1')->group(function () {
    Route::apiResource('banks', 'BankController')->only('index', 'show');
    Route::apiResource('banks.atms', 'AtmController')->only('index', 'show');
    Route::apiResource('banks.branches', 'BranchController')->only('index', 'show');
    Route::apiResource('banks.services', 'ServiceController')->only('index', 'store');
});
